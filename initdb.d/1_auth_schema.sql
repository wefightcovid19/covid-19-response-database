--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.17
-- Dumped by pg_dump version 10.8

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: auth; Type: SCHEMA; Schema: -; Owner: auth-db-user
--

CREATE SCHEMA auth;


ALTER SCHEMA auth OWNER TO "auth-db-user";

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: admin_event_entity; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.admin_event_entity (
    id character varying(36) NOT NULL,
    admin_event_time bigint,
    realm_id character varying(255),
    operation_type character varying(255),
    auth_realm_id character varying(255),
    auth_client_id character varying(255),
    auth_user_id character varying(255),
    ip_address character varying(255),
    resource_path character varying(2550),
    representation text,
    error character varying(255),
    resource_type character varying(64)
);


ALTER TABLE auth.admin_event_entity OWNER TO "auth-db-user";

--
-- Name: associated_policy; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.associated_policy (
    policy_id character varying(36) NOT NULL,
    associated_policy_id character varying(36) NOT NULL
);


ALTER TABLE auth.associated_policy OWNER TO "auth-db-user";

--
-- Name: authentication_execution; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.authentication_execution (
    id character varying(36) NOT NULL,
    alias character varying(255),
    authenticator character varying(36),
    realm_id character varying(36),
    flow_id character varying(36),
    requirement integer,
    priority integer,
    authenticator_flow boolean DEFAULT false NOT NULL,
    auth_flow_id character varying(36),
    auth_config character varying(36)
);


ALTER TABLE auth.authentication_execution OWNER TO "auth-db-user";

--
-- Name: authentication_flow; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.authentication_flow (
    id character varying(36) NOT NULL,
    alias character varying(255),
    description character varying(255),
    realm_id character varying(36),
    provider_id character varying(36) DEFAULT 'basic-flow'::character varying NOT NULL,
    top_level boolean DEFAULT false NOT NULL,
    built_in boolean DEFAULT false NOT NULL
);


ALTER TABLE auth.authentication_flow OWNER TO "auth-db-user";

--
-- Name: authenticator_config; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.authenticator_config (
    id character varying(36) NOT NULL,
    alias character varying(255),
    realm_id character varying(36)
);


ALTER TABLE auth.authenticator_config OWNER TO "auth-db-user";

--
-- Name: authenticator_config_entry; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.authenticator_config_entry (
    authenticator_id character varying(36) NOT NULL,
    value text,
    name character varying(255) NOT NULL
);


ALTER TABLE auth.authenticator_config_entry OWNER TO "auth-db-user";

--
-- Name: broker_link; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.broker_link (
    identity_provider character varying(255) NOT NULL,
    storage_provider_id character varying(255),
    realm_id character varying(36) NOT NULL,
    broker_user_id character varying(255),
    broker_username character varying(255),
    token text,
    user_id character varying(255) NOT NULL
);


ALTER TABLE auth.broker_link OWNER TO "auth-db-user";

--
-- Name: client; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.client (
    id character varying(36) NOT NULL,
    enabled boolean DEFAULT false NOT NULL,
    full_scope_allowed boolean DEFAULT false NOT NULL,
    client_id character varying(255),
    not_before integer,
    public_client boolean DEFAULT false NOT NULL,
    secret character varying(255),
    base_url character varying(255),
    bearer_only boolean DEFAULT false NOT NULL,
    management_url character varying(255),
    surrogate_auth_required boolean DEFAULT false NOT NULL,
    realm_id character varying(36),
    protocol character varying(255),
    node_rereg_timeout integer DEFAULT 0,
    frontchannel_logout boolean DEFAULT false NOT NULL,
    consent_required boolean DEFAULT false NOT NULL,
    name character varying(255),
    service_accounts_enabled boolean DEFAULT false NOT NULL,
    client_authenticator_type character varying(255),
    root_url character varying(255),
    description character varying(255),
    registration_token character varying(255),
    standard_flow_enabled boolean DEFAULT true NOT NULL,
    implicit_flow_enabled boolean DEFAULT false NOT NULL,
    direct_access_grants_enabled boolean DEFAULT false NOT NULL
);


ALTER TABLE auth.client OWNER TO "auth-db-user";

--
-- Name: client_attributes; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.client_attributes (
    client_id character varying(36) NOT NULL,
    value character varying(4000),
    name character varying(255) NOT NULL
);


ALTER TABLE auth.client_attributes OWNER TO "auth-db-user";

--
-- Name: client_auth_flow_bindings; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.client_auth_flow_bindings (
    client_id character varying(36) NOT NULL,
    flow_id character varying(36),
    binding_name character varying(255) NOT NULL
);


ALTER TABLE auth.client_auth_flow_bindings OWNER TO "auth-db-user";

--
-- Name: client_default_roles; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.client_default_roles (
    client_id character varying(36) NOT NULL,
    role_id character varying(36) NOT NULL
);


ALTER TABLE auth.client_default_roles OWNER TO "auth-db-user";

--
-- Name: client_initial_access; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.client_initial_access (
    id character varying(36) NOT NULL,
    realm_id character varying(36) NOT NULL,
    "timestamp" integer,
    expiration integer,
    count integer,
    remaining_count integer
);


ALTER TABLE auth.client_initial_access OWNER TO "auth-db-user";

--
-- Name: client_node_registrations; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.client_node_registrations (
    client_id character varying(36) NOT NULL,
    value integer,
    name character varying(255) NOT NULL
);


ALTER TABLE auth.client_node_registrations OWNER TO "auth-db-user";

--
-- Name: client_scope; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.client_scope (
    id character varying(36) NOT NULL,
    name character varying(255),
    realm_id character varying(36),
    description character varying(255),
    protocol character varying(255)
);


ALTER TABLE auth.client_scope OWNER TO "auth-db-user";

--
-- Name: client_scope_attributes; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.client_scope_attributes (
    scope_id character varying(36) NOT NULL,
    value character varying(2048),
    name character varying(255) NOT NULL
);


ALTER TABLE auth.client_scope_attributes OWNER TO "auth-db-user";

--
-- Name: client_scope_client; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.client_scope_client (
    client_id character varying(36) NOT NULL,
    scope_id character varying(36) NOT NULL,
    default_scope boolean DEFAULT false NOT NULL
);


ALTER TABLE auth.client_scope_client OWNER TO "auth-db-user";

--
-- Name: client_scope_role_mapping; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.client_scope_role_mapping (
    scope_id character varying(36) NOT NULL,
    role_id character varying(36) NOT NULL
);


ALTER TABLE auth.client_scope_role_mapping OWNER TO "auth-db-user";

--
-- Name: client_session; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.client_session (
    id character varying(36) NOT NULL,
    client_id character varying(36),
    redirect_uri character varying(255),
    state character varying(255),
    "timestamp" integer,
    session_id character varying(36),
    auth_method character varying(255),
    realm_id character varying(255),
    auth_user_id character varying(36),
    current_action character varying(36)
);


ALTER TABLE auth.client_session OWNER TO "auth-db-user";

--
-- Name: client_session_auth_status; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.client_session_auth_status (
    authenticator character varying(36) NOT NULL,
    status integer,
    client_session character varying(36) NOT NULL
);


ALTER TABLE auth.client_session_auth_status OWNER TO "auth-db-user";

--
-- Name: client_session_note; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.client_session_note (
    name character varying(255) NOT NULL,
    value character varying(255),
    client_session character varying(36) NOT NULL
);


ALTER TABLE auth.client_session_note OWNER TO "auth-db-user";

--
-- Name: client_session_prot_mapper; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.client_session_prot_mapper (
    protocol_mapper_id character varying(36) NOT NULL,
    client_session character varying(36) NOT NULL
);


ALTER TABLE auth.client_session_prot_mapper OWNER TO "auth-db-user";

--
-- Name: client_session_role; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.client_session_role (
    role_id character varying(255) NOT NULL,
    client_session character varying(36) NOT NULL
);


ALTER TABLE auth.client_session_role OWNER TO "auth-db-user";

--
-- Name: client_user_session_note; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.client_user_session_note (
    name character varying(255) NOT NULL,
    value character varying(2048),
    client_session character varying(36) NOT NULL
);


ALTER TABLE auth.client_user_session_note OWNER TO "auth-db-user";

--
-- Name: component; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.component (
    id character varying(36) NOT NULL,
    name character varying(255),
    parent_id character varying(36),
    provider_id character varying(36),
    provider_type character varying(255),
    realm_id character varying(36),
    sub_type character varying(255)
);


ALTER TABLE auth.component OWNER TO "auth-db-user";

--
-- Name: component_config; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.component_config (
    id character varying(36) NOT NULL,
    component_id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(4000)
);


ALTER TABLE auth.component_config OWNER TO "auth-db-user";

--
-- Name: composite_role; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.composite_role (
    composite character varying(36) NOT NULL,
    child_role character varying(36) NOT NULL
);


ALTER TABLE auth.composite_role OWNER TO "auth-db-user";

--
-- Name: credential; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.credential (
    id character varying(36) NOT NULL,
    device character varying(255),
    hash_iterations integer,
    salt bytea,
    type character varying(255),
    value character varying(4000),
    user_id character varying(36),
    created_date bigint,
    counter integer DEFAULT 0,
    digits integer DEFAULT 6,
    period integer DEFAULT 30,
    algorithm character varying(36) DEFAULT NULL::character varying
);


ALTER TABLE auth.credential OWNER TO "auth-db-user";

--
-- Name: credential_attribute; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.credential_attribute (
    id character varying(36) NOT NULL,
    credential_id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(4000)
);


ALTER TABLE auth.credential_attribute OWNER TO "auth-db-user";

--
-- Name: databasechangelog; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.databasechangelog (
    id character varying(255) NOT NULL,
    author character varying(255) NOT NULL,
    filename character varying(255) NOT NULL,
    dateexecuted timestamp without time zone NOT NULL,
    orderexecuted integer NOT NULL,
    exectype character varying(10) NOT NULL,
    md5sum character varying(35),
    description character varying(255),
    comments character varying(255),
    tag character varying(255),
    liquibase character varying(20),
    contexts character varying(255),
    labels character varying(255),
    deployment_id character varying(10)
);


ALTER TABLE auth.databasechangelog OWNER TO "auth-db-user";

--
-- Name: databasechangeloglock; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.databasechangeloglock (
    id integer NOT NULL,
    locked boolean NOT NULL,
    lockgranted timestamp without time zone,
    lockedby character varying(255)
);


ALTER TABLE auth.databasechangeloglock OWNER TO "auth-db-user";

--
-- Name: default_client_scope; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.default_client_scope (
    realm_id character varying(36) NOT NULL,
    scope_id character varying(36) NOT NULL,
    default_scope boolean DEFAULT false NOT NULL
);


ALTER TABLE auth.default_client_scope OWNER TO "auth-db-user";

--
-- Name: event_entity; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.event_entity (
    id character varying(36) NOT NULL,
    client_id character varying(255),
    details_json character varying(2550),
    error character varying(255),
    ip_address character varying(255),
    realm_id character varying(255),
    session_id character varying(255),
    event_time bigint,
    type character varying(255),
    user_id character varying(255)
);


ALTER TABLE auth.event_entity OWNER TO "auth-db-user";

--
-- Name: fed_credential_attribute; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.fed_credential_attribute (
    id character varying(36) NOT NULL,
    credential_id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(4000)
);


ALTER TABLE auth.fed_credential_attribute OWNER TO "auth-db-user";

--
-- Name: fed_user_attribute; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.fed_user_attribute (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36),
    value character varying(2024)
);


ALTER TABLE auth.fed_user_attribute OWNER TO "auth-db-user";

--
-- Name: fed_user_consent; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.fed_user_consent (
    id character varying(36) NOT NULL,
    client_id character varying(36),
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36),
    created_date bigint,
    last_updated_date bigint,
    client_storage_provider character varying(36),
    external_client_id character varying(255)
);


ALTER TABLE auth.fed_user_consent OWNER TO "auth-db-user";

--
-- Name: fed_user_consent_cl_scope; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.fed_user_consent_cl_scope (
    user_consent_id character varying(36) NOT NULL,
    scope_id character varying(36) NOT NULL
);


ALTER TABLE auth.fed_user_consent_cl_scope OWNER TO "auth-db-user";

--
-- Name: fed_user_credential; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.fed_user_credential (
    id character varying(36) NOT NULL,
    device character varying(255),
    hash_iterations integer,
    salt bytea,
    type character varying(255),
    value character varying(255),
    created_date bigint,
    counter integer DEFAULT 0,
    digits integer DEFAULT 6,
    period integer DEFAULT 30,
    algorithm character varying(36) DEFAULT 'HmacSHA1'::character varying,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36)
);


ALTER TABLE auth.fed_user_credential OWNER TO "auth-db-user";

--
-- Name: fed_user_group_membership; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.fed_user_group_membership (
    group_id character varying(36) NOT NULL,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36)
);


ALTER TABLE auth.fed_user_group_membership OWNER TO "auth-db-user";

--
-- Name: fed_user_required_action; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.fed_user_required_action (
    required_action character varying(255) DEFAULT ' '::character varying NOT NULL,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36)
);


ALTER TABLE auth.fed_user_required_action OWNER TO "auth-db-user";

--
-- Name: fed_user_role_mapping; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.fed_user_role_mapping (
    role_id character varying(36) NOT NULL,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    storage_provider_id character varying(36)
);


ALTER TABLE auth.fed_user_role_mapping OWNER TO "auth-db-user";

--
-- Name: federated_identity; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.federated_identity (
    identity_provider character varying(255) NOT NULL,
    realm_id character varying(36),
    federated_user_id character varying(255),
    federated_username character varying(255),
    token text,
    user_id character varying(36) NOT NULL
);


ALTER TABLE auth.federated_identity OWNER TO "auth-db-user";

--
-- Name: federated_user; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.federated_user (
    id character varying(255) NOT NULL,
    storage_provider_id character varying(255),
    realm_id character varying(36) NOT NULL
);


ALTER TABLE auth.federated_user OWNER TO "auth-db-user";

--
-- Name: group_attribute; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.group_attribute (
    id character varying(36) DEFAULT 'sybase-needs-something-here'::character varying NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(255),
    group_id character varying(36) NOT NULL
);


ALTER TABLE auth.group_attribute OWNER TO "auth-db-user";

--
-- Name: group_role_mapping; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.group_role_mapping (
    role_id character varying(36) NOT NULL,
    group_id character varying(36) NOT NULL
);


ALTER TABLE auth.group_role_mapping OWNER TO "auth-db-user";

--
-- Name: identity_provider; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.identity_provider (
    internal_id character varying(36) NOT NULL,
    enabled boolean DEFAULT false NOT NULL,
    provider_alias character varying(255),
    provider_id character varying(255),
    store_token boolean DEFAULT false NOT NULL,
    authenticate_by_default boolean DEFAULT false NOT NULL,
    realm_id character varying(36),
    add_token_role boolean DEFAULT true NOT NULL,
    trust_email boolean DEFAULT false NOT NULL,
    first_broker_login_flow_id character varying(36),
    post_broker_login_flow_id character varying(36),
    provider_display_name character varying(255),
    link_only boolean DEFAULT false NOT NULL
);


ALTER TABLE auth.identity_provider OWNER TO "auth-db-user";

--
-- Name: identity_provider_config; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.identity_provider_config (
    identity_provider_id character varying(36) NOT NULL,
    value text,
    name character varying(255) NOT NULL
);


ALTER TABLE auth.identity_provider_config OWNER TO "auth-db-user";

--
-- Name: identity_provider_mapper; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.identity_provider_mapper (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    idp_alias character varying(255) NOT NULL,
    idp_mapper_name character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL
);


ALTER TABLE auth.identity_provider_mapper OWNER TO "auth-db-user";

--
-- Name: idp_mapper_config; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.idp_mapper_config (
    idp_mapper_id character varying(36) NOT NULL,
    value text,
    name character varying(255) NOT NULL
);


ALTER TABLE auth.idp_mapper_config OWNER TO "auth-db-user";

--
-- Name: keycloak_group; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.keycloak_group (
    id character varying(36) NOT NULL,
    name character varying(255),
    parent_group character varying(36),
    realm_id character varying(36)
);


ALTER TABLE auth.keycloak_group OWNER TO "auth-db-user";

--
-- Name: keycloak_role; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.keycloak_role (
    id character varying(36) NOT NULL,
    client_realm_constraint character varying(36),
    client_role boolean DEFAULT false NOT NULL,
    description character varying(255),
    name character varying(255),
    realm_id character varying(255),
    client character varying(36),
    realm character varying(36)
);


ALTER TABLE auth.keycloak_role OWNER TO "auth-db-user";

--
-- Name: migration_model; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.migration_model (
    id character varying(36) NOT NULL,
    version character varying(36)
);


ALTER TABLE auth.migration_model OWNER TO "auth-db-user";

--
-- Name: offline_client_session; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.offline_client_session (
    user_session_id character varying(36) NOT NULL,
    client_id character varying(36) NOT NULL,
    offline_flag character varying(4) NOT NULL,
    "timestamp" integer,
    data text,
    client_storage_provider character varying(36) DEFAULT 'local'::character varying NOT NULL,
    external_client_id character varying(255) DEFAULT 'local'::character varying NOT NULL
);


ALTER TABLE auth.offline_client_session OWNER TO "auth-db-user";

--
-- Name: offline_user_session; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.offline_user_session (
    user_session_id character varying(36) NOT NULL,
    user_id character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL,
    created_on integer NOT NULL,
    offline_flag character varying(4) NOT NULL,
    data text,
    last_session_refresh integer DEFAULT 0 NOT NULL
);


ALTER TABLE auth.offline_user_session OWNER TO "auth-db-user";

--
-- Name: policy_config; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.policy_config (
    policy_id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    value text
);


ALTER TABLE auth.policy_config OWNER TO "auth-db-user";

--
-- Name: protocol_mapper; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.protocol_mapper (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    protocol character varying(255) NOT NULL,
    protocol_mapper_name character varying(255) NOT NULL,
    client_id character varying(36),
    client_scope_id character varying(36)
);


ALTER TABLE auth.protocol_mapper OWNER TO "auth-db-user";

--
-- Name: protocol_mapper_config; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.protocol_mapper_config (
    protocol_mapper_id character varying(36) NOT NULL,
    value text,
    name character varying(255) NOT NULL
);


ALTER TABLE auth.protocol_mapper_config OWNER TO "auth-db-user";

--
-- Name: realm; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.realm (
    id character varying(36) NOT NULL,
    access_code_lifespan integer,
    user_action_lifespan integer,
    access_token_lifespan integer,
    account_theme character varying(255),
    admin_theme character varying(255),
    email_theme character varying(255),
    enabled boolean DEFAULT false NOT NULL,
    events_enabled boolean DEFAULT false NOT NULL,
    events_expiration bigint,
    login_theme character varying(255),
    name character varying(255),
    not_before integer,
    password_policy character varying(2550),
    registration_allowed boolean DEFAULT false NOT NULL,
    remember_me boolean DEFAULT false NOT NULL,
    reset_password_allowed boolean DEFAULT false NOT NULL,
    social boolean DEFAULT false NOT NULL,
    ssl_required character varying(255),
    sso_idle_timeout integer,
    sso_max_lifespan integer,
    update_profile_on_soc_login boolean DEFAULT false NOT NULL,
    verify_email boolean DEFAULT false NOT NULL,
    master_admin_client character varying(36),
    login_lifespan integer,
    internationalization_enabled boolean DEFAULT false NOT NULL,
    default_locale character varying(255),
    reg_email_as_username boolean DEFAULT false NOT NULL,
    admin_events_enabled boolean DEFAULT false NOT NULL,
    admin_events_details_enabled boolean DEFAULT false NOT NULL,
    edit_username_allowed boolean DEFAULT false NOT NULL,
    otp_policy_counter integer DEFAULT 0,
    otp_policy_window integer DEFAULT 1,
    otp_policy_period integer DEFAULT 30,
    otp_policy_digits integer DEFAULT 6,
    otp_policy_alg character varying(36) DEFAULT 'HmacSHA1'::character varying,
    otp_policy_type character varying(36) DEFAULT 'totp'::character varying,
    browser_flow character varying(36),
    registration_flow character varying(36),
    direct_grant_flow character varying(36),
    reset_credentials_flow character varying(36),
    client_auth_flow character varying(36),
    offline_session_idle_timeout integer DEFAULT 0,
    revoke_refresh_token boolean DEFAULT false NOT NULL,
    access_token_life_implicit integer DEFAULT 0,
    login_with_email_allowed boolean DEFAULT true NOT NULL,
    duplicate_emails_allowed boolean DEFAULT false NOT NULL,
    docker_auth_flow character varying(36),
    refresh_token_max_reuse integer DEFAULT 0,
    allow_user_managed_access boolean DEFAULT false NOT NULL,
    sso_max_lifespan_remember_me integer DEFAULT 0 NOT NULL,
    sso_idle_timeout_remember_me integer DEFAULT 0 NOT NULL
);


ALTER TABLE auth.realm OWNER TO "auth-db-user";

--
-- Name: realm_attribute; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.realm_attribute (
    name character varying(255) NOT NULL,
    value character varying(255),
    realm_id character varying(36) NOT NULL
);


ALTER TABLE auth.realm_attribute OWNER TO "auth-db-user";

--
-- Name: realm_default_groups; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.realm_default_groups (
    realm_id character varying(36) NOT NULL,
    group_id character varying(36) NOT NULL
);


ALTER TABLE auth.realm_default_groups OWNER TO "auth-db-user";

--
-- Name: realm_default_roles; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.realm_default_roles (
    realm_id character varying(36) NOT NULL,
    role_id character varying(36) NOT NULL
);


ALTER TABLE auth.realm_default_roles OWNER TO "auth-db-user";

--
-- Name: realm_enabled_event_types; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.realm_enabled_event_types (
    realm_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE auth.realm_enabled_event_types OWNER TO "auth-db-user";

--
-- Name: realm_events_listeners; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.realm_events_listeners (
    realm_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE auth.realm_events_listeners OWNER TO "auth-db-user";

--
-- Name: realm_required_credential; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.realm_required_credential (
    type character varying(255) NOT NULL,
    form_label character varying(255),
    input boolean DEFAULT false NOT NULL,
    secret boolean DEFAULT false NOT NULL,
    realm_id character varying(36) NOT NULL
);


ALTER TABLE auth.realm_required_credential OWNER TO "auth-db-user";

--
-- Name: realm_smtp_config; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.realm_smtp_config (
    realm_id character varying(36) NOT NULL,
    value character varying(255),
    name character varying(255) NOT NULL
);


ALTER TABLE auth.realm_smtp_config OWNER TO "auth-db-user";

--
-- Name: realm_supported_locales; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.realm_supported_locales (
    realm_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE auth.realm_supported_locales OWNER TO "auth-db-user";

--
-- Name: redirect_uris; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.redirect_uris (
    client_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE auth.redirect_uris OWNER TO "auth-db-user";

--
-- Name: required_action_config; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.required_action_config (
    required_action_id character varying(36) NOT NULL,
    value text,
    name character varying(255) NOT NULL
);


ALTER TABLE auth.required_action_config OWNER TO "auth-db-user";

--
-- Name: required_action_provider; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.required_action_provider (
    id character varying(36) NOT NULL,
    alias character varying(255),
    name character varying(255),
    realm_id character varying(36),
    enabled boolean DEFAULT false NOT NULL,
    default_action boolean DEFAULT false NOT NULL,
    provider_id character varying(255),
    priority integer
);


ALTER TABLE auth.required_action_provider OWNER TO "auth-db-user";

--
-- Name: resource_attribute; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.resource_attribute (
    id character varying(36) DEFAULT 'sybase-needs-something-here'::character varying NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(255),
    resource_id character varying(36) NOT NULL
);


ALTER TABLE auth.resource_attribute OWNER TO "auth-db-user";

--
-- Name: resource_policy; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.resource_policy (
    resource_id character varying(36) NOT NULL,
    policy_id character varying(36) NOT NULL
);


ALTER TABLE auth.resource_policy OWNER TO "auth-db-user";

--
-- Name: resource_scope; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.resource_scope (
    resource_id character varying(36) NOT NULL,
    scope_id character varying(36) NOT NULL
);


ALTER TABLE auth.resource_scope OWNER TO "auth-db-user";

--
-- Name: resource_server; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.resource_server (
    id character varying(36) NOT NULL,
    allow_rs_remote_mgmt boolean DEFAULT false NOT NULL,
    policy_enforce_mode character varying(15) NOT NULL
);


ALTER TABLE auth.resource_server OWNER TO "auth-db-user";

--
-- Name: resource_server_perm_ticket; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.resource_server_perm_ticket (
    id character varying(36) NOT NULL,
    owner character varying(36) NOT NULL,
    requester character varying(36) NOT NULL,
    created_timestamp bigint NOT NULL,
    granted_timestamp bigint,
    resource_id character varying(36) NOT NULL,
    scope_id character varying(36),
    resource_server_id character varying(36) NOT NULL,
    policy_id character varying(36)
);


ALTER TABLE auth.resource_server_perm_ticket OWNER TO "auth-db-user";

--
-- Name: resource_server_policy; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.resource_server_policy (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    description character varying(255),
    type character varying(255) NOT NULL,
    decision_strategy character varying(20),
    logic character varying(20),
    resource_server_id character varying(36) NOT NULL,
    owner character varying(36)
);


ALTER TABLE auth.resource_server_policy OWNER TO "auth-db-user";

--
-- Name: resource_server_resource; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.resource_server_resource (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    type character varying(255),
    icon_uri character varying(255),
    owner character varying(36) NOT NULL,
    resource_server_id character varying(36) NOT NULL,
    owner_managed_access boolean DEFAULT false NOT NULL,
    display_name character varying(255)
);


ALTER TABLE auth.resource_server_resource OWNER TO "auth-db-user";

--
-- Name: resource_server_scope; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.resource_server_scope (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    icon_uri character varying(255),
    resource_server_id character varying(36) NOT NULL,
    display_name character varying(255)
);


ALTER TABLE auth.resource_server_scope OWNER TO "auth-db-user";

--
-- Name: resource_uris; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.resource_uris (
    resource_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE auth.resource_uris OWNER TO "auth-db-user";

--
-- Name: role_attribute; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.role_attribute (
    id character varying(36) NOT NULL,
    role_id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(255)
);


ALTER TABLE auth.role_attribute OWNER TO "auth-db-user";

--
-- Name: scope_mapping; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.scope_mapping (
    client_id character varying(36) NOT NULL,
    role_id character varying(36) NOT NULL
);


ALTER TABLE auth.scope_mapping OWNER TO "auth-db-user";

--
-- Name: scope_policy; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.scope_policy (
    scope_id character varying(36) NOT NULL,
    policy_id character varying(36) NOT NULL
);


ALTER TABLE auth.scope_policy OWNER TO "auth-db-user";

--
-- Name: user_attribute; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.user_attribute (
    name character varying(255) NOT NULL,
    value character varying(255),
    user_id character varying(36) NOT NULL,
    id character varying(36) DEFAULT 'sybase-needs-something-here'::character varying NOT NULL
);


ALTER TABLE auth.user_attribute OWNER TO "auth-db-user";

--
-- Name: user_consent; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.user_consent (
    id character varying(36) NOT NULL,
    client_id character varying(36),
    user_id character varying(36) NOT NULL,
    created_date bigint,
    last_updated_date bigint,
    client_storage_provider character varying(36),
    external_client_id character varying(255)
);


ALTER TABLE auth.user_consent OWNER TO "auth-db-user";

--
-- Name: user_consent_client_scope; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.user_consent_client_scope (
    user_consent_id character varying(36) NOT NULL,
    scope_id character varying(36) NOT NULL
);


ALTER TABLE auth.user_consent_client_scope OWNER TO "auth-db-user";

--
-- Name: user_entity; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.user_entity (
    id character varying(36) NOT NULL,
    email character varying(255),
    email_constraint character varying(255),
    email_verified boolean DEFAULT false NOT NULL,
    enabled boolean DEFAULT false NOT NULL,
    federation_link character varying(255),
    first_name character varying(255),
    last_name character varying(255),
    realm_id character varying(255),
    username character varying(255),
    created_timestamp bigint,
    service_account_client_link character varying(36),
    not_before integer DEFAULT 0 NOT NULL
);


ALTER TABLE auth.user_entity OWNER TO "auth-db-user";

--
-- Name: user_federation_config; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.user_federation_config (
    user_federation_provider_id character varying(36) NOT NULL,
    value character varying(255),
    name character varying(255) NOT NULL
);


ALTER TABLE auth.user_federation_config OWNER TO "auth-db-user";

--
-- Name: user_federation_mapper; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.user_federation_mapper (
    id character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    federation_provider_id character varying(36) NOT NULL,
    federation_mapper_type character varying(255) NOT NULL,
    realm_id character varying(36) NOT NULL
);


ALTER TABLE auth.user_federation_mapper OWNER TO "auth-db-user";

--
-- Name: user_federation_mapper_config; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.user_federation_mapper_config (
    user_federation_mapper_id character varying(36) NOT NULL,
    value character varying(255),
    name character varying(255) NOT NULL
);


ALTER TABLE auth.user_federation_mapper_config OWNER TO "auth-db-user";

--
-- Name: user_federation_provider; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.user_federation_provider (
    id character varying(36) NOT NULL,
    changed_sync_period integer,
    display_name character varying(255),
    full_sync_period integer,
    last_sync integer,
    priority integer,
    provider_name character varying(255),
    realm_id character varying(36)
);


ALTER TABLE auth.user_federation_provider OWNER TO "auth-db-user";

--
-- Name: user_group_membership; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.user_group_membership (
    group_id character varying(36) NOT NULL,
    user_id character varying(36) NOT NULL
);


ALTER TABLE auth.user_group_membership OWNER TO "auth-db-user";

--
-- Name: user_required_action; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.user_required_action (
    user_id character varying(36) NOT NULL,
    required_action character varying(255) DEFAULT ' '::character varying NOT NULL
);


ALTER TABLE auth.user_required_action OWNER TO "auth-db-user";

--
-- Name: user_role_mapping; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.user_role_mapping (
    role_id character varying(255) NOT NULL,
    user_id character varying(36) NOT NULL
);


ALTER TABLE auth.user_role_mapping OWNER TO "auth-db-user";

--
-- Name: user_session; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.user_session (
    id character varying(36) NOT NULL,
    auth_method character varying(255),
    ip_address character varying(255),
    last_session_refresh integer,
    login_username character varying(255),
    realm_id character varying(255),
    remember_me boolean DEFAULT false NOT NULL,
    started integer,
    user_id character varying(255),
    user_session_state integer,
    broker_session_id character varying(255),
    broker_user_id character varying(255)
);


ALTER TABLE auth.user_session OWNER TO "auth-db-user";

--
-- Name: user_session_note; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.user_session_note (
    user_session character varying(36) NOT NULL,
    name character varying(255) NOT NULL,
    value character varying(2048)
);


ALTER TABLE auth.user_session_note OWNER TO "auth-db-user";

--
-- Name: username_login_failure; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.username_login_failure (
    realm_id character varying(36) NOT NULL,
    username character varying(255) NOT NULL,
    failed_login_not_before integer,
    last_failure bigint,
    last_ip_failure character varying(255),
    num_failures integer
);


ALTER TABLE auth.username_login_failure OWNER TO "auth-db-user";

--
-- Name: web_origins; Type: TABLE; Schema: auth; Owner: auth-db-user
--

CREATE TABLE auth.web_origins (
    client_id character varying(36) NOT NULL,
    value character varying(255) NOT NULL
);


ALTER TABLE auth.web_origins OWNER TO "auth-db-user";

--
-- Name: username_login_failure CONSTRAINT_17-2; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.username_login_failure
    ADD CONSTRAINT "CONSTRAINT_17-2" PRIMARY KEY (realm_id, username);


--
-- Name: keycloak_role UK_J3RWUVD56ONTGSUHOGM184WW2-2; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.keycloak_role
    ADD CONSTRAINT "UK_J3RWUVD56ONTGSUHOGM184WW2-2" UNIQUE (name, client_realm_constraint);


--
-- Name: client_auth_flow_bindings c_cli_flow_bind; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.client_auth_flow_bindings
    ADD CONSTRAINT c_cli_flow_bind PRIMARY KEY (client_id, binding_name);


--
-- Name: client_scope_client c_cli_scope_bind; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.client_scope_client
    ADD CONSTRAINT c_cli_scope_bind PRIMARY KEY (client_id, scope_id);


--
-- Name: client_initial_access cnstr_client_init_acc_pk; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.client_initial_access
    ADD CONSTRAINT cnstr_client_init_acc_pk PRIMARY KEY (id);


--
-- Name: realm_default_groups con_group_id_def_groups; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.realm_default_groups
    ADD CONSTRAINT con_group_id_def_groups UNIQUE (group_id);


--
-- Name: broker_link constr_broker_link_pk; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.broker_link
    ADD CONSTRAINT constr_broker_link_pk PRIMARY KEY (identity_provider, user_id);


--
-- Name: client_user_session_note constr_cl_usr_ses_note; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.client_user_session_note
    ADD CONSTRAINT constr_cl_usr_ses_note PRIMARY KEY (client_session, name);


--
-- Name: client_default_roles constr_client_default_roles; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.client_default_roles
    ADD CONSTRAINT constr_client_default_roles PRIMARY KEY (client_id, role_id);


--
-- Name: component_config constr_component_config_pk; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.component_config
    ADD CONSTRAINT constr_component_config_pk PRIMARY KEY (id);


--
-- Name: component constr_component_pk; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.component
    ADD CONSTRAINT constr_component_pk PRIMARY KEY (id);


--
-- Name: fed_user_required_action constr_fed_required_action; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.fed_user_required_action
    ADD CONSTRAINT constr_fed_required_action PRIMARY KEY (required_action, user_id);


--
-- Name: fed_user_attribute constr_fed_user_attr_pk; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.fed_user_attribute
    ADD CONSTRAINT constr_fed_user_attr_pk PRIMARY KEY (id);


--
-- Name: fed_user_consent constr_fed_user_consent_pk; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.fed_user_consent
    ADD CONSTRAINT constr_fed_user_consent_pk PRIMARY KEY (id);


--
-- Name: fed_user_credential constr_fed_user_cred_pk; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.fed_user_credential
    ADD CONSTRAINT constr_fed_user_cred_pk PRIMARY KEY (id);


--
-- Name: fed_user_group_membership constr_fed_user_group; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.fed_user_group_membership
    ADD CONSTRAINT constr_fed_user_group PRIMARY KEY (group_id, user_id);


--
-- Name: fed_user_role_mapping constr_fed_user_role; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.fed_user_role_mapping
    ADD CONSTRAINT constr_fed_user_role PRIMARY KEY (role_id, user_id);


--
-- Name: federated_user constr_federated_user; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.federated_user
    ADD CONSTRAINT constr_federated_user PRIMARY KEY (id);


--
-- Name: realm_default_groups constr_realm_default_groups; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.realm_default_groups
    ADD CONSTRAINT constr_realm_default_groups PRIMARY KEY (realm_id, group_id);


--
-- Name: realm_enabled_event_types constr_realm_enabl_event_types; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.realm_enabled_event_types
    ADD CONSTRAINT constr_realm_enabl_event_types PRIMARY KEY (realm_id, value);


--
-- Name: realm_events_listeners constr_realm_events_listeners; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.realm_events_listeners
    ADD CONSTRAINT constr_realm_events_listeners PRIMARY KEY (realm_id, value);


--
-- Name: realm_supported_locales constr_realm_supported_locales; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.realm_supported_locales
    ADD CONSTRAINT constr_realm_supported_locales PRIMARY KEY (realm_id, value);


--
-- Name: identity_provider constraint_2b; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.identity_provider
    ADD CONSTRAINT constraint_2b PRIMARY KEY (internal_id);


--
-- Name: client_attributes constraint_3c; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.client_attributes
    ADD CONSTRAINT constraint_3c PRIMARY KEY (client_id, name);


--
-- Name: event_entity constraint_4; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.event_entity
    ADD CONSTRAINT constraint_4 PRIMARY KEY (id);


--
-- Name: federated_identity constraint_40; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.federated_identity
    ADD CONSTRAINT constraint_40 PRIMARY KEY (identity_provider, user_id);


--
-- Name: realm constraint_4a; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.realm
    ADD CONSTRAINT constraint_4a PRIMARY KEY (id);


--
-- Name: client_session_role constraint_5; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.client_session_role
    ADD CONSTRAINT constraint_5 PRIMARY KEY (client_session, role_id);


--
-- Name: user_session constraint_57; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.user_session
    ADD CONSTRAINT constraint_57 PRIMARY KEY (id);


--
-- Name: user_federation_provider constraint_5c; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.user_federation_provider
    ADD CONSTRAINT constraint_5c PRIMARY KEY (id);


--
-- Name: client_session_note constraint_5e; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.client_session_note
    ADD CONSTRAINT constraint_5e PRIMARY KEY (client_session, name);


--
-- Name: client constraint_7; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.client
    ADD CONSTRAINT constraint_7 PRIMARY KEY (id);


--
-- Name: client_session constraint_8; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.client_session
    ADD CONSTRAINT constraint_8 PRIMARY KEY (id);


--
-- Name: scope_mapping constraint_81; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.scope_mapping
    ADD CONSTRAINT constraint_81 PRIMARY KEY (client_id, role_id);


--
-- Name: client_node_registrations constraint_84; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.client_node_registrations
    ADD CONSTRAINT constraint_84 PRIMARY KEY (client_id, name);


--
-- Name: realm_attribute constraint_9; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.realm_attribute
    ADD CONSTRAINT constraint_9 PRIMARY KEY (name, realm_id);


--
-- Name: realm_required_credential constraint_92; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.realm_required_credential
    ADD CONSTRAINT constraint_92 PRIMARY KEY (realm_id, type);


--
-- Name: keycloak_role constraint_a; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.keycloak_role
    ADD CONSTRAINT constraint_a PRIMARY KEY (id);


--
-- Name: admin_event_entity constraint_admin_event_entity; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.admin_event_entity
    ADD CONSTRAINT constraint_admin_event_entity PRIMARY KEY (id);


--
-- Name: authenticator_config_entry constraint_auth_cfg_pk; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.authenticator_config_entry
    ADD CONSTRAINT constraint_auth_cfg_pk PRIMARY KEY (authenticator_id, name);


--
-- Name: authentication_execution constraint_auth_exec_pk; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.authentication_execution
    ADD CONSTRAINT constraint_auth_exec_pk PRIMARY KEY (id);


--
-- Name: authentication_flow constraint_auth_flow_pk; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.authentication_flow
    ADD CONSTRAINT constraint_auth_flow_pk PRIMARY KEY (id);


--
-- Name: authenticator_config constraint_auth_pk; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.authenticator_config
    ADD CONSTRAINT constraint_auth_pk PRIMARY KEY (id);


--
-- Name: client_session_auth_status constraint_auth_status_pk; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.client_session_auth_status
    ADD CONSTRAINT constraint_auth_status_pk PRIMARY KEY (client_session, authenticator);


--
-- Name: user_role_mapping constraint_c; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.user_role_mapping
    ADD CONSTRAINT constraint_c PRIMARY KEY (role_id, user_id);


--
-- Name: composite_role constraint_composite_role; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.composite_role
    ADD CONSTRAINT constraint_composite_role PRIMARY KEY (composite, child_role);


--
-- Name: credential_attribute constraint_credential_attr; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.credential_attribute
    ADD CONSTRAINT constraint_credential_attr PRIMARY KEY (id);


--
-- Name: client_session_prot_mapper constraint_cs_pmp_pk; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.client_session_prot_mapper
    ADD CONSTRAINT constraint_cs_pmp_pk PRIMARY KEY (client_session, protocol_mapper_id);


--
-- Name: identity_provider_config constraint_d; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.identity_provider_config
    ADD CONSTRAINT constraint_d PRIMARY KEY (identity_provider_id, name);


--
-- Name: policy_config constraint_dpc; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.policy_config
    ADD CONSTRAINT constraint_dpc PRIMARY KEY (policy_id, name);


--
-- Name: realm_smtp_config constraint_e; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.realm_smtp_config
    ADD CONSTRAINT constraint_e PRIMARY KEY (realm_id, name);


--
-- Name: credential constraint_f; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.credential
    ADD CONSTRAINT constraint_f PRIMARY KEY (id);


--
-- Name: user_federation_config constraint_f9; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.user_federation_config
    ADD CONSTRAINT constraint_f9 PRIMARY KEY (user_federation_provider_id, name);


--
-- Name: resource_server_perm_ticket constraint_fapmt; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.resource_server_perm_ticket
    ADD CONSTRAINT constraint_fapmt PRIMARY KEY (id);


--
-- Name: resource_server_resource constraint_farsr; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.resource_server_resource
    ADD CONSTRAINT constraint_farsr PRIMARY KEY (id);


--
-- Name: resource_server_policy constraint_farsrp; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.resource_server_policy
    ADD CONSTRAINT constraint_farsrp PRIMARY KEY (id);


--
-- Name: associated_policy constraint_farsrpap; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.associated_policy
    ADD CONSTRAINT constraint_farsrpap PRIMARY KEY (policy_id, associated_policy_id);


--
-- Name: resource_policy constraint_farsrpp; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.resource_policy
    ADD CONSTRAINT constraint_farsrpp PRIMARY KEY (resource_id, policy_id);


--
-- Name: resource_server_scope constraint_farsrs; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.resource_server_scope
    ADD CONSTRAINT constraint_farsrs PRIMARY KEY (id);


--
-- Name: resource_scope constraint_farsrsp; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.resource_scope
    ADD CONSTRAINT constraint_farsrsp PRIMARY KEY (resource_id, scope_id);


--
-- Name: scope_policy constraint_farsrsps; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.scope_policy
    ADD CONSTRAINT constraint_farsrsps PRIMARY KEY (scope_id, policy_id);


--
-- Name: user_entity constraint_fb; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.user_entity
    ADD CONSTRAINT constraint_fb PRIMARY KEY (id);


--
-- Name: fed_credential_attribute constraint_fed_credential_attr; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.fed_credential_attribute
    ADD CONSTRAINT constraint_fed_credential_attr PRIMARY KEY (id);


--
-- Name: user_federation_mapper_config constraint_fedmapper_cfg_pm; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.user_federation_mapper_config
    ADD CONSTRAINT constraint_fedmapper_cfg_pm PRIMARY KEY (user_federation_mapper_id, name);


--
-- Name: user_federation_mapper constraint_fedmapperpm; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.user_federation_mapper
    ADD CONSTRAINT constraint_fedmapperpm PRIMARY KEY (id);


--
-- Name: fed_user_consent_cl_scope constraint_fgrntcsnt_clsc_pm; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.fed_user_consent_cl_scope
    ADD CONSTRAINT constraint_fgrntcsnt_clsc_pm PRIMARY KEY (user_consent_id, scope_id);


--
-- Name: user_consent_client_scope constraint_grntcsnt_clsc_pm; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.user_consent_client_scope
    ADD CONSTRAINT constraint_grntcsnt_clsc_pm PRIMARY KEY (user_consent_id, scope_id);


--
-- Name: user_consent constraint_grntcsnt_pm; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.user_consent
    ADD CONSTRAINT constraint_grntcsnt_pm PRIMARY KEY (id);


--
-- Name: keycloak_group constraint_group; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.keycloak_group
    ADD CONSTRAINT constraint_group PRIMARY KEY (id);


--
-- Name: group_attribute constraint_group_attribute_pk; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.group_attribute
    ADD CONSTRAINT constraint_group_attribute_pk PRIMARY KEY (id);


--
-- Name: group_role_mapping constraint_group_role; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.group_role_mapping
    ADD CONSTRAINT constraint_group_role PRIMARY KEY (role_id, group_id);


--
-- Name: identity_provider_mapper constraint_idpm; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.identity_provider_mapper
    ADD CONSTRAINT constraint_idpm PRIMARY KEY (id);


--
-- Name: idp_mapper_config constraint_idpmconfig; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.idp_mapper_config
    ADD CONSTRAINT constraint_idpmconfig PRIMARY KEY (idp_mapper_id, name);


--
-- Name: migration_model constraint_migmod; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.migration_model
    ADD CONSTRAINT constraint_migmod PRIMARY KEY (id);


--
-- Name: offline_client_session constraint_offl_cl_ses_pk3; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.offline_client_session
    ADD CONSTRAINT constraint_offl_cl_ses_pk3 PRIMARY KEY (user_session_id, client_id, client_storage_provider, external_client_id, offline_flag);


--
-- Name: offline_user_session constraint_offl_us_ses_pk2; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.offline_user_session
    ADD CONSTRAINT constraint_offl_us_ses_pk2 PRIMARY KEY (user_session_id, offline_flag);


--
-- Name: protocol_mapper constraint_pcm; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.protocol_mapper
    ADD CONSTRAINT constraint_pcm PRIMARY KEY (id);


--
-- Name: protocol_mapper_config constraint_pmconfig; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.protocol_mapper_config
    ADD CONSTRAINT constraint_pmconfig PRIMARY KEY (protocol_mapper_id, name);


--
-- Name: realm_default_roles constraint_realm_default_roles; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.realm_default_roles
    ADD CONSTRAINT constraint_realm_default_roles PRIMARY KEY (realm_id, role_id);


--
-- Name: redirect_uris constraint_redirect_uris; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.redirect_uris
    ADD CONSTRAINT constraint_redirect_uris PRIMARY KEY (client_id, value);


--
-- Name: required_action_config constraint_req_act_cfg_pk; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.required_action_config
    ADD CONSTRAINT constraint_req_act_cfg_pk PRIMARY KEY (required_action_id, name);


--
-- Name: required_action_provider constraint_req_act_prv_pk; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.required_action_provider
    ADD CONSTRAINT constraint_req_act_prv_pk PRIMARY KEY (id);


--
-- Name: user_required_action constraint_required_action; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.user_required_action
    ADD CONSTRAINT constraint_required_action PRIMARY KEY (required_action, user_id);


--
-- Name: role_attribute constraint_role_attribute_pk; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.role_attribute
    ADD CONSTRAINT constraint_role_attribute_pk PRIMARY KEY (id);


--
-- Name: user_attribute constraint_user_attribute_pk; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.user_attribute
    ADD CONSTRAINT constraint_user_attribute_pk PRIMARY KEY (id);


--
-- Name: user_group_membership constraint_user_group; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.user_group_membership
    ADD CONSTRAINT constraint_user_group PRIMARY KEY (group_id, user_id);


--
-- Name: user_session_note constraint_usn_pk; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.user_session_note
    ADD CONSTRAINT constraint_usn_pk PRIMARY KEY (user_session, name);


--
-- Name: web_origins constraint_web_origins; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.web_origins
    ADD CONSTRAINT constraint_web_origins PRIMARY KEY (client_id, value);


--
-- Name: client_scope_attributes pk_cl_tmpl_attr; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.client_scope_attributes
    ADD CONSTRAINT pk_cl_tmpl_attr PRIMARY KEY (scope_id, name);


--
-- Name: client_scope pk_cli_template; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.client_scope
    ADD CONSTRAINT pk_cli_template PRIMARY KEY (id);


--
-- Name: databasechangeloglock pk_databasechangeloglock; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.databasechangeloglock
    ADD CONSTRAINT pk_databasechangeloglock PRIMARY KEY (id);


--
-- Name: resource_server pk_resource_server; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.resource_server
    ADD CONSTRAINT pk_resource_server PRIMARY KEY (id);


--
-- Name: client_scope_role_mapping pk_template_scope; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.client_scope_role_mapping
    ADD CONSTRAINT pk_template_scope PRIMARY KEY (scope_id, role_id);


--
-- Name: default_client_scope r_def_cli_scope_bind; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.default_client_scope
    ADD CONSTRAINT r_def_cli_scope_bind PRIMARY KEY (realm_id, scope_id);


--
-- Name: resource_attribute res_attr_pk; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.resource_attribute
    ADD CONSTRAINT res_attr_pk PRIMARY KEY (id);


--
-- Name: keycloak_group sibling_names; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.keycloak_group
    ADD CONSTRAINT sibling_names UNIQUE (realm_id, parent_group, name);


--
-- Name: identity_provider uk_2daelwnibji49avxsrtuf6xj33; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.identity_provider
    ADD CONSTRAINT uk_2daelwnibji49avxsrtuf6xj33 UNIQUE (provider_alias, realm_id);


--
-- Name: client_default_roles uk_8aelwnibji49avxsrtuf6xjow; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.client_default_roles
    ADD CONSTRAINT uk_8aelwnibji49avxsrtuf6xjow UNIQUE (role_id);


--
-- Name: client uk_b71cjlbenv945rb6gcon438at; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.client
    ADD CONSTRAINT uk_b71cjlbenv945rb6gcon438at UNIQUE (realm_id, client_id);


--
-- Name: client_scope uk_cli_scope; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.client_scope
    ADD CONSTRAINT uk_cli_scope UNIQUE (realm_id, name);


--
-- Name: user_entity uk_dykn684sl8up1crfei6eckhd7; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.user_entity
    ADD CONSTRAINT uk_dykn684sl8up1crfei6eckhd7 UNIQUE (realm_id, email_constraint);


--
-- Name: resource_server_resource uk_frsr6t700s9v50bu18ws5ha6; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.resource_server_resource
    ADD CONSTRAINT uk_frsr6t700s9v50bu18ws5ha6 UNIQUE (name, owner, resource_server_id);


--
-- Name: resource_server_perm_ticket uk_frsr6t700s9v50bu18ws5pmt; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.resource_server_perm_ticket
    ADD CONSTRAINT uk_frsr6t700s9v50bu18ws5pmt UNIQUE (owner, requester, resource_server_id, resource_id, scope_id);


--
-- Name: resource_server_policy uk_frsrpt700s9v50bu18ws5ha6; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.resource_server_policy
    ADD CONSTRAINT uk_frsrpt700s9v50bu18ws5ha6 UNIQUE (name, resource_server_id);


--
-- Name: resource_server_scope uk_frsrst700s9v50bu18ws5ha6; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.resource_server_scope
    ADD CONSTRAINT uk_frsrst700s9v50bu18ws5ha6 UNIQUE (name, resource_server_id);


--
-- Name: realm_default_roles uk_h4wpd7w4hsoolni3h0sw7btje; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.realm_default_roles
    ADD CONSTRAINT uk_h4wpd7w4hsoolni3h0sw7btje UNIQUE (role_id);


--
-- Name: user_consent uk_jkuwuvd56ontgsuhogm8uewrt; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.user_consent
    ADD CONSTRAINT uk_jkuwuvd56ontgsuhogm8uewrt UNIQUE (client_id, client_storage_provider, external_client_id, user_id);


--
-- Name: realm uk_orvsdmla56612eaefiq6wl5oi; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.realm
    ADD CONSTRAINT uk_orvsdmla56612eaefiq6wl5oi UNIQUE (name);


--
-- Name: user_entity uk_ru8tt6t700s9v50bu18ws5ha6; Type: CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.user_entity
    ADD CONSTRAINT uk_ru8tt6t700s9v50bu18ws5ha6 UNIQUE (realm_id, username);


--
-- Name: idx_assoc_pol_assoc_pol_id; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_assoc_pol_assoc_pol_id ON auth.associated_policy USING btree (associated_policy_id);


--
-- Name: idx_auth_config_realm; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_auth_config_realm ON auth.authenticator_config USING btree (realm_id);


--
-- Name: idx_auth_exec_flow; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_auth_exec_flow ON auth.authentication_execution USING btree (flow_id);


--
-- Name: idx_auth_exec_realm_flow; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_auth_exec_realm_flow ON auth.authentication_execution USING btree (realm_id, flow_id);


--
-- Name: idx_auth_flow_realm; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_auth_flow_realm ON auth.authentication_flow USING btree (realm_id);


--
-- Name: idx_cl_clscope; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_cl_clscope ON auth.client_scope_client USING btree (scope_id);


--
-- Name: idx_client_def_roles_client; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_client_def_roles_client ON auth.client_default_roles USING btree (client_id);


--
-- Name: idx_client_init_acc_realm; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_client_init_acc_realm ON auth.client_initial_access USING btree (realm_id);


--
-- Name: idx_client_session_session; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_client_session_session ON auth.client_session USING btree (session_id);


--
-- Name: idx_clscope_attrs; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_clscope_attrs ON auth.client_scope_attributes USING btree (scope_id);


--
-- Name: idx_clscope_cl; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_clscope_cl ON auth.client_scope_client USING btree (client_id);


--
-- Name: idx_clscope_protmap; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_clscope_protmap ON auth.protocol_mapper USING btree (client_scope_id);


--
-- Name: idx_clscope_role; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_clscope_role ON auth.client_scope_role_mapping USING btree (scope_id);


--
-- Name: idx_compo_config_compo; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_compo_config_compo ON auth.component_config USING btree (component_id);


--
-- Name: idx_component_provider_type; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_component_provider_type ON auth.component USING btree (provider_type);


--
-- Name: idx_component_realm; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_component_realm ON auth.component USING btree (realm_id);


--
-- Name: idx_composite; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_composite ON auth.composite_role USING btree (composite);


--
-- Name: idx_composite_child; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_composite_child ON auth.composite_role USING btree (child_role);


--
-- Name: idx_credential_attr_cred; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_credential_attr_cred ON auth.credential_attribute USING btree (credential_id);


--
-- Name: idx_defcls_realm; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_defcls_realm ON auth.default_client_scope USING btree (realm_id);


--
-- Name: idx_defcls_scope; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_defcls_scope ON auth.default_client_scope USING btree (scope_id);


--
-- Name: idx_fed_cred_attr_cred; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_fed_cred_attr_cred ON auth.fed_credential_attribute USING btree (credential_id);


--
-- Name: idx_fedidentity_feduser; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_fedidentity_feduser ON auth.federated_identity USING btree (federated_user_id);


--
-- Name: idx_fedidentity_user; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_fedidentity_user ON auth.federated_identity USING btree (user_id);


--
-- Name: idx_fu_attribute; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_fu_attribute ON auth.fed_user_attribute USING btree (user_id, realm_id, name);


--
-- Name: idx_fu_cnsnt_ext; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_fu_cnsnt_ext ON auth.fed_user_consent USING btree (user_id, client_storage_provider, external_client_id);


--
-- Name: idx_fu_consent; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_fu_consent ON auth.fed_user_consent USING btree (user_id, client_id);


--
-- Name: idx_fu_consent_ru; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_fu_consent_ru ON auth.fed_user_consent USING btree (realm_id, user_id);


--
-- Name: idx_fu_credential; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_fu_credential ON auth.fed_user_credential USING btree (user_id, type);


--
-- Name: idx_fu_credential_ru; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_fu_credential_ru ON auth.fed_user_credential USING btree (realm_id, user_id);


--
-- Name: idx_fu_group_membership; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_fu_group_membership ON auth.fed_user_group_membership USING btree (user_id, group_id);


--
-- Name: idx_fu_group_membership_ru; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_fu_group_membership_ru ON auth.fed_user_group_membership USING btree (realm_id, user_id);


--
-- Name: idx_fu_required_action; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_fu_required_action ON auth.fed_user_required_action USING btree (user_id, required_action);


--
-- Name: idx_fu_required_action_ru; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_fu_required_action_ru ON auth.fed_user_required_action USING btree (realm_id, user_id);


--
-- Name: idx_fu_role_mapping; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_fu_role_mapping ON auth.fed_user_role_mapping USING btree (user_id, role_id);


--
-- Name: idx_fu_role_mapping_ru; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_fu_role_mapping_ru ON auth.fed_user_role_mapping USING btree (realm_id, user_id);


--
-- Name: idx_group_attr_group; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_group_attr_group ON auth.group_attribute USING btree (group_id);


--
-- Name: idx_group_role_mapp_group; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_group_role_mapp_group ON auth.group_role_mapping USING btree (group_id);


--
-- Name: idx_id_prov_mapp_realm; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_id_prov_mapp_realm ON auth.identity_provider_mapper USING btree (realm_id);


--
-- Name: idx_ident_prov_realm; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_ident_prov_realm ON auth.identity_provider USING btree (realm_id);


--
-- Name: idx_keycloak_role_client; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_keycloak_role_client ON auth.keycloak_role USING btree (client);


--
-- Name: idx_keycloak_role_realm; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_keycloak_role_realm ON auth.keycloak_role USING btree (realm);


--
-- Name: idx_offline_uss_createdon; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_offline_uss_createdon ON auth.offline_user_session USING btree (created_on);


--
-- Name: idx_protocol_mapper_client; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_protocol_mapper_client ON auth.protocol_mapper USING btree (client_id);


--
-- Name: idx_realm_attr_realm; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_realm_attr_realm ON auth.realm_attribute USING btree (realm_id);


--
-- Name: idx_realm_clscope; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_realm_clscope ON auth.client_scope USING btree (realm_id);


--
-- Name: idx_realm_def_grp_realm; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_realm_def_grp_realm ON auth.realm_default_groups USING btree (realm_id);


--
-- Name: idx_realm_def_roles_realm; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_realm_def_roles_realm ON auth.realm_default_roles USING btree (realm_id);


--
-- Name: idx_realm_evt_list_realm; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_realm_evt_list_realm ON auth.realm_events_listeners USING btree (realm_id);


--
-- Name: idx_realm_evt_types_realm; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_realm_evt_types_realm ON auth.realm_enabled_event_types USING btree (realm_id);


--
-- Name: idx_realm_master_adm_cli; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_realm_master_adm_cli ON auth.realm USING btree (master_admin_client);


--
-- Name: idx_realm_supp_local_realm; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_realm_supp_local_realm ON auth.realm_supported_locales USING btree (realm_id);


--
-- Name: idx_redir_uri_client; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_redir_uri_client ON auth.redirect_uris USING btree (client_id);


--
-- Name: idx_req_act_prov_realm; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_req_act_prov_realm ON auth.required_action_provider USING btree (realm_id);


--
-- Name: idx_res_policy_policy; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_res_policy_policy ON auth.resource_policy USING btree (policy_id);


--
-- Name: idx_res_scope_scope; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_res_scope_scope ON auth.resource_scope USING btree (scope_id);


--
-- Name: idx_res_serv_pol_res_serv; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_res_serv_pol_res_serv ON auth.resource_server_policy USING btree (resource_server_id);


--
-- Name: idx_res_srv_res_res_srv; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_res_srv_res_res_srv ON auth.resource_server_resource USING btree (resource_server_id);


--
-- Name: idx_res_srv_scope_res_srv; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_res_srv_scope_res_srv ON auth.resource_server_scope USING btree (resource_server_id);


--
-- Name: idx_role_attribute; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_role_attribute ON auth.role_attribute USING btree (role_id);


--
-- Name: idx_role_clscope; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_role_clscope ON auth.client_scope_role_mapping USING btree (role_id);


--
-- Name: idx_scope_mapping_role; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_scope_mapping_role ON auth.scope_mapping USING btree (role_id);


--
-- Name: idx_scope_policy_policy; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_scope_policy_policy ON auth.scope_policy USING btree (policy_id);


--
-- Name: idx_us_sess_id_on_cl_sess; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_us_sess_id_on_cl_sess ON auth.offline_client_session USING btree (user_session_id);


--
-- Name: idx_usconsent_clscope; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_usconsent_clscope ON auth.user_consent_client_scope USING btree (user_consent_id);


--
-- Name: idx_user_attribute; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_user_attribute ON auth.user_attribute USING btree (user_id);


--
-- Name: idx_user_consent; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_user_consent ON auth.user_consent USING btree (user_id);


--
-- Name: idx_user_credential; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_user_credential ON auth.credential USING btree (user_id);


--
-- Name: idx_user_email; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_user_email ON auth.user_entity USING btree (email);


--
-- Name: idx_user_group_mapping; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_user_group_mapping ON auth.user_group_membership USING btree (user_id);


--
-- Name: idx_user_reqactions; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_user_reqactions ON auth.user_required_action USING btree (user_id);


--
-- Name: idx_user_role_mapping; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_user_role_mapping ON auth.user_role_mapping USING btree (user_id);


--
-- Name: idx_usr_fed_map_fed_prv; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_usr_fed_map_fed_prv ON auth.user_federation_mapper USING btree (federation_provider_id);


--
-- Name: idx_usr_fed_map_realm; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_usr_fed_map_realm ON auth.user_federation_mapper USING btree (realm_id);


--
-- Name: idx_usr_fed_prv_realm; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_usr_fed_prv_realm ON auth.user_federation_provider USING btree (realm_id);


--
-- Name: idx_web_orig_client; Type: INDEX; Schema: auth; Owner: auth-db-user
--

CREATE INDEX idx_web_orig_client ON auth.web_origins USING btree (client_id);


--
-- Name: client_session_auth_status auth_status_constraint; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.client_session_auth_status
    ADD CONSTRAINT auth_status_constraint FOREIGN KEY (client_session) REFERENCES auth.client_session(id);


--
-- Name: identity_provider fk2b4ebc52ae5c3b34; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.identity_provider
    ADD CONSTRAINT fk2b4ebc52ae5c3b34 FOREIGN KEY (realm_id) REFERENCES auth.realm(id);


--
-- Name: client_attributes fk3c47c64beacca966; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.client_attributes
    ADD CONSTRAINT fk3c47c64beacca966 FOREIGN KEY (client_id) REFERENCES auth.client(id);


--
-- Name: federated_identity fk404288b92ef007a6; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.federated_identity
    ADD CONSTRAINT fk404288b92ef007a6 FOREIGN KEY (user_id) REFERENCES auth.user_entity(id);


--
-- Name: client_node_registrations fk4129723ba992f594; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.client_node_registrations
    ADD CONSTRAINT fk4129723ba992f594 FOREIGN KEY (client_id) REFERENCES auth.client(id);


--
-- Name: client_session_note fk5edfb00ff51c2736; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.client_session_note
    ADD CONSTRAINT fk5edfb00ff51c2736 FOREIGN KEY (client_session) REFERENCES auth.client_session(id);


--
-- Name: user_session_note fk5edfb00ff51d3472; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.user_session_note
    ADD CONSTRAINT fk5edfb00ff51d3472 FOREIGN KEY (user_session) REFERENCES auth.user_session(id);


--
-- Name: client_session_role fk_11b7sgqw18i532811v7o2dv76; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.client_session_role
    ADD CONSTRAINT fk_11b7sgqw18i532811v7o2dv76 FOREIGN KEY (client_session) REFERENCES auth.client_session(id);


--
-- Name: redirect_uris fk_1burs8pb4ouj97h5wuppahv9f; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.redirect_uris
    ADD CONSTRAINT fk_1burs8pb4ouj97h5wuppahv9f FOREIGN KEY (client_id) REFERENCES auth.client(id);


--
-- Name: user_federation_provider fk_1fj32f6ptolw2qy60cd8n01e8; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.user_federation_provider
    ADD CONSTRAINT fk_1fj32f6ptolw2qy60cd8n01e8 FOREIGN KEY (realm_id) REFERENCES auth.realm(id);


--
-- Name: client_session_prot_mapper fk_33a8sgqw18i532811v7o2dk89; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.client_session_prot_mapper
    ADD CONSTRAINT fk_33a8sgqw18i532811v7o2dk89 FOREIGN KEY (client_session) REFERENCES auth.client_session(id);


--
-- Name: realm_required_credential fk_5hg65lybevavkqfki3kponh9v; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.realm_required_credential
    ADD CONSTRAINT fk_5hg65lybevavkqfki3kponh9v FOREIGN KEY (realm_id) REFERENCES auth.realm(id);


--
-- Name: resource_attribute fk_5hrm2vlf9ql5fu022kqepovbr; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.resource_attribute
    ADD CONSTRAINT fk_5hrm2vlf9ql5fu022kqepovbr FOREIGN KEY (resource_id) REFERENCES auth.resource_server_resource(id);


--
-- Name: user_attribute fk_5hrm2vlf9ql5fu043kqepovbr; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.user_attribute
    ADD CONSTRAINT fk_5hrm2vlf9ql5fu043kqepovbr FOREIGN KEY (user_id) REFERENCES auth.user_entity(id);


--
-- Name: user_required_action fk_6qj3w1jw9cvafhe19bwsiuvmd; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.user_required_action
    ADD CONSTRAINT fk_6qj3w1jw9cvafhe19bwsiuvmd FOREIGN KEY (user_id) REFERENCES auth.user_entity(id);


--
-- Name: keycloak_role fk_6vyqfe4cn4wlq8r6kt5vdsj5c; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.keycloak_role
    ADD CONSTRAINT fk_6vyqfe4cn4wlq8r6kt5vdsj5c FOREIGN KEY (realm) REFERENCES auth.realm(id);


--
-- Name: realm_smtp_config fk_70ej8xdxgxd0b9hh6180irr0o; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.realm_smtp_config
    ADD CONSTRAINT fk_70ej8xdxgxd0b9hh6180irr0o FOREIGN KEY (realm_id) REFERENCES auth.realm(id);


--
-- Name: client_default_roles fk_8aelwnibji49avxsrtuf6xjow; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.client_default_roles
    ADD CONSTRAINT fk_8aelwnibji49avxsrtuf6xjow FOREIGN KEY (role_id) REFERENCES auth.keycloak_role(id);


--
-- Name: realm_attribute fk_8shxd6l3e9atqukacxgpffptw; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.realm_attribute
    ADD CONSTRAINT fk_8shxd6l3e9atqukacxgpffptw FOREIGN KEY (realm_id) REFERENCES auth.realm(id);


--
-- Name: composite_role fk_a63wvekftu8jo1pnj81e7mce2; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.composite_role
    ADD CONSTRAINT fk_a63wvekftu8jo1pnj81e7mce2 FOREIGN KEY (composite) REFERENCES auth.keycloak_role(id);


--
-- Name: authentication_execution fk_auth_exec_flow; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.authentication_execution
    ADD CONSTRAINT fk_auth_exec_flow FOREIGN KEY (flow_id) REFERENCES auth.authentication_flow(id);


--
-- Name: authentication_execution fk_auth_exec_realm; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.authentication_execution
    ADD CONSTRAINT fk_auth_exec_realm FOREIGN KEY (realm_id) REFERENCES auth.realm(id);


--
-- Name: authentication_flow fk_auth_flow_realm; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.authentication_flow
    ADD CONSTRAINT fk_auth_flow_realm FOREIGN KEY (realm_id) REFERENCES auth.realm(id);


--
-- Name: authenticator_config fk_auth_realm; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.authenticator_config
    ADD CONSTRAINT fk_auth_realm FOREIGN KEY (realm_id) REFERENCES auth.realm(id);


--
-- Name: client_session fk_b4ao2vcvat6ukau74wbwtfqo1; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.client_session
    ADD CONSTRAINT fk_b4ao2vcvat6ukau74wbwtfqo1 FOREIGN KEY (session_id) REFERENCES auth.user_session(id);


--
-- Name: user_role_mapping fk_c4fqv34p1mbylloxang7b1q3l; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.user_role_mapping
    ADD CONSTRAINT fk_c4fqv34p1mbylloxang7b1q3l FOREIGN KEY (user_id) REFERENCES auth.user_entity(id);


--
-- Name: client_scope_client fk_c_cli_scope_client; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.client_scope_client
    ADD CONSTRAINT fk_c_cli_scope_client FOREIGN KEY (client_id) REFERENCES auth.client(id);


--
-- Name: client_scope_client fk_c_cli_scope_scope; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.client_scope_client
    ADD CONSTRAINT fk_c_cli_scope_scope FOREIGN KEY (scope_id) REFERENCES auth.client_scope(id);


--
-- Name: client_scope_attributes fk_cl_scope_attr_scope; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.client_scope_attributes
    ADD CONSTRAINT fk_cl_scope_attr_scope FOREIGN KEY (scope_id) REFERENCES auth.client_scope(id);


--
-- Name: client_scope_role_mapping fk_cl_scope_rm_role; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.client_scope_role_mapping
    ADD CONSTRAINT fk_cl_scope_rm_role FOREIGN KEY (role_id) REFERENCES auth.keycloak_role(id);


--
-- Name: client_scope_role_mapping fk_cl_scope_rm_scope; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.client_scope_role_mapping
    ADD CONSTRAINT fk_cl_scope_rm_scope FOREIGN KEY (scope_id) REFERENCES auth.client_scope(id);


--
-- Name: client_user_session_note fk_cl_usr_ses_note; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.client_user_session_note
    ADD CONSTRAINT fk_cl_usr_ses_note FOREIGN KEY (client_session) REFERENCES auth.client_session(id);


--
-- Name: protocol_mapper fk_cli_scope_mapper; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.protocol_mapper
    ADD CONSTRAINT fk_cli_scope_mapper FOREIGN KEY (client_scope_id) REFERENCES auth.client_scope(id);


--
-- Name: client_initial_access fk_client_init_acc_realm; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.client_initial_access
    ADD CONSTRAINT fk_client_init_acc_realm FOREIGN KEY (realm_id) REFERENCES auth.realm(id);


--
-- Name: component_config fk_component_config; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.component_config
    ADD CONSTRAINT fk_component_config FOREIGN KEY (component_id) REFERENCES auth.component(id);


--
-- Name: component fk_component_realm; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.component
    ADD CONSTRAINT fk_component_realm FOREIGN KEY (realm_id) REFERENCES auth.realm(id);


--
-- Name: credential_attribute fk_cred_attr; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.credential_attribute
    ADD CONSTRAINT fk_cred_attr FOREIGN KEY (credential_id) REFERENCES auth.credential(id);


--
-- Name: realm_default_groups fk_def_groups_group; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.realm_default_groups
    ADD CONSTRAINT fk_def_groups_group FOREIGN KEY (group_id) REFERENCES auth.keycloak_group(id);


--
-- Name: realm_default_groups fk_def_groups_realm; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.realm_default_groups
    ADD CONSTRAINT fk_def_groups_realm FOREIGN KEY (realm_id) REFERENCES auth.realm(id);


--
-- Name: realm_default_roles fk_evudb1ppw84oxfax2drs03icc; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.realm_default_roles
    ADD CONSTRAINT fk_evudb1ppw84oxfax2drs03icc FOREIGN KEY (realm_id) REFERENCES auth.realm(id);


--
-- Name: fed_credential_attribute fk_fed_cred_attr; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.fed_credential_attribute
    ADD CONSTRAINT fk_fed_cred_attr FOREIGN KEY (credential_id) REFERENCES auth.fed_user_credential(id);


--
-- Name: user_federation_mapper_config fk_fedmapper_cfg; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.user_federation_mapper_config
    ADD CONSTRAINT fk_fedmapper_cfg FOREIGN KEY (user_federation_mapper_id) REFERENCES auth.user_federation_mapper(id);


--
-- Name: user_federation_mapper fk_fedmapperpm_fedprv; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.user_federation_mapper
    ADD CONSTRAINT fk_fedmapperpm_fedprv FOREIGN KEY (federation_provider_id) REFERENCES auth.user_federation_provider(id);


--
-- Name: user_federation_mapper fk_fedmapperpm_realm; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.user_federation_mapper
    ADD CONSTRAINT fk_fedmapperpm_realm FOREIGN KEY (realm_id) REFERENCES auth.realm(id);


--
-- Name: associated_policy fk_frsr5s213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.associated_policy
    ADD CONSTRAINT fk_frsr5s213xcx4wnkog82ssrfy FOREIGN KEY (associated_policy_id) REFERENCES auth.resource_server_policy(id);


--
-- Name: scope_policy fk_frsrasp13xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.scope_policy
    ADD CONSTRAINT fk_frsrasp13xcx4wnkog82ssrfy FOREIGN KEY (policy_id) REFERENCES auth.resource_server_policy(id);


--
-- Name: resource_server_perm_ticket fk_frsrho213xcx4wnkog82sspmt; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.resource_server_perm_ticket
    ADD CONSTRAINT fk_frsrho213xcx4wnkog82sspmt FOREIGN KEY (resource_server_id) REFERENCES auth.resource_server(id);


--
-- Name: resource_server_resource fk_frsrho213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.resource_server_resource
    ADD CONSTRAINT fk_frsrho213xcx4wnkog82ssrfy FOREIGN KEY (resource_server_id) REFERENCES auth.resource_server(id);


--
-- Name: resource_server_perm_ticket fk_frsrho213xcx4wnkog83sspmt; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.resource_server_perm_ticket
    ADD CONSTRAINT fk_frsrho213xcx4wnkog83sspmt FOREIGN KEY (resource_id) REFERENCES auth.resource_server_resource(id);


--
-- Name: resource_server_perm_ticket fk_frsrho213xcx4wnkog84sspmt; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.resource_server_perm_ticket
    ADD CONSTRAINT fk_frsrho213xcx4wnkog84sspmt FOREIGN KEY (scope_id) REFERENCES auth.resource_server_scope(id);


--
-- Name: associated_policy fk_frsrpas14xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.associated_policy
    ADD CONSTRAINT fk_frsrpas14xcx4wnkog82ssrfy FOREIGN KEY (policy_id) REFERENCES auth.resource_server_policy(id);


--
-- Name: scope_policy fk_frsrpass3xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.scope_policy
    ADD CONSTRAINT fk_frsrpass3xcx4wnkog82ssrfy FOREIGN KEY (scope_id) REFERENCES auth.resource_server_scope(id);


--
-- Name: resource_server_perm_ticket fk_frsrpo2128cx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.resource_server_perm_ticket
    ADD CONSTRAINT fk_frsrpo2128cx4wnkog82ssrfy FOREIGN KEY (policy_id) REFERENCES auth.resource_server_policy(id);


--
-- Name: resource_server_policy fk_frsrpo213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.resource_server_policy
    ADD CONSTRAINT fk_frsrpo213xcx4wnkog82ssrfy FOREIGN KEY (resource_server_id) REFERENCES auth.resource_server(id);


--
-- Name: resource_scope fk_frsrpos13xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.resource_scope
    ADD CONSTRAINT fk_frsrpos13xcx4wnkog82ssrfy FOREIGN KEY (resource_id) REFERENCES auth.resource_server_resource(id);


--
-- Name: resource_policy fk_frsrpos53xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.resource_policy
    ADD CONSTRAINT fk_frsrpos53xcx4wnkog82ssrfy FOREIGN KEY (resource_id) REFERENCES auth.resource_server_resource(id);


--
-- Name: resource_policy fk_frsrpp213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.resource_policy
    ADD CONSTRAINT fk_frsrpp213xcx4wnkog82ssrfy FOREIGN KEY (policy_id) REFERENCES auth.resource_server_policy(id);


--
-- Name: resource_scope fk_frsrps213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.resource_scope
    ADD CONSTRAINT fk_frsrps213xcx4wnkog82ssrfy FOREIGN KEY (scope_id) REFERENCES auth.resource_server_scope(id);


--
-- Name: resource_server_scope fk_frsrso213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.resource_server_scope
    ADD CONSTRAINT fk_frsrso213xcx4wnkog82ssrfy FOREIGN KEY (resource_server_id) REFERENCES auth.resource_server(id);


--
-- Name: composite_role fk_gr7thllb9lu8q4vqa4524jjy8; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.composite_role
    ADD CONSTRAINT fk_gr7thllb9lu8q4vqa4524jjy8 FOREIGN KEY (child_role) REFERENCES auth.keycloak_role(id);


--
-- Name: user_consent_client_scope fk_grntcsnt_clsc_usc; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.user_consent_client_scope
    ADD CONSTRAINT fk_grntcsnt_clsc_usc FOREIGN KEY (user_consent_id) REFERENCES auth.user_consent(id);


--
-- Name: user_consent fk_grntcsnt_user; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.user_consent
    ADD CONSTRAINT fk_grntcsnt_user FOREIGN KEY (user_id) REFERENCES auth.user_entity(id);


--
-- Name: group_attribute fk_group_attribute_group; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.group_attribute
    ADD CONSTRAINT fk_group_attribute_group FOREIGN KEY (group_id) REFERENCES auth.keycloak_group(id);


--
-- Name: keycloak_group fk_group_realm; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.keycloak_group
    ADD CONSTRAINT fk_group_realm FOREIGN KEY (realm_id) REFERENCES auth.realm(id);


--
-- Name: group_role_mapping fk_group_role_group; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.group_role_mapping
    ADD CONSTRAINT fk_group_role_group FOREIGN KEY (group_id) REFERENCES auth.keycloak_group(id);


--
-- Name: group_role_mapping fk_group_role_role; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.group_role_mapping
    ADD CONSTRAINT fk_group_role_role FOREIGN KEY (role_id) REFERENCES auth.keycloak_role(id);


--
-- Name: realm_default_roles fk_h4wpd7w4hsoolni3h0sw7btje; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.realm_default_roles
    ADD CONSTRAINT fk_h4wpd7w4hsoolni3h0sw7btje FOREIGN KEY (role_id) REFERENCES auth.keycloak_role(id);


--
-- Name: realm_enabled_event_types fk_h846o4h0w8epx5nwedrf5y69j; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.realm_enabled_event_types
    ADD CONSTRAINT fk_h846o4h0w8epx5nwedrf5y69j FOREIGN KEY (realm_id) REFERENCES auth.realm(id);


--
-- Name: realm_events_listeners fk_h846o4h0w8epx5nxev9f5y69j; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.realm_events_listeners
    ADD CONSTRAINT fk_h846o4h0w8epx5nxev9f5y69j FOREIGN KEY (realm_id) REFERENCES auth.realm(id);


--
-- Name: identity_provider_mapper fk_idpm_realm; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.identity_provider_mapper
    ADD CONSTRAINT fk_idpm_realm FOREIGN KEY (realm_id) REFERENCES auth.realm(id);


--
-- Name: idp_mapper_config fk_idpmconfig; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.idp_mapper_config
    ADD CONSTRAINT fk_idpmconfig FOREIGN KEY (idp_mapper_id) REFERENCES auth.identity_provider_mapper(id);


--
-- Name: keycloak_role fk_kjho5le2c0ral09fl8cm9wfw9; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.keycloak_role
    ADD CONSTRAINT fk_kjho5le2c0ral09fl8cm9wfw9 FOREIGN KEY (client) REFERENCES auth.client(id);


--
-- Name: web_origins fk_lojpho213xcx4wnkog82ssrfy; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.web_origins
    ADD CONSTRAINT fk_lojpho213xcx4wnkog82ssrfy FOREIGN KEY (client_id) REFERENCES auth.client(id);


--
-- Name: client_default_roles fk_nuilts7klwqw2h8m2b5joytky; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.client_default_roles
    ADD CONSTRAINT fk_nuilts7klwqw2h8m2b5joytky FOREIGN KEY (client_id) REFERENCES auth.client(id);


--
-- Name: scope_mapping fk_ouse064plmlr732lxjcn1q5f1; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.scope_mapping
    ADD CONSTRAINT fk_ouse064plmlr732lxjcn1q5f1 FOREIGN KEY (client_id) REFERENCES auth.client(id);


--
-- Name: scope_mapping fk_p3rh9grku11kqfrs4fltt7rnq; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.scope_mapping
    ADD CONSTRAINT fk_p3rh9grku11kqfrs4fltt7rnq FOREIGN KEY (role_id) REFERENCES auth.keycloak_role(id);


--
-- Name: client fk_p56ctinxxb9gsk57fo49f9tac; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.client
    ADD CONSTRAINT fk_p56ctinxxb9gsk57fo49f9tac FOREIGN KEY (realm_id) REFERENCES auth.realm(id);


--
-- Name: protocol_mapper fk_pcm_realm; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.protocol_mapper
    ADD CONSTRAINT fk_pcm_realm FOREIGN KEY (client_id) REFERENCES auth.client(id);


--
-- Name: credential fk_pfyr0glasqyl0dei3kl69r6v0; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.credential
    ADD CONSTRAINT fk_pfyr0glasqyl0dei3kl69r6v0 FOREIGN KEY (user_id) REFERENCES auth.user_entity(id);


--
-- Name: protocol_mapper_config fk_pmconfig; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.protocol_mapper_config
    ADD CONSTRAINT fk_pmconfig FOREIGN KEY (protocol_mapper_id) REFERENCES auth.protocol_mapper(id);


--
-- Name: default_client_scope fk_r_def_cli_scope_realm; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.default_client_scope
    ADD CONSTRAINT fk_r_def_cli_scope_realm FOREIGN KEY (realm_id) REFERENCES auth.realm(id);


--
-- Name: default_client_scope fk_r_def_cli_scope_scope; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.default_client_scope
    ADD CONSTRAINT fk_r_def_cli_scope_scope FOREIGN KEY (scope_id) REFERENCES auth.client_scope(id);


--
-- Name: client_scope fk_realm_cli_scope; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.client_scope
    ADD CONSTRAINT fk_realm_cli_scope FOREIGN KEY (realm_id) REFERENCES auth.realm(id);


--
-- Name: required_action_provider fk_req_act_realm; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.required_action_provider
    ADD CONSTRAINT fk_req_act_realm FOREIGN KEY (realm_id) REFERENCES auth.realm(id);


--
-- Name: resource_uris fk_resource_server_uris; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.resource_uris
    ADD CONSTRAINT fk_resource_server_uris FOREIGN KEY (resource_id) REFERENCES auth.resource_server_resource(id);


--
-- Name: role_attribute fk_role_attribute_id; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.role_attribute
    ADD CONSTRAINT fk_role_attribute_id FOREIGN KEY (role_id) REFERENCES auth.keycloak_role(id);


--
-- Name: realm_supported_locales fk_supported_locales_realm; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.realm_supported_locales
    ADD CONSTRAINT fk_supported_locales_realm FOREIGN KEY (realm_id) REFERENCES auth.realm(id);


--
-- Name: user_federation_config fk_t13hpu1j94r2ebpekr39x5eu5; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.user_federation_config
    ADD CONSTRAINT fk_t13hpu1j94r2ebpekr39x5eu5 FOREIGN KEY (user_federation_provider_id) REFERENCES auth.user_federation_provider(id);


--
-- Name: realm fk_traf444kk6qrkms7n56aiwq5y; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.realm
    ADD CONSTRAINT fk_traf444kk6qrkms7n56aiwq5y FOREIGN KEY (master_admin_client) REFERENCES auth.client(id);


--
-- Name: user_group_membership fk_user_group_user; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.user_group_membership
    ADD CONSTRAINT fk_user_group_user FOREIGN KEY (user_id) REFERENCES auth.user_entity(id);


--
-- Name: policy_config fkdc34197cf864c4e43; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.policy_config
    ADD CONSTRAINT fkdc34197cf864c4e43 FOREIGN KEY (policy_id) REFERENCES auth.resource_server_policy(id);


--
-- Name: identity_provider_config fkdc4897cf864c4e43; Type: FK CONSTRAINT; Schema: auth; Owner: auth-db-user
--

ALTER TABLE ONLY auth.identity_provider_config
    ADD CONSTRAINT fkdc4897cf864c4e43 FOREIGN KEY (identity_provider_id) REFERENCES auth.identity_provider(internal_id);


--
-- PostgreSQL database dump complete
--

