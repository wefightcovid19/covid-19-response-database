SET session_replication_role = replica;

--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.17
-- Dumped by pg_dump version 10.8

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: admin_event_entity; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: resource_server; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: resource_server_policy; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: associated_policy; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: client; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--

INSERT INTO auth.client VALUES ('1e839edf-2187-4e52-b927-f5c883ad0177', true, true, 'master-realm', 0, false, '45413bb8-bb04-4c21-a858-03f361d0bf74', NULL, true, NULL, false, 'master', NULL, 0, false, false, 'master Realm', false, 'client-secret', NULL, NULL, NULL, true, false, false);
INSERT INTO auth.client VALUES ('e5cc98c8-b77c-45d4-94c3-7880a3212ce9', true, false, 'account', 0, false, 'ae042cf5-9817-49b7-9219-5d7d4878813c', '/auth/realms/master/account', false, NULL, false, 'master', 'openid-connect', 0, false, false, '${client_account}', false, 'client-secret', NULL, NULL, NULL, true, false, false);
INSERT INTO auth.client VALUES ('74c608e3-e006-402f-b6d3-253bd1ec5015', true, false, 'broker', 0, false, '3c7a8624-a781-4912-8a5f-949b66b71360', NULL, false, NULL, false, 'master', 'openid-connect', 0, false, false, '${client_broker}', false, 'client-secret', NULL, NULL, NULL, true, false, false);
INSERT INTO auth.client VALUES ('2834224e-a18a-4c06-bb03-62e6232ee4dc', true, false, 'security-admin-console', 0, true, '19e704bf-c984-4103-a9fb-ea97f57a4d5a', '/auth/admin/master/console/index.html', false, NULL, false, 'master', 'openid-connect', 0, false, false, '${client_security-admin-console}', false, 'client-secret', NULL, NULL, NULL, true, false, false);
INSERT INTO auth.client VALUES ('920f387e-f7d2-46a3-8ef3-c0550b6c0ae3', true, false, 'admin-cli', 0, true, '6a9bec66-bc99-45af-9fd8-9376943972cf', NULL, false, NULL, false, 'master', 'openid-connect', 0, false, false, '${client_admin-cli}', false, 'client-secret', NULL, NULL, NULL, false, false, true);
INSERT INTO auth.client VALUES ('a965bf32-66d4-41a3-8b65-c9747ad2ac41', true, true, 'covid-19-response-realm', 0, false, '0e8dd603-46ba-43be-899a-601d59337de4', NULL, true, NULL, false, 'master', NULL, 0, false, false, 'covid-19-response Realm', false, 'client-secret', NULL, NULL, NULL, true, false, false);
INSERT INTO auth.client VALUES ('79d753fd-3540-4b38-862a-f1b1852628fe', true, false, 'realm-management', 0, false, '634fb928-3d22-4234-823b-23883a56e985', NULL, true, NULL, false, 'covid-19-response', 'openid-connect', 0, false, false, '${client_realm-management}', false, 'client-secret', NULL, NULL, NULL, true, false, false);
INSERT INTO auth.client VALUES ('de4d38cb-f94e-48ee-8d2d-58f7e84a56e6', true, false, 'account', 0, false, 'e6e06461-42f7-43b4-8ea5-faaec42a3207', '/auth/realms/covid-19-response/account', false, NULL, false, 'covid-19-response', 'openid-connect', 0, false, false, '${client_account}', false, 'client-secret', NULL, NULL, NULL, true, false, false);
INSERT INTO auth.client VALUES ('e58c9a0e-a896-485c-9bde-8af66fbb5144', true, false, 'broker', 0, false, 'fc6e912b-9292-4082-84c0-72bb1aaa9e06', NULL, false, NULL, false, 'covid-19-response', 'openid-connect', 0, false, false, '${client_broker}', false, 'client-secret', NULL, NULL, NULL, true, false, false);
INSERT INTO auth.client VALUES ('22d35cbb-94aa-4004-bb96-82b3687c3441', true, false, 'security-admin-console', 0, true, 'd5a07d1b-90e5-4296-8244-7dcabdd07325', '/auth/admin/covid-19-response/console/index.html', false, NULL, false, 'covid-19-response', 'openid-connect', 0, false, false, '${client_security-admin-console}', false, 'client-secret', NULL, NULL, NULL, true, false, false);
INSERT INTO auth.client VALUES ('9a368257-2d7d-4fcc-84b8-e94eb60452f3', true, false, 'admin-cli', 0, true, '8d2d1616-7e2e-43d5-a4dd-0fa730e75906', NULL, false, NULL, false, 'covid-19-response', 'openid-connect', 0, false, false, '${client_admin-cli}', false, 'client-secret', NULL, NULL, NULL, false, false, true);
INSERT INTO auth.client VALUES ('4ae3a8d3-031e-4332-bbeb-c05edb02f358', true, true, 'temperature-tracker', 0, true, 'a3792d85-6568-4483-bba6-fe8b0bf97936', NULL, false, NULL, false, 'covid-19-response', 'openid-connect', -1, false, false, NULL, false, 'client-secret', NULL, NULL, NULL, true, false, true);


--
-- Data for Name: realm; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--

INSERT INTO auth.realm VALUES ('covid-19-response', 60, 300, 300, NULL, NULL, NULL, true, false, 0, NULL, 'covid-19-response', 0, NULL, false, false, false, false, 'EXTERNAL', 1800, 36000, false, false, 'a965bf32-66d4-41a3-8b65-c9747ad2ac41', 1800, false, NULL, false, false, false, false, 0, 1, 30, 6, 'HmacSHA1', 'totp', 'e03fd03c-f357-497c-93f5-5f1b6eb55993', '8f101136-0a16-423d-8d05-1402acbd706d', '92969321-8514-44fd-87b7-b4ef48eb0337', 'e16cac06-a999-4c4d-9b73-8a30e4671f94', '159a861a-bec7-4e75-97cf-9d90e31041f1', 2592000, false, 900, true, false, 'ae924f95-be93-4294-ba1e-6fd615d1050c', 0, false, 0, 0);
INSERT INTO auth.realm VALUES ('master', 60, 300, 60, NULL, NULL, NULL, true, false, 0, NULL, 'master', 0, NULL, false, false, false, false, 'EXTERNAL', 1800, 36000, false, false, '1e839edf-2187-4e52-b927-f5c883ad0177', 1800, false, NULL, false, false, false, false, 0, 1, 30, 6, 'HmacSHA1', 'totp', 'ae5eb76e-3103-41f7-8837-30ba9ce9fa96', '93b840e5-1630-4b8a-8171-fa0844671706', '271c8108-fb09-482f-89da-4e45dbbac903', '6da40868-5973-4f75-882f-6da157ddbea0', '62934f2e-a341-4b42-b8c5-60437db5022e', 2592000, false, 900, true, false, '679316a7-61e9-4fb7-b38e-99b0784052f1', 0, false, 0, 0);


--
-- Data for Name: authentication_flow; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--

INSERT INTO auth.authentication_flow VALUES ('ae5eb76e-3103-41f7-8837-30ba9ce9fa96', 'browser', 'browser based authentication', 'master', 'basic-flow', true, true);
INSERT INTO auth.authentication_flow VALUES ('100ae1bc-e688-4646-b2e2-41f4e4a7c2c4', 'forms', 'Username, password, otp and other auth forms.', 'master', 'basic-flow', false, true);
INSERT INTO auth.authentication_flow VALUES ('271c8108-fb09-482f-89da-4e45dbbac903', 'direct grant', 'OpenID Connect Resource Owner Grant', 'master', 'basic-flow', true, true);
INSERT INTO auth.authentication_flow VALUES ('93b840e5-1630-4b8a-8171-fa0844671706', 'registration', 'registration flow', 'master', 'basic-flow', true, true);
INSERT INTO auth.authentication_flow VALUES ('28c2395e-4d67-4c16-a301-4cba36b85c1f', 'registration form', 'registration form', 'master', 'form-flow', false, true);
INSERT INTO auth.authentication_flow VALUES ('6da40868-5973-4f75-882f-6da157ddbea0', 'reset credentials', 'Reset credentials for a user if they forgot their password or something', 'master', 'basic-flow', true, true);
INSERT INTO auth.authentication_flow VALUES ('62934f2e-a341-4b42-b8c5-60437db5022e', 'clients', 'Base authentication for clients', 'master', 'client-flow', true, true);
INSERT INTO auth.authentication_flow VALUES ('bdbd62ae-e705-4b3f-bf4a-8a373500f793', 'first broker login', 'Actions taken after first broker login with identity provider account, which is not yet linked to any Keycloak account', 'master', 'basic-flow', true, true);
INSERT INTO auth.authentication_flow VALUES ('ec14a095-61d1-4023-a1eb-5d3fe4597fd1', 'Handle Existing Account', 'Handle what to do if there is existing account with same email/username like authenticated identity provider', 'master', 'basic-flow', false, true);
INSERT INTO auth.authentication_flow VALUES ('6af83467-7090-4d66-9a44-4680363da3c6', 'Verify Existing Account by Re-authentication', 'Reauthentication of existing account', 'master', 'basic-flow', false, true);
INSERT INTO auth.authentication_flow VALUES ('c0a2c348-394a-4863-9549-ccf3aa1f5253', 'saml ecp', 'SAML ECP Profile Authentication Flow', 'master', 'basic-flow', true, true);
INSERT INTO auth.authentication_flow VALUES ('679316a7-61e9-4fb7-b38e-99b0784052f1', 'docker auth', 'Used by Docker clients to authenticate against the IDP', 'master', 'basic-flow', true, true);
INSERT INTO auth.authentication_flow VALUES ('207524c4-c81c-4682-a891-5576c338890e', 'http challenge', 'An authentication flow based on challenge-response HTTP Authentication Schemes', 'master', 'basic-flow', true, true);
INSERT INTO auth.authentication_flow VALUES ('e03fd03c-f357-497c-93f5-5f1b6eb55993', 'browser', 'browser based authentication', 'covid-19-response', 'basic-flow', true, true);
INSERT INTO auth.authentication_flow VALUES ('4d266107-42cd-4343-86d6-7169be824bd7', 'forms', 'Username, password, otp and other auth forms.', 'covid-19-response', 'basic-flow', false, true);
INSERT INTO auth.authentication_flow VALUES ('92969321-8514-44fd-87b7-b4ef48eb0337', 'direct grant', 'OpenID Connect Resource Owner Grant', 'covid-19-response', 'basic-flow', true, true);
INSERT INTO auth.authentication_flow VALUES ('8f101136-0a16-423d-8d05-1402acbd706d', 'registration', 'registration flow', 'covid-19-response', 'basic-flow', true, true);
INSERT INTO auth.authentication_flow VALUES ('5a5d7fc5-8b4c-42d8-94db-14f90d9d6054', 'registration form', 'registration form', 'covid-19-response', 'form-flow', false, true);
INSERT INTO auth.authentication_flow VALUES ('e16cac06-a999-4c4d-9b73-8a30e4671f94', 'reset credentials', 'Reset credentials for a user if they forgot their password or something', 'covid-19-response', 'basic-flow', true, true);
INSERT INTO auth.authentication_flow VALUES ('159a861a-bec7-4e75-97cf-9d90e31041f1', 'clients', 'Base authentication for clients', 'covid-19-response', 'client-flow', true, true);
INSERT INTO auth.authentication_flow VALUES ('55e479ea-a568-4899-99c7-e651fae9ee08', 'first broker login', 'Actions taken after first broker login with identity provider account, which is not yet linked to any Keycloak account', 'covid-19-response', 'basic-flow', true, true);
INSERT INTO auth.authentication_flow VALUES ('7fae3b48-0cd1-4e60-96ec-5b73e6c494f9', 'Handle Existing Account', 'Handle what to do if there is existing account with same email/username like authenticated identity provider', 'covid-19-response', 'basic-flow', false, true);
INSERT INTO auth.authentication_flow VALUES ('a751f8ab-152b-469a-9f70-9bdcfab991bc', 'Verify Existing Account by Re-authentication', 'Reauthentication of existing account', 'covid-19-response', 'basic-flow', false, true);
INSERT INTO auth.authentication_flow VALUES ('01696f90-c431-4449-b980-09f3c349b087', 'saml ecp', 'SAML ECP Profile Authentication Flow', 'covid-19-response', 'basic-flow', true, true);
INSERT INTO auth.authentication_flow VALUES ('ae924f95-be93-4294-ba1e-6fd615d1050c', 'docker auth', 'Used by Docker clients to authenticate against the IDP', 'covid-19-response', 'basic-flow', true, true);
INSERT INTO auth.authentication_flow VALUES ('c1c5ebee-fb0b-4f09-8683-eb2adde5e8db', 'http challenge', 'An authentication flow based on challenge-response HTTP Authentication Schemes', 'covid-19-response', 'basic-flow', true, true);


--
-- Data for Name: authentication_execution; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--

INSERT INTO auth.authentication_execution VALUES ('9c8a7bf0-65a9-468e-b852-031f6689eed0', NULL, 'auth-cookie', 'master', 'ae5eb76e-3103-41f7-8837-30ba9ce9fa96', 2, 10, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('22a0ef17-d35a-43ff-b575-583627c1a09c', NULL, 'auth-spnego', 'master', 'ae5eb76e-3103-41f7-8837-30ba9ce9fa96', 3, 20, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('beaf6d24-6978-481e-8698-a3a610e94fa1', NULL, 'identity-provider-redirector', 'master', 'ae5eb76e-3103-41f7-8837-30ba9ce9fa96', 2, 25, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('5ad6c331-e4a7-4b48-9fd9-b7d1f8a2d9ef', NULL, NULL, 'master', 'ae5eb76e-3103-41f7-8837-30ba9ce9fa96', 2, 30, true, '100ae1bc-e688-4646-b2e2-41f4e4a7c2c4', NULL);
INSERT INTO auth.authentication_execution VALUES ('830e924f-d856-48cb-a12c-6f1b1ea23c24', NULL, 'auth-username-password-form', 'master', '100ae1bc-e688-4646-b2e2-41f4e4a7c2c4', 0, 10, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('724a0d28-daa8-46af-8aeb-5833240d0b06', NULL, 'auth-otp-form', 'master', '100ae1bc-e688-4646-b2e2-41f4e4a7c2c4', 1, 20, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('c5116be1-f5eb-4197-9e75-a7dacbc5e074', NULL, 'direct-grant-validate-username', 'master', '271c8108-fb09-482f-89da-4e45dbbac903', 0, 10, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('c6b17d66-282e-4225-ab23-2a431cd931fe', NULL, 'direct-grant-validate-password', 'master', '271c8108-fb09-482f-89da-4e45dbbac903', 0, 20, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('8588ef60-996a-4730-83df-e167a7a0464f', NULL, 'direct-grant-validate-otp', 'master', '271c8108-fb09-482f-89da-4e45dbbac903', 1, 30, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('7a02734a-398c-4524-b9a0-0514fb42181d', NULL, 'registration-page-form', 'master', '93b840e5-1630-4b8a-8171-fa0844671706', 0, 10, true, '28c2395e-4d67-4c16-a301-4cba36b85c1f', NULL);
INSERT INTO auth.authentication_execution VALUES ('e6cefea7-9133-412d-8a2f-afd6d138c48e', NULL, 'registration-user-creation', 'master', '28c2395e-4d67-4c16-a301-4cba36b85c1f', 0, 20, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('f9f4cb2e-4923-480c-992c-98f4a330b646', NULL, 'registration-profile-action', 'master', '28c2395e-4d67-4c16-a301-4cba36b85c1f', 0, 40, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('bc35acbb-f741-4c99-80fe-8ec3332340a2', NULL, 'registration-password-action', 'master', '28c2395e-4d67-4c16-a301-4cba36b85c1f', 0, 50, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('24dc2f1c-a4f4-443c-a5a0-bb073fd130d4', NULL, 'registration-recaptcha-action', 'master', '28c2395e-4d67-4c16-a301-4cba36b85c1f', 3, 60, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('53ef8b8f-74cc-4af4-a9e7-95bce5225df1', NULL, 'reset-credentials-choose-user', 'master', '6da40868-5973-4f75-882f-6da157ddbea0', 0, 10, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('05ed5681-d7b0-4673-b402-d76ce4ef0342', NULL, 'reset-credential-email', 'master', '6da40868-5973-4f75-882f-6da157ddbea0', 0, 20, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('4ebcbd53-07d2-494c-8a09-70bb3c3ca8e5', NULL, 'reset-password', 'master', '6da40868-5973-4f75-882f-6da157ddbea0', 0, 30, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('416206f0-e745-4f4a-8351-54172e71f40b', NULL, 'reset-otp', 'master', '6da40868-5973-4f75-882f-6da157ddbea0', 1, 40, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('aade548a-cc5e-4405-8829-ffe74a9a04b8', NULL, 'client-secret', 'master', '62934f2e-a341-4b42-b8c5-60437db5022e', 2, 10, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('90617031-0113-407e-a8af-d94e00066bb3', NULL, 'client-jwt', 'master', '62934f2e-a341-4b42-b8c5-60437db5022e', 2, 20, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('1834ec3b-f2fd-4ef9-927c-986e8a6c000b', NULL, 'client-secret-jwt', 'master', '62934f2e-a341-4b42-b8c5-60437db5022e', 2, 30, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('f6227b93-0c6d-4fdc-a895-fc324fe0246a', NULL, 'client-x509', 'master', '62934f2e-a341-4b42-b8c5-60437db5022e', 2, 40, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('e8765467-cc71-48f0-aacb-af4aeb09f15c', NULL, 'idp-review-profile', 'master', 'bdbd62ae-e705-4b3f-bf4a-8a373500f793', 0, 10, false, NULL, 'fcb0b1aa-48ff-46c9-8f02-727ee0a3e419');
INSERT INTO auth.authentication_execution VALUES ('1ac50c99-1599-4a45-a1e3-76932cce87a4', NULL, 'idp-create-user-if-unique', 'master', 'bdbd62ae-e705-4b3f-bf4a-8a373500f793', 2, 20, false, NULL, 'd8e22a63-1208-41e7-892d-8268b89cf24a');
INSERT INTO auth.authentication_execution VALUES ('f4ed13ef-ee2f-4d2e-98b8-184119fb3170', NULL, NULL, 'master', 'bdbd62ae-e705-4b3f-bf4a-8a373500f793', 2, 30, true, 'ec14a095-61d1-4023-a1eb-5d3fe4597fd1', NULL);
INSERT INTO auth.authentication_execution VALUES ('2c8f1a8b-8d82-489c-b387-4831dccc20f9', NULL, 'idp-confirm-link', 'master', 'ec14a095-61d1-4023-a1eb-5d3fe4597fd1', 0, 10, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('18b912de-2c29-4627-beda-833683952ba6', NULL, 'idp-email-verification', 'master', 'ec14a095-61d1-4023-a1eb-5d3fe4597fd1', 2, 20, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('98ef93a3-e429-4d61-ae96-d0b815524eb1', NULL, NULL, 'master', 'ec14a095-61d1-4023-a1eb-5d3fe4597fd1', 2, 30, true, '6af83467-7090-4d66-9a44-4680363da3c6', NULL);
INSERT INTO auth.authentication_execution VALUES ('30735cad-7f66-4db3-8953-e8e85b7a5432', NULL, 'idp-username-password-form', 'master', '6af83467-7090-4d66-9a44-4680363da3c6', 0, 10, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('70727272-d02f-4105-b14c-63beeab14509', NULL, 'auth-otp-form', 'master', '6af83467-7090-4d66-9a44-4680363da3c6', 1, 20, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('f6fe8145-f0ab-4a9f-94b3-a6a05b2ba3f8', NULL, 'http-basic-authenticator', 'master', 'c0a2c348-394a-4863-9549-ccf3aa1f5253', 0, 10, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('77bec812-467d-448f-bc4a-8779303eed43', NULL, 'docker-http-basic-authenticator', 'master', '679316a7-61e9-4fb7-b38e-99b0784052f1', 0, 10, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('40a8e1fd-16d9-4dda-8a71-9a7e49701b83', NULL, 'no-cookie-redirect', 'master', '207524c4-c81c-4682-a891-5576c338890e', 0, 10, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('0f597ac8-3a05-468b-bf2b-80029fe1e312', NULL, 'basic-auth', 'master', '207524c4-c81c-4682-a891-5576c338890e', 0, 20, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('8ae43bd9-f79e-495f-bdb6-8da012f66d72', NULL, 'basic-auth-otp', 'master', '207524c4-c81c-4682-a891-5576c338890e', 3, 30, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('c52e64bc-9cd3-4feb-a080-ec2715c7c0e0', NULL, 'auth-spnego', 'master', '207524c4-c81c-4682-a891-5576c338890e', 3, 40, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('289e532c-f751-4e20-8fee-a799a97c50c9', NULL, 'auth-cookie', 'covid-19-response', 'e03fd03c-f357-497c-93f5-5f1b6eb55993', 2, 10, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('f6f57d8e-2788-4485-95ee-d57433188c66', NULL, 'auth-spnego', 'covid-19-response', 'e03fd03c-f357-497c-93f5-5f1b6eb55993', 3, 20, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('005bbbb2-6b44-4462-a737-406cc1bc8443', NULL, 'identity-provider-redirector', 'covid-19-response', 'e03fd03c-f357-497c-93f5-5f1b6eb55993', 2, 25, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('b70ffc86-6834-4dce-af7c-25f3a7333ccc', NULL, NULL, 'covid-19-response', 'e03fd03c-f357-497c-93f5-5f1b6eb55993', 2, 30, true, '4d266107-42cd-4343-86d6-7169be824bd7', NULL);
INSERT INTO auth.authentication_execution VALUES ('eb12e261-167d-4944-af09-76129a6c458e', NULL, 'auth-username-password-form', 'covid-19-response', '4d266107-42cd-4343-86d6-7169be824bd7', 0, 10, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('3819cd7d-47b7-41f7-ba12-fa45b194fff2', NULL, 'auth-otp-form', 'covid-19-response', '4d266107-42cd-4343-86d6-7169be824bd7', 1, 20, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('61814d7c-b72c-4682-ae29-36135f603b44', NULL, 'direct-grant-validate-username', 'covid-19-response', '92969321-8514-44fd-87b7-b4ef48eb0337', 0, 10, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('7badb105-45ca-4c64-b520-9b52307a1b80', NULL, 'direct-grant-validate-password', 'covid-19-response', '92969321-8514-44fd-87b7-b4ef48eb0337', 0, 20, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('0ae31b6a-7350-4696-abe6-a87730f7fbb6', NULL, 'direct-grant-validate-otp', 'covid-19-response', '92969321-8514-44fd-87b7-b4ef48eb0337', 1, 30, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('78b25607-7048-437a-8882-bc7b24cbc635', NULL, 'registration-page-form', 'covid-19-response', '8f101136-0a16-423d-8d05-1402acbd706d', 0, 10, true, '5a5d7fc5-8b4c-42d8-94db-14f90d9d6054', NULL);
INSERT INTO auth.authentication_execution VALUES ('e2e863e2-cbdd-43b1-a185-b7ac862687c6', NULL, 'registration-user-creation', 'covid-19-response', '5a5d7fc5-8b4c-42d8-94db-14f90d9d6054', 0, 20, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('9f289af8-cdb6-4544-86ea-279f31e7becf', NULL, 'registration-profile-action', 'covid-19-response', '5a5d7fc5-8b4c-42d8-94db-14f90d9d6054', 0, 40, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('9a0fecec-c314-4564-bf94-d78d4e01516d', NULL, 'registration-password-action', 'covid-19-response', '5a5d7fc5-8b4c-42d8-94db-14f90d9d6054', 0, 50, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('59f1605d-1fe8-4a52-839e-aa9334998105', NULL, 'registration-recaptcha-action', 'covid-19-response', '5a5d7fc5-8b4c-42d8-94db-14f90d9d6054', 3, 60, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('126c8f56-c53d-47c0-8393-42ea761b1745', NULL, 'reset-credentials-choose-user', 'covid-19-response', 'e16cac06-a999-4c4d-9b73-8a30e4671f94', 0, 10, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('d30ce459-7e5e-41e1-8479-b17a6d646cda', NULL, 'reset-credential-email', 'covid-19-response', 'e16cac06-a999-4c4d-9b73-8a30e4671f94', 0, 20, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('ae04af5a-a160-4216-8299-cb0db1b13012', NULL, 'reset-password', 'covid-19-response', 'e16cac06-a999-4c4d-9b73-8a30e4671f94', 0, 30, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('69ce3ea8-a77e-4895-b4c9-bbecf9352783', NULL, 'reset-otp', 'covid-19-response', 'e16cac06-a999-4c4d-9b73-8a30e4671f94', 1, 40, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('cbc2d713-4d69-4a3a-a715-7f5c315d1c78', NULL, 'client-secret', 'covid-19-response', '159a861a-bec7-4e75-97cf-9d90e31041f1', 2, 10, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('1fa54c97-d428-4c63-ae6a-5f056edb6e01', NULL, 'client-jwt', 'covid-19-response', '159a861a-bec7-4e75-97cf-9d90e31041f1', 2, 20, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('a28f7225-ec47-4919-98ea-51d267316c88', NULL, 'client-secret-jwt', 'covid-19-response', '159a861a-bec7-4e75-97cf-9d90e31041f1', 2, 30, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('24fa4b22-16ff-4fbb-992c-41843e1e052e', NULL, 'client-x509', 'covid-19-response', '159a861a-bec7-4e75-97cf-9d90e31041f1', 2, 40, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('9c5be20b-7d33-4a86-b64c-e057c24684c8', NULL, 'idp-review-profile', 'covid-19-response', '55e479ea-a568-4899-99c7-e651fae9ee08', 0, 10, false, NULL, '26a550f5-05ee-49f1-b754-aa730095c5cd');
INSERT INTO auth.authentication_execution VALUES ('05ef971c-8859-4e38-8ec3-21762446277f', NULL, 'idp-create-user-if-unique', 'covid-19-response', '55e479ea-a568-4899-99c7-e651fae9ee08', 2, 20, false, NULL, '4cf56cf7-d1ce-4891-99db-6dbfe318fd24');
INSERT INTO auth.authentication_execution VALUES ('b61229d8-b732-469a-bf3d-01bb87310c2c', NULL, NULL, 'covid-19-response', '55e479ea-a568-4899-99c7-e651fae9ee08', 2, 30, true, '7fae3b48-0cd1-4e60-96ec-5b73e6c494f9', NULL);
INSERT INTO auth.authentication_execution VALUES ('9b1f90c8-4e1f-413e-9fd3-7388caae2cc8', NULL, 'idp-confirm-link', 'covid-19-response', '7fae3b48-0cd1-4e60-96ec-5b73e6c494f9', 0, 10, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('c40d81d0-df8a-4ef9-a772-5fca8c7cdbb2', NULL, 'idp-email-verification', 'covid-19-response', '7fae3b48-0cd1-4e60-96ec-5b73e6c494f9', 2, 20, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('27d73bce-bd9e-4286-ac1a-35b609e61c61', NULL, NULL, 'covid-19-response', '7fae3b48-0cd1-4e60-96ec-5b73e6c494f9', 2, 30, true, 'a751f8ab-152b-469a-9f70-9bdcfab991bc', NULL);
INSERT INTO auth.authentication_execution VALUES ('e94e3825-11f8-4ad1-a9a1-898b724446c2', NULL, 'idp-username-password-form', 'covid-19-response', 'a751f8ab-152b-469a-9f70-9bdcfab991bc', 0, 10, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('1936f6cc-38ef-4f2d-b5f8-e65a91ab1208', NULL, 'auth-otp-form', 'covid-19-response', 'a751f8ab-152b-469a-9f70-9bdcfab991bc', 1, 20, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('49eba78e-523f-4e43-9c61-277546925699', NULL, 'http-basic-authenticator', 'covid-19-response', '01696f90-c431-4449-b980-09f3c349b087', 0, 10, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('fbdf8d5d-a9df-4ab2-8066-f123c6888478', NULL, 'docker-http-basic-authenticator', 'covid-19-response', 'ae924f95-be93-4294-ba1e-6fd615d1050c', 0, 10, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('6143c186-698c-42d0-9f6b-e3be059a5821', NULL, 'no-cookie-redirect', 'covid-19-response', 'c1c5ebee-fb0b-4f09-8683-eb2adde5e8db', 0, 10, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('a4b0d684-0c5d-4727-9c60-943955f7fec5', NULL, 'basic-auth', 'covid-19-response', 'c1c5ebee-fb0b-4f09-8683-eb2adde5e8db', 0, 20, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('00f01194-a635-4816-8438-7ae8fda419fe', NULL, 'basic-auth-otp', 'covid-19-response', 'c1c5ebee-fb0b-4f09-8683-eb2adde5e8db', 3, 30, false, NULL, NULL);
INSERT INTO auth.authentication_execution VALUES ('8a44bd51-4735-4f81-a7d5-2da816be8a6b', NULL, 'auth-spnego', 'covid-19-response', 'c1c5ebee-fb0b-4f09-8683-eb2adde5e8db', 3, 40, false, NULL, NULL);


--
-- Data for Name: authenticator_config; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--

INSERT INTO auth.authenticator_config VALUES ('fcb0b1aa-48ff-46c9-8f02-727ee0a3e419', 'review profile config', 'master');
INSERT INTO auth.authenticator_config VALUES ('d8e22a63-1208-41e7-892d-8268b89cf24a', 'create unique user config', 'master');
INSERT INTO auth.authenticator_config VALUES ('26a550f5-05ee-49f1-b754-aa730095c5cd', 'review profile config', 'covid-19-response');
INSERT INTO auth.authenticator_config VALUES ('4cf56cf7-d1ce-4891-99db-6dbfe318fd24', 'create unique user config', 'covid-19-response');


--
-- Data for Name: authenticator_config_entry; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--

INSERT INTO auth.authenticator_config_entry VALUES ('fcb0b1aa-48ff-46c9-8f02-727ee0a3e419', 'missing', 'update.profile.on.first.login');
INSERT INTO auth.authenticator_config_entry VALUES ('d8e22a63-1208-41e7-892d-8268b89cf24a', 'false', 'require.password.update.after.registration');
INSERT INTO auth.authenticator_config_entry VALUES ('26a550f5-05ee-49f1-b754-aa730095c5cd', 'missing', 'update.profile.on.first.login');
INSERT INTO auth.authenticator_config_entry VALUES ('4cf56cf7-d1ce-4891-99db-6dbfe318fd24', 'false', 'require.password.update.after.registration');


--
-- Data for Name: broker_link; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: client_attributes; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--

INSERT INTO auth.client_attributes VALUES ('4ae3a8d3-031e-4332-bbeb-c05edb02f358', 'false', 'saml.server.signature');
INSERT INTO auth.client_attributes VALUES ('4ae3a8d3-031e-4332-bbeb-c05edb02f358', 'false', 'saml.server.signature.keyinfo.ext');
INSERT INTO auth.client_attributes VALUES ('4ae3a8d3-031e-4332-bbeb-c05edb02f358', 'false', 'saml.assertion.signature');
INSERT INTO auth.client_attributes VALUES ('4ae3a8d3-031e-4332-bbeb-c05edb02f358', 'false', 'saml.client.signature');
INSERT INTO auth.client_attributes VALUES ('4ae3a8d3-031e-4332-bbeb-c05edb02f358', 'false', 'saml.encrypt');
INSERT INTO auth.client_attributes VALUES ('4ae3a8d3-031e-4332-bbeb-c05edb02f358', 'false', 'saml.authnstatement');
INSERT INTO auth.client_attributes VALUES ('4ae3a8d3-031e-4332-bbeb-c05edb02f358', 'false', 'saml.onetimeuse.condition');
INSERT INTO auth.client_attributes VALUES ('4ae3a8d3-031e-4332-bbeb-c05edb02f358', 'false', 'saml_force_name_id_format');
INSERT INTO auth.client_attributes VALUES ('4ae3a8d3-031e-4332-bbeb-c05edb02f358', 'false', 'saml.multivalued.roles');
INSERT INTO auth.client_attributes VALUES ('4ae3a8d3-031e-4332-bbeb-c05edb02f358', 'false', 'saml.force.post.binding');
INSERT INTO auth.client_attributes VALUES ('4ae3a8d3-031e-4332-bbeb-c05edb02f358', 'false', 'exclude.session.state.from.auth.response');
INSERT INTO auth.client_attributes VALUES ('4ae3a8d3-031e-4332-bbeb-c05edb02f358', 'false', 'tls.client.certificate.bound.access.tokens');
INSERT INTO auth.client_attributes VALUES ('4ae3a8d3-031e-4332-bbeb-c05edb02f358', 'false', 'display.on.consent.screen');


--
-- Data for Name: client_auth_flow_bindings; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: keycloak_role; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--

INSERT INTO auth.keycloak_role VALUES ('348a55d0-14a9-428d-8b9c-3ea6cc73495f', 'master', false, '${role_admin}', 'admin', 'master', NULL, 'master');
INSERT INTO auth.keycloak_role VALUES ('a09b4532-2c7b-42e0-a0b4-d63693e1fed1', 'master', false, '${role_create-realm}', 'create-realm', 'master', NULL, 'master');
INSERT INTO auth.keycloak_role VALUES ('8e1f5874-d11d-4195-88b0-2ce3ee3ed6ba', '1e839edf-2187-4e52-b927-f5c883ad0177', true, '${role_create-client}', 'create-client', 'master', '1e839edf-2187-4e52-b927-f5c883ad0177', NULL);
INSERT INTO auth.keycloak_role VALUES ('459d7386-d5c0-410c-bac9-e34e71008c59', '1e839edf-2187-4e52-b927-f5c883ad0177', true, '${role_view-realm}', 'view-realm', 'master', '1e839edf-2187-4e52-b927-f5c883ad0177', NULL);
INSERT INTO auth.keycloak_role VALUES ('4d881722-396f-4aac-9ced-d1b141e65486', '1e839edf-2187-4e52-b927-f5c883ad0177', true, '${role_view-users}', 'view-users', 'master', '1e839edf-2187-4e52-b927-f5c883ad0177', NULL);
INSERT INTO auth.keycloak_role VALUES ('40fdf4e8-9882-4310-9743-a2e2168ad70c', '1e839edf-2187-4e52-b927-f5c883ad0177', true, '${role_view-clients}', 'view-clients', 'master', '1e839edf-2187-4e52-b927-f5c883ad0177', NULL);
INSERT INTO auth.keycloak_role VALUES ('3931e94d-9398-4be8-b7ee-b1f4b78f544a', '1e839edf-2187-4e52-b927-f5c883ad0177', true, '${role_view-events}', 'view-events', 'master', '1e839edf-2187-4e52-b927-f5c883ad0177', NULL);
INSERT INTO auth.keycloak_role VALUES ('13b7f8c6-e121-4bd1-a02d-1cd72b741ad5', '1e839edf-2187-4e52-b927-f5c883ad0177', true, '${role_view-identity-providers}', 'view-identity-providers', 'master', '1e839edf-2187-4e52-b927-f5c883ad0177', NULL);
INSERT INTO auth.keycloak_role VALUES ('5f71ace1-babd-488e-ad39-887451d36263', '1e839edf-2187-4e52-b927-f5c883ad0177', true, '${role_view-authorization}', 'view-authorization', 'master', '1e839edf-2187-4e52-b927-f5c883ad0177', NULL);
INSERT INTO auth.keycloak_role VALUES ('bca41d85-48b9-4206-a79b-752d372a65e6', '1e839edf-2187-4e52-b927-f5c883ad0177', true, '${role_manage-realm}', 'manage-realm', 'master', '1e839edf-2187-4e52-b927-f5c883ad0177', NULL);
INSERT INTO auth.keycloak_role VALUES ('b1f4dc89-50f5-4bd3-b01f-a682b2dfcfa6', '1e839edf-2187-4e52-b927-f5c883ad0177', true, '${role_manage-users}', 'manage-users', 'master', '1e839edf-2187-4e52-b927-f5c883ad0177', NULL);
INSERT INTO auth.keycloak_role VALUES ('73c8af2d-1b65-4823-8f52-fe3939637010', '1e839edf-2187-4e52-b927-f5c883ad0177', true, '${role_manage-clients}', 'manage-clients', 'master', '1e839edf-2187-4e52-b927-f5c883ad0177', NULL);
INSERT INTO auth.keycloak_role VALUES ('e58187c1-7759-4f0f-a947-b4a7db393efa', '1e839edf-2187-4e52-b927-f5c883ad0177', true, '${role_manage-events}', 'manage-events', 'master', '1e839edf-2187-4e52-b927-f5c883ad0177', NULL);
INSERT INTO auth.keycloak_role VALUES ('efba2c27-2c1f-48c2-8c0b-413d00e43003', '1e839edf-2187-4e52-b927-f5c883ad0177', true, '${role_manage-identity-providers}', 'manage-identity-providers', 'master', '1e839edf-2187-4e52-b927-f5c883ad0177', NULL);
INSERT INTO auth.keycloak_role VALUES ('311e52e2-39b0-4580-8b59-18121fa04441', '1e839edf-2187-4e52-b927-f5c883ad0177', true, '${role_manage-authorization}', 'manage-authorization', 'master', '1e839edf-2187-4e52-b927-f5c883ad0177', NULL);
INSERT INTO auth.keycloak_role VALUES ('1b2aa2d3-9056-4855-984d-21487dd3ad22', '1e839edf-2187-4e52-b927-f5c883ad0177', true, '${role_query-users}', 'query-users', 'master', '1e839edf-2187-4e52-b927-f5c883ad0177', NULL);
INSERT INTO auth.keycloak_role VALUES ('a6fa0a1f-6795-4359-ab3e-55f369682153', '1e839edf-2187-4e52-b927-f5c883ad0177', true, '${role_query-clients}', 'query-clients', 'master', '1e839edf-2187-4e52-b927-f5c883ad0177', NULL);
INSERT INTO auth.keycloak_role VALUES ('8a8c6b44-f202-444f-ae15-7672889d067b', '1e839edf-2187-4e52-b927-f5c883ad0177', true, '${role_query-realms}', 'query-realms', 'master', '1e839edf-2187-4e52-b927-f5c883ad0177', NULL);
INSERT INTO auth.keycloak_role VALUES ('96320915-ec41-486f-bb4f-a159b9b518cc', '1e839edf-2187-4e52-b927-f5c883ad0177', true, '${role_query-groups}', 'query-groups', 'master', '1e839edf-2187-4e52-b927-f5c883ad0177', NULL);
INSERT INTO auth.keycloak_role VALUES ('02326645-6d43-4026-9291-e6961f7ab352', 'e5cc98c8-b77c-45d4-94c3-7880a3212ce9', true, '${role_view-profile}', 'view-profile', 'master', 'e5cc98c8-b77c-45d4-94c3-7880a3212ce9', NULL);
INSERT INTO auth.keycloak_role VALUES ('b673e7a0-bd70-4543-b840-c1c6cb193bee', 'e5cc98c8-b77c-45d4-94c3-7880a3212ce9', true, '${role_manage-account}', 'manage-account', 'master', 'e5cc98c8-b77c-45d4-94c3-7880a3212ce9', NULL);
INSERT INTO auth.keycloak_role VALUES ('f5dc963c-735f-44b9-8fb4-2030993b693c', 'e5cc98c8-b77c-45d4-94c3-7880a3212ce9', true, '${role_manage-account-links}', 'manage-account-links', 'master', 'e5cc98c8-b77c-45d4-94c3-7880a3212ce9', NULL);
INSERT INTO auth.keycloak_role VALUES ('97fdc944-cec7-4b8c-ba9d-fefe195943d6', '74c608e3-e006-402f-b6d3-253bd1ec5015', true, '${role_read-token}', 'read-token', 'master', '74c608e3-e006-402f-b6d3-253bd1ec5015', NULL);
INSERT INTO auth.keycloak_role VALUES ('0189e5f4-8c50-48fb-b663-1303eeac0493', '1e839edf-2187-4e52-b927-f5c883ad0177', true, '${role_impersonation}', 'impersonation', 'master', '1e839edf-2187-4e52-b927-f5c883ad0177', NULL);
INSERT INTO auth.keycloak_role VALUES ('a2f1c7f8-e931-4ae0-b554-32a351bbbb14', 'master', false, '${role_offline-access}', 'offline_access', 'master', NULL, 'master');
INSERT INTO auth.keycloak_role VALUES ('f4ff4b3b-949d-49b3-a3e8-b7ab6a879e73', 'master', false, '${role_uma_authorization}', 'uma_authorization', 'master', NULL, 'master');
INSERT INTO auth.keycloak_role VALUES ('1e272c1f-ed09-494e-8a4e-18ae33466b5c', 'a965bf32-66d4-41a3-8b65-c9747ad2ac41', true, '${role_create-client}', 'create-client', 'master', 'a965bf32-66d4-41a3-8b65-c9747ad2ac41', NULL);
INSERT INTO auth.keycloak_role VALUES ('952e122d-a8d2-4979-9ddc-eb159fb4ab72', 'a965bf32-66d4-41a3-8b65-c9747ad2ac41', true, '${role_view-realm}', 'view-realm', 'master', 'a965bf32-66d4-41a3-8b65-c9747ad2ac41', NULL);
INSERT INTO auth.keycloak_role VALUES ('291d9982-ce08-44ff-8b86-330984e7c786', 'a965bf32-66d4-41a3-8b65-c9747ad2ac41', true, '${role_view-users}', 'view-users', 'master', 'a965bf32-66d4-41a3-8b65-c9747ad2ac41', NULL);
INSERT INTO auth.keycloak_role VALUES ('725f9e5b-3896-4a43-aa19-438862528549', 'a965bf32-66d4-41a3-8b65-c9747ad2ac41', true, '${role_view-clients}', 'view-clients', 'master', 'a965bf32-66d4-41a3-8b65-c9747ad2ac41', NULL);
INSERT INTO auth.keycloak_role VALUES ('ec8ddff1-390e-4c51-a5b8-4d65043263c0', 'a965bf32-66d4-41a3-8b65-c9747ad2ac41', true, '${role_view-events}', 'view-events', 'master', 'a965bf32-66d4-41a3-8b65-c9747ad2ac41', NULL);
INSERT INTO auth.keycloak_role VALUES ('8379a962-0964-4eb8-a55c-afea13e45cb4', 'a965bf32-66d4-41a3-8b65-c9747ad2ac41', true, '${role_view-identity-providers}', 'view-identity-providers', 'master', 'a965bf32-66d4-41a3-8b65-c9747ad2ac41', NULL);
INSERT INTO auth.keycloak_role VALUES ('85f4ea7b-c18f-4451-9efb-350b73bbcc5b', 'a965bf32-66d4-41a3-8b65-c9747ad2ac41', true, '${role_view-authorization}', 'view-authorization', 'master', 'a965bf32-66d4-41a3-8b65-c9747ad2ac41', NULL);
INSERT INTO auth.keycloak_role VALUES ('5d1facc3-d305-49d0-82af-8286e9a1d03e', 'a965bf32-66d4-41a3-8b65-c9747ad2ac41', true, '${role_manage-realm}', 'manage-realm', 'master', 'a965bf32-66d4-41a3-8b65-c9747ad2ac41', NULL);
INSERT INTO auth.keycloak_role VALUES ('ea38b558-928a-4e6f-9383-18bf06d113dc', 'a965bf32-66d4-41a3-8b65-c9747ad2ac41', true, '${role_manage-users}', 'manage-users', 'master', 'a965bf32-66d4-41a3-8b65-c9747ad2ac41', NULL);
INSERT INTO auth.keycloak_role VALUES ('8ded8850-7b73-48ec-8faf-89042a9a81c0', 'a965bf32-66d4-41a3-8b65-c9747ad2ac41', true, '${role_manage-clients}', 'manage-clients', 'master', 'a965bf32-66d4-41a3-8b65-c9747ad2ac41', NULL);
INSERT INTO auth.keycloak_role VALUES ('1a22dbe4-b81e-4cae-b038-c2d9638c4b7d', 'a965bf32-66d4-41a3-8b65-c9747ad2ac41', true, '${role_manage-events}', 'manage-events', 'master', 'a965bf32-66d4-41a3-8b65-c9747ad2ac41', NULL);
INSERT INTO auth.keycloak_role VALUES ('09f22d46-655e-4fe1-a9e5-ffde9015160a', 'a965bf32-66d4-41a3-8b65-c9747ad2ac41', true, '${role_manage-identity-providers}', 'manage-identity-providers', 'master', 'a965bf32-66d4-41a3-8b65-c9747ad2ac41', NULL);
INSERT INTO auth.keycloak_role VALUES ('c62fff6d-a217-48ff-a132-5ea4237dcc6a', 'a965bf32-66d4-41a3-8b65-c9747ad2ac41', true, '${role_manage-authorization}', 'manage-authorization', 'master', 'a965bf32-66d4-41a3-8b65-c9747ad2ac41', NULL);
INSERT INTO auth.keycloak_role VALUES ('6c752745-e71a-465e-9729-6c4feb35d91e', 'a965bf32-66d4-41a3-8b65-c9747ad2ac41', true, '${role_query-users}', 'query-users', 'master', 'a965bf32-66d4-41a3-8b65-c9747ad2ac41', NULL);
INSERT INTO auth.keycloak_role VALUES ('a68a5254-39c1-4b95-8df3-1b18de4bbd08', 'a965bf32-66d4-41a3-8b65-c9747ad2ac41', true, '${role_query-clients}', 'query-clients', 'master', 'a965bf32-66d4-41a3-8b65-c9747ad2ac41', NULL);
INSERT INTO auth.keycloak_role VALUES ('5765c0ef-cc9a-4993-ad16-59aa1a4870c7', 'a965bf32-66d4-41a3-8b65-c9747ad2ac41', true, '${role_query-realms}', 'query-realms', 'master', 'a965bf32-66d4-41a3-8b65-c9747ad2ac41', NULL);
INSERT INTO auth.keycloak_role VALUES ('57cd65f9-e1c9-402e-ad6a-572a782033c8', 'a965bf32-66d4-41a3-8b65-c9747ad2ac41', true, '${role_query-groups}', 'query-groups', 'master', 'a965bf32-66d4-41a3-8b65-c9747ad2ac41', NULL);
INSERT INTO auth.keycloak_role VALUES ('40830bf7-5ad4-4a78-84c7-e2e8bc464286', '79d753fd-3540-4b38-862a-f1b1852628fe', true, '${role_realm-admin}', 'realm-admin', 'covid-19-response', '79d753fd-3540-4b38-862a-f1b1852628fe', NULL);
INSERT INTO auth.keycloak_role VALUES ('e5ec89d0-a653-4147-b43c-490769626dde', '79d753fd-3540-4b38-862a-f1b1852628fe', true, '${role_create-client}', 'create-client', 'covid-19-response', '79d753fd-3540-4b38-862a-f1b1852628fe', NULL);
INSERT INTO auth.keycloak_role VALUES ('90d99e25-83f6-4b4c-9d1e-25fa89701194', '79d753fd-3540-4b38-862a-f1b1852628fe', true, '${role_view-realm}', 'view-realm', 'covid-19-response', '79d753fd-3540-4b38-862a-f1b1852628fe', NULL);
INSERT INTO auth.keycloak_role VALUES ('8e158abe-5758-4358-8c7d-4b20536bb6a7', '79d753fd-3540-4b38-862a-f1b1852628fe', true, '${role_view-users}', 'view-users', 'covid-19-response', '79d753fd-3540-4b38-862a-f1b1852628fe', NULL);
INSERT INTO auth.keycloak_role VALUES ('2c094da4-1417-4cd6-ad9a-8ed51567da35', '79d753fd-3540-4b38-862a-f1b1852628fe', true, '${role_view-clients}', 'view-clients', 'covid-19-response', '79d753fd-3540-4b38-862a-f1b1852628fe', NULL);
INSERT INTO auth.keycloak_role VALUES ('bc3140d9-a709-44bc-9f92-a3cd4a7d3a9f', '79d753fd-3540-4b38-862a-f1b1852628fe', true, '${role_view-events}', 'view-events', 'covid-19-response', '79d753fd-3540-4b38-862a-f1b1852628fe', NULL);
INSERT INTO auth.keycloak_role VALUES ('c248d505-9717-44ce-a750-b27faedbf59a', '79d753fd-3540-4b38-862a-f1b1852628fe', true, '${role_view-identity-providers}', 'view-identity-providers', 'covid-19-response', '79d753fd-3540-4b38-862a-f1b1852628fe', NULL);
INSERT INTO auth.keycloak_role VALUES ('bc2acc7e-fe0b-4265-8c4d-5e64179ff7ad', '79d753fd-3540-4b38-862a-f1b1852628fe', true, '${role_view-authorization}', 'view-authorization', 'covid-19-response', '79d753fd-3540-4b38-862a-f1b1852628fe', NULL);
INSERT INTO auth.keycloak_role VALUES ('698e008d-daa4-4af4-b5e9-715d25625bff', '79d753fd-3540-4b38-862a-f1b1852628fe', true, '${role_manage-realm}', 'manage-realm', 'covid-19-response', '79d753fd-3540-4b38-862a-f1b1852628fe', NULL);
INSERT INTO auth.keycloak_role VALUES ('e7ef0adb-7c2a-4602-95cf-e058adbdff94', '79d753fd-3540-4b38-862a-f1b1852628fe', true, '${role_manage-users}', 'manage-users', 'covid-19-response', '79d753fd-3540-4b38-862a-f1b1852628fe', NULL);
INSERT INTO auth.keycloak_role VALUES ('c341a789-0fc2-4a1a-9195-92a4a5da9bb6', '79d753fd-3540-4b38-862a-f1b1852628fe', true, '${role_manage-clients}', 'manage-clients', 'covid-19-response', '79d753fd-3540-4b38-862a-f1b1852628fe', NULL);
INSERT INTO auth.keycloak_role VALUES ('e858fccb-b989-4b31-bf51-24a48488ada3', '79d753fd-3540-4b38-862a-f1b1852628fe', true, '${role_manage-events}', 'manage-events', 'covid-19-response', '79d753fd-3540-4b38-862a-f1b1852628fe', NULL);
INSERT INTO auth.keycloak_role VALUES ('4d6bb3c6-a97f-4f9f-a137-fe632feb9377', '79d753fd-3540-4b38-862a-f1b1852628fe', true, '${role_manage-identity-providers}', 'manage-identity-providers', 'covid-19-response', '79d753fd-3540-4b38-862a-f1b1852628fe', NULL);
INSERT INTO auth.keycloak_role VALUES ('105e31f4-4dcb-4fc9-b0e5-3550da4cf53f', '79d753fd-3540-4b38-862a-f1b1852628fe', true, '${role_manage-authorization}', 'manage-authorization', 'covid-19-response', '79d753fd-3540-4b38-862a-f1b1852628fe', NULL);
INSERT INTO auth.keycloak_role VALUES ('fd7c904c-ddfb-4545-92f9-380f6964b861', '79d753fd-3540-4b38-862a-f1b1852628fe', true, '${role_query-users}', 'query-users', 'covid-19-response', '79d753fd-3540-4b38-862a-f1b1852628fe', NULL);
INSERT INTO auth.keycloak_role VALUES ('b76d64e1-be19-4ce9-b388-dc792a13df00', '79d753fd-3540-4b38-862a-f1b1852628fe', true, '${role_query-clients}', 'query-clients', 'covid-19-response', '79d753fd-3540-4b38-862a-f1b1852628fe', NULL);
INSERT INTO auth.keycloak_role VALUES ('490b15ee-5a66-4a34-ab0f-6c04468e6c05', '79d753fd-3540-4b38-862a-f1b1852628fe', true, '${role_query-realms}', 'query-realms', 'covid-19-response', '79d753fd-3540-4b38-862a-f1b1852628fe', NULL);
INSERT INTO auth.keycloak_role VALUES ('c141a289-d0ab-4835-af72-372b84793d2d', '79d753fd-3540-4b38-862a-f1b1852628fe', true, '${role_query-groups}', 'query-groups', 'covid-19-response', '79d753fd-3540-4b38-862a-f1b1852628fe', NULL);
INSERT INTO auth.keycloak_role VALUES ('5be77ce0-89ec-4e1b-8e62-ae3e04ad2303', 'de4d38cb-f94e-48ee-8d2d-58f7e84a56e6', true, '${role_view-profile}', 'view-profile', 'covid-19-response', 'de4d38cb-f94e-48ee-8d2d-58f7e84a56e6', NULL);
INSERT INTO auth.keycloak_role VALUES ('3faf057f-3d35-4ddb-b32f-f3e8d066e824', 'de4d38cb-f94e-48ee-8d2d-58f7e84a56e6', true, '${role_manage-account}', 'manage-account', 'covid-19-response', 'de4d38cb-f94e-48ee-8d2d-58f7e84a56e6', NULL);
INSERT INTO auth.keycloak_role VALUES ('73d40e2e-22d0-443e-b33b-6fd2dda5fb00', 'de4d38cb-f94e-48ee-8d2d-58f7e84a56e6', true, '${role_manage-account-links}', 'manage-account-links', 'covid-19-response', 'de4d38cb-f94e-48ee-8d2d-58f7e84a56e6', NULL);
INSERT INTO auth.keycloak_role VALUES ('f828cd1e-a0bb-48d0-9e9f-220e4a7e7479', 'a965bf32-66d4-41a3-8b65-c9747ad2ac41', true, '${role_impersonation}', 'impersonation', 'master', 'a965bf32-66d4-41a3-8b65-c9747ad2ac41', NULL);
INSERT INTO auth.keycloak_role VALUES ('3eb1fa86-9976-49c0-bff0-76b9f51e4849', '79d753fd-3540-4b38-862a-f1b1852628fe', true, '${role_impersonation}', 'impersonation', 'covid-19-response', '79d753fd-3540-4b38-862a-f1b1852628fe', NULL);
INSERT INTO auth.keycloak_role VALUES ('45aa7a04-0195-47cb-bf52-3105656d36d6', 'e58c9a0e-a896-485c-9bde-8af66fbb5144', true, '${role_read-token}', 'read-token', 'covid-19-response', 'e58c9a0e-a896-485c-9bde-8af66fbb5144', NULL);
INSERT INTO auth.keycloak_role VALUES ('f1bee91c-a966-489f-8613-17f3b2c4f97a', 'covid-19-response', false, '${role_offline-access}', 'offline_access', 'covid-19-response', NULL, 'covid-19-response');
INSERT INTO auth.keycloak_role VALUES ('45778388-9cb7-41d9-a60c-fb85a4626823', 'covid-19-response', false, '${role_uma_authorization}', 'uma_authorization', 'covid-19-response', NULL, 'covid-19-response');
INSERT INTO auth.keycloak_role VALUES ('72cd3b20-4531-467f-8f07-5905c105fad7', 'covid-19-response', false, NULL, 'user', 'covid-19-response', NULL, 'covid-19-response');
INSERT INTO auth.keycloak_role VALUES ('40885237-9f24-4e73-8e4d-01de149c6af5', 'covid-19-response', false, NULL, 'physician', 'covid-19-response', NULL, 'covid-19-response');
INSERT INTO auth.keycloak_role VALUES ('e1742c92-1ffb-4733-a56e-1f7895a47f9c', 'covid-19-response', false, NULL, 'response-coordinator', 'covid-19-response', NULL, 'covid-19-response');


--
-- Data for Name: client_default_roles; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--

INSERT INTO auth.client_default_roles VALUES ('e5cc98c8-b77c-45d4-94c3-7880a3212ce9', '02326645-6d43-4026-9291-e6961f7ab352');
INSERT INTO auth.client_default_roles VALUES ('e5cc98c8-b77c-45d4-94c3-7880a3212ce9', 'b673e7a0-bd70-4543-b840-c1c6cb193bee');
INSERT INTO auth.client_default_roles VALUES ('de4d38cb-f94e-48ee-8d2d-58f7e84a56e6', '5be77ce0-89ec-4e1b-8e62-ae3e04ad2303');
INSERT INTO auth.client_default_roles VALUES ('de4d38cb-f94e-48ee-8d2d-58f7e84a56e6', '3faf057f-3d35-4ddb-b32f-f3e8d066e824');


--
-- Data for Name: client_initial_access; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: client_node_registrations; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: client_scope; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--

INSERT INTO auth.client_scope VALUES ('6fb2a610-f30d-422e-8989-65de1384b2a7', 'offline_access', 'master', 'OpenID Connect built-in scope: offline_access', 'openid-connect');
INSERT INTO auth.client_scope VALUES ('137bbf78-f128-4430-a903-3f75d2dc21c1', 'role_list', 'master', 'SAML role list', 'saml');
INSERT INTO auth.client_scope VALUES ('01e619ba-b33a-43f1-9fce-275a97bd1ed9', 'profile', 'master', 'OpenID Connect built-in scope: profile', 'openid-connect');
INSERT INTO auth.client_scope VALUES ('04f08721-3b7f-474f-a832-fd10dbb25f93', 'email', 'master', 'OpenID Connect built-in scope: email', 'openid-connect');
INSERT INTO auth.client_scope VALUES ('6af199c9-55c2-4bfd-8d84-4754ac4e0785', 'address', 'master', 'OpenID Connect built-in scope: address', 'openid-connect');
INSERT INTO auth.client_scope VALUES ('dce4aead-463e-404c-b6d3-8b8af9848858', 'phone', 'master', 'OpenID Connect built-in scope: phone', 'openid-connect');
INSERT INTO auth.client_scope VALUES ('e673129b-836e-4d83-b2e3-29222d509cbd', 'roles', 'master', 'OpenID Connect scope for add user roles to the access token', 'openid-connect');
INSERT INTO auth.client_scope VALUES ('60a35141-6a11-4cf7-b6e6-d96551bf769f', 'web-origins', 'master', 'OpenID Connect scope for add allowed web origins to the access token', 'openid-connect');
INSERT INTO auth.client_scope VALUES ('d8127c92-669b-4103-9589-18a43f0b96d0', 'microprofile-jwt', 'master', 'Microprofile - JWT built-in scope', 'openid-connect');
INSERT INTO auth.client_scope VALUES ('09b00872-fac3-4e22-b8e4-58b8e5b8974e', 'offline_access', 'covid-19-response', 'OpenID Connect built-in scope: offline_access', 'openid-connect');
INSERT INTO auth.client_scope VALUES ('d7aa712a-765a-4781-bc44-71cd45e9478d', 'role_list', 'covid-19-response', 'SAML role list', 'saml');
INSERT INTO auth.client_scope VALUES ('be2e69a2-3bab-4dc6-ad0b-08da74ad6639', 'profile', 'covid-19-response', 'OpenID Connect built-in scope: profile', 'openid-connect');
INSERT INTO auth.client_scope VALUES ('893f5998-d9e1-4abe-97d6-c76d36eeca5b', 'email', 'covid-19-response', 'OpenID Connect built-in scope: email', 'openid-connect');
INSERT INTO auth.client_scope VALUES ('9e9d9d47-955f-4caa-b1d7-6270c3ec9851', 'address', 'covid-19-response', 'OpenID Connect built-in scope: address', 'openid-connect');
INSERT INTO auth.client_scope VALUES ('f983b788-77ec-4d94-827a-381cd4a09fdb', 'phone', 'covid-19-response', 'OpenID Connect built-in scope: phone', 'openid-connect');
INSERT INTO auth.client_scope VALUES ('c3087358-01ce-4008-b98c-ef8a63314098', 'roles', 'covid-19-response', 'OpenID Connect scope for add user roles to the access token', 'openid-connect');
INSERT INTO auth.client_scope VALUES ('94394280-1348-44e2-b162-3ae3423ea30c', 'web-origins', 'covid-19-response', 'OpenID Connect scope for add allowed web origins to the access token', 'openid-connect');
INSERT INTO auth.client_scope VALUES ('166fc10c-ad21-428e-b44c-957572891d70', 'microprofile-jwt', 'covid-19-response', 'Microprofile - JWT built-in scope', 'openid-connect');
INSERT INTO auth.client_scope VALUES ('22f7b8c8-2916-45c5-ab2d-04f9604531ed', 'temperature-tracker', 'covid-19-response', NULL, 'openid-connect');


--
-- Data for Name: client_scope_attributes; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--

INSERT INTO auth.client_scope_attributes VALUES ('6fb2a610-f30d-422e-8989-65de1384b2a7', 'true', 'display.on.consent.screen');
INSERT INTO auth.client_scope_attributes VALUES ('6fb2a610-f30d-422e-8989-65de1384b2a7', '${offlineAccessScopeConsentText}', 'consent.screen.text');
INSERT INTO auth.client_scope_attributes VALUES ('137bbf78-f128-4430-a903-3f75d2dc21c1', 'true', 'display.on.consent.screen');
INSERT INTO auth.client_scope_attributes VALUES ('137bbf78-f128-4430-a903-3f75d2dc21c1', '${samlRoleListScopeConsentText}', 'consent.screen.text');
INSERT INTO auth.client_scope_attributes VALUES ('01e619ba-b33a-43f1-9fce-275a97bd1ed9', 'true', 'display.on.consent.screen');
INSERT INTO auth.client_scope_attributes VALUES ('01e619ba-b33a-43f1-9fce-275a97bd1ed9', '${profileScopeConsentText}', 'consent.screen.text');
INSERT INTO auth.client_scope_attributes VALUES ('01e619ba-b33a-43f1-9fce-275a97bd1ed9', 'true', 'include.in.token.scope');
INSERT INTO auth.client_scope_attributes VALUES ('04f08721-3b7f-474f-a832-fd10dbb25f93', 'true', 'display.on.consent.screen');
INSERT INTO auth.client_scope_attributes VALUES ('04f08721-3b7f-474f-a832-fd10dbb25f93', '${emailScopeConsentText}', 'consent.screen.text');
INSERT INTO auth.client_scope_attributes VALUES ('04f08721-3b7f-474f-a832-fd10dbb25f93', 'true', 'include.in.token.scope');
INSERT INTO auth.client_scope_attributes VALUES ('6af199c9-55c2-4bfd-8d84-4754ac4e0785', 'true', 'display.on.consent.screen');
INSERT INTO auth.client_scope_attributes VALUES ('6af199c9-55c2-4bfd-8d84-4754ac4e0785', '${addressScopeConsentText}', 'consent.screen.text');
INSERT INTO auth.client_scope_attributes VALUES ('6af199c9-55c2-4bfd-8d84-4754ac4e0785', 'true', 'include.in.token.scope');
INSERT INTO auth.client_scope_attributes VALUES ('dce4aead-463e-404c-b6d3-8b8af9848858', 'true', 'display.on.consent.screen');
INSERT INTO auth.client_scope_attributes VALUES ('dce4aead-463e-404c-b6d3-8b8af9848858', '${phoneScopeConsentText}', 'consent.screen.text');
INSERT INTO auth.client_scope_attributes VALUES ('dce4aead-463e-404c-b6d3-8b8af9848858', 'true', 'include.in.token.scope');
INSERT INTO auth.client_scope_attributes VALUES ('e673129b-836e-4d83-b2e3-29222d509cbd', 'true', 'display.on.consent.screen');
INSERT INTO auth.client_scope_attributes VALUES ('e673129b-836e-4d83-b2e3-29222d509cbd', '${rolesScopeConsentText}', 'consent.screen.text');
INSERT INTO auth.client_scope_attributes VALUES ('e673129b-836e-4d83-b2e3-29222d509cbd', 'false', 'include.in.token.scope');
INSERT INTO auth.client_scope_attributes VALUES ('60a35141-6a11-4cf7-b6e6-d96551bf769f', 'false', 'display.on.consent.screen');
INSERT INTO auth.client_scope_attributes VALUES ('60a35141-6a11-4cf7-b6e6-d96551bf769f', '', 'consent.screen.text');
INSERT INTO auth.client_scope_attributes VALUES ('60a35141-6a11-4cf7-b6e6-d96551bf769f', 'false', 'include.in.token.scope');
INSERT INTO auth.client_scope_attributes VALUES ('d8127c92-669b-4103-9589-18a43f0b96d0', 'false', 'display.on.consent.screen');
INSERT INTO auth.client_scope_attributes VALUES ('d8127c92-669b-4103-9589-18a43f0b96d0', 'true', 'include.in.token.scope');
INSERT INTO auth.client_scope_attributes VALUES ('09b00872-fac3-4e22-b8e4-58b8e5b8974e', 'true', 'display.on.consent.screen');
INSERT INTO auth.client_scope_attributes VALUES ('09b00872-fac3-4e22-b8e4-58b8e5b8974e', '${offlineAccessScopeConsentText}', 'consent.screen.text');
INSERT INTO auth.client_scope_attributes VALUES ('d7aa712a-765a-4781-bc44-71cd45e9478d', 'true', 'display.on.consent.screen');
INSERT INTO auth.client_scope_attributes VALUES ('d7aa712a-765a-4781-bc44-71cd45e9478d', '${samlRoleListScopeConsentText}', 'consent.screen.text');
INSERT INTO auth.client_scope_attributes VALUES ('be2e69a2-3bab-4dc6-ad0b-08da74ad6639', 'true', 'display.on.consent.screen');
INSERT INTO auth.client_scope_attributes VALUES ('be2e69a2-3bab-4dc6-ad0b-08da74ad6639', '${profileScopeConsentText}', 'consent.screen.text');
INSERT INTO auth.client_scope_attributes VALUES ('be2e69a2-3bab-4dc6-ad0b-08da74ad6639', 'true', 'include.in.token.scope');
INSERT INTO auth.client_scope_attributes VALUES ('893f5998-d9e1-4abe-97d6-c76d36eeca5b', 'true', 'display.on.consent.screen');
INSERT INTO auth.client_scope_attributes VALUES ('893f5998-d9e1-4abe-97d6-c76d36eeca5b', '${emailScopeConsentText}', 'consent.screen.text');
INSERT INTO auth.client_scope_attributes VALUES ('893f5998-d9e1-4abe-97d6-c76d36eeca5b', 'true', 'include.in.token.scope');
INSERT INTO auth.client_scope_attributes VALUES ('9e9d9d47-955f-4caa-b1d7-6270c3ec9851', 'true', 'display.on.consent.screen');
INSERT INTO auth.client_scope_attributes VALUES ('9e9d9d47-955f-4caa-b1d7-6270c3ec9851', '${addressScopeConsentText}', 'consent.screen.text');
INSERT INTO auth.client_scope_attributes VALUES ('9e9d9d47-955f-4caa-b1d7-6270c3ec9851', 'true', 'include.in.token.scope');
INSERT INTO auth.client_scope_attributes VALUES ('f983b788-77ec-4d94-827a-381cd4a09fdb', 'true', 'display.on.consent.screen');
INSERT INTO auth.client_scope_attributes VALUES ('f983b788-77ec-4d94-827a-381cd4a09fdb', '${phoneScopeConsentText}', 'consent.screen.text');
INSERT INTO auth.client_scope_attributes VALUES ('f983b788-77ec-4d94-827a-381cd4a09fdb', 'true', 'include.in.token.scope');
INSERT INTO auth.client_scope_attributes VALUES ('c3087358-01ce-4008-b98c-ef8a63314098', 'true', 'display.on.consent.screen');
INSERT INTO auth.client_scope_attributes VALUES ('c3087358-01ce-4008-b98c-ef8a63314098', '${rolesScopeConsentText}', 'consent.screen.text');
INSERT INTO auth.client_scope_attributes VALUES ('c3087358-01ce-4008-b98c-ef8a63314098', 'false', 'include.in.token.scope');
INSERT INTO auth.client_scope_attributes VALUES ('94394280-1348-44e2-b162-3ae3423ea30c', 'false', 'display.on.consent.screen');
INSERT INTO auth.client_scope_attributes VALUES ('94394280-1348-44e2-b162-3ae3423ea30c', '', 'consent.screen.text');
INSERT INTO auth.client_scope_attributes VALUES ('94394280-1348-44e2-b162-3ae3423ea30c', 'false', 'include.in.token.scope');
INSERT INTO auth.client_scope_attributes VALUES ('166fc10c-ad21-428e-b44c-957572891d70', 'false', 'display.on.consent.screen');
INSERT INTO auth.client_scope_attributes VALUES ('166fc10c-ad21-428e-b44c-957572891d70', 'true', 'include.in.token.scope');
INSERT INTO auth.client_scope_attributes VALUES ('22f7b8c8-2916-45c5-ab2d-04f9604531ed', 'true', 'display.on.consent.screen');
INSERT INTO auth.client_scope_attributes VALUES ('22f7b8c8-2916-45c5-ab2d-04f9604531ed', 'true', 'include.in.token.scope');


--
-- Data for Name: client_scope_client; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--

INSERT INTO auth.client_scope_client VALUES ('e5cc98c8-b77c-45d4-94c3-7880a3212ce9', '137bbf78-f128-4430-a903-3f75d2dc21c1', true);
INSERT INTO auth.client_scope_client VALUES ('920f387e-f7d2-46a3-8ef3-c0550b6c0ae3', '137bbf78-f128-4430-a903-3f75d2dc21c1', true);
INSERT INTO auth.client_scope_client VALUES ('74c608e3-e006-402f-b6d3-253bd1ec5015', '137bbf78-f128-4430-a903-3f75d2dc21c1', true);
INSERT INTO auth.client_scope_client VALUES ('1e839edf-2187-4e52-b927-f5c883ad0177', '137bbf78-f128-4430-a903-3f75d2dc21c1', true);
INSERT INTO auth.client_scope_client VALUES ('2834224e-a18a-4c06-bb03-62e6232ee4dc', '137bbf78-f128-4430-a903-3f75d2dc21c1', true);
INSERT INTO auth.client_scope_client VALUES ('e5cc98c8-b77c-45d4-94c3-7880a3212ce9', '01e619ba-b33a-43f1-9fce-275a97bd1ed9', true);
INSERT INTO auth.client_scope_client VALUES ('e5cc98c8-b77c-45d4-94c3-7880a3212ce9', '04f08721-3b7f-474f-a832-fd10dbb25f93', true);
INSERT INTO auth.client_scope_client VALUES ('e5cc98c8-b77c-45d4-94c3-7880a3212ce9', 'e673129b-836e-4d83-b2e3-29222d509cbd', true);
INSERT INTO auth.client_scope_client VALUES ('e5cc98c8-b77c-45d4-94c3-7880a3212ce9', '60a35141-6a11-4cf7-b6e6-d96551bf769f', true);
INSERT INTO auth.client_scope_client VALUES ('e5cc98c8-b77c-45d4-94c3-7880a3212ce9', '6fb2a610-f30d-422e-8989-65de1384b2a7', false);
INSERT INTO auth.client_scope_client VALUES ('e5cc98c8-b77c-45d4-94c3-7880a3212ce9', '6af199c9-55c2-4bfd-8d84-4754ac4e0785', false);
INSERT INTO auth.client_scope_client VALUES ('e5cc98c8-b77c-45d4-94c3-7880a3212ce9', 'dce4aead-463e-404c-b6d3-8b8af9848858', false);
INSERT INTO auth.client_scope_client VALUES ('e5cc98c8-b77c-45d4-94c3-7880a3212ce9', 'd8127c92-669b-4103-9589-18a43f0b96d0', false);
INSERT INTO auth.client_scope_client VALUES ('920f387e-f7d2-46a3-8ef3-c0550b6c0ae3', '01e619ba-b33a-43f1-9fce-275a97bd1ed9', true);
INSERT INTO auth.client_scope_client VALUES ('920f387e-f7d2-46a3-8ef3-c0550b6c0ae3', '04f08721-3b7f-474f-a832-fd10dbb25f93', true);
INSERT INTO auth.client_scope_client VALUES ('920f387e-f7d2-46a3-8ef3-c0550b6c0ae3', 'e673129b-836e-4d83-b2e3-29222d509cbd', true);
INSERT INTO auth.client_scope_client VALUES ('920f387e-f7d2-46a3-8ef3-c0550b6c0ae3', '60a35141-6a11-4cf7-b6e6-d96551bf769f', true);
INSERT INTO auth.client_scope_client VALUES ('920f387e-f7d2-46a3-8ef3-c0550b6c0ae3', '6fb2a610-f30d-422e-8989-65de1384b2a7', false);
INSERT INTO auth.client_scope_client VALUES ('920f387e-f7d2-46a3-8ef3-c0550b6c0ae3', '6af199c9-55c2-4bfd-8d84-4754ac4e0785', false);
INSERT INTO auth.client_scope_client VALUES ('920f387e-f7d2-46a3-8ef3-c0550b6c0ae3', 'dce4aead-463e-404c-b6d3-8b8af9848858', false);
INSERT INTO auth.client_scope_client VALUES ('920f387e-f7d2-46a3-8ef3-c0550b6c0ae3', 'd8127c92-669b-4103-9589-18a43f0b96d0', false);
INSERT INTO auth.client_scope_client VALUES ('74c608e3-e006-402f-b6d3-253bd1ec5015', '01e619ba-b33a-43f1-9fce-275a97bd1ed9', true);
INSERT INTO auth.client_scope_client VALUES ('74c608e3-e006-402f-b6d3-253bd1ec5015', '04f08721-3b7f-474f-a832-fd10dbb25f93', true);
INSERT INTO auth.client_scope_client VALUES ('74c608e3-e006-402f-b6d3-253bd1ec5015', 'e673129b-836e-4d83-b2e3-29222d509cbd', true);
INSERT INTO auth.client_scope_client VALUES ('74c608e3-e006-402f-b6d3-253bd1ec5015', '60a35141-6a11-4cf7-b6e6-d96551bf769f', true);
INSERT INTO auth.client_scope_client VALUES ('74c608e3-e006-402f-b6d3-253bd1ec5015', '6fb2a610-f30d-422e-8989-65de1384b2a7', false);
INSERT INTO auth.client_scope_client VALUES ('74c608e3-e006-402f-b6d3-253bd1ec5015', '6af199c9-55c2-4bfd-8d84-4754ac4e0785', false);
INSERT INTO auth.client_scope_client VALUES ('74c608e3-e006-402f-b6d3-253bd1ec5015', 'dce4aead-463e-404c-b6d3-8b8af9848858', false);
INSERT INTO auth.client_scope_client VALUES ('74c608e3-e006-402f-b6d3-253bd1ec5015', 'd8127c92-669b-4103-9589-18a43f0b96d0', false);
INSERT INTO auth.client_scope_client VALUES ('1e839edf-2187-4e52-b927-f5c883ad0177', '01e619ba-b33a-43f1-9fce-275a97bd1ed9', true);
INSERT INTO auth.client_scope_client VALUES ('1e839edf-2187-4e52-b927-f5c883ad0177', '04f08721-3b7f-474f-a832-fd10dbb25f93', true);
INSERT INTO auth.client_scope_client VALUES ('1e839edf-2187-4e52-b927-f5c883ad0177', 'e673129b-836e-4d83-b2e3-29222d509cbd', true);
INSERT INTO auth.client_scope_client VALUES ('1e839edf-2187-4e52-b927-f5c883ad0177', '60a35141-6a11-4cf7-b6e6-d96551bf769f', true);
INSERT INTO auth.client_scope_client VALUES ('1e839edf-2187-4e52-b927-f5c883ad0177', '6fb2a610-f30d-422e-8989-65de1384b2a7', false);
INSERT INTO auth.client_scope_client VALUES ('1e839edf-2187-4e52-b927-f5c883ad0177', '6af199c9-55c2-4bfd-8d84-4754ac4e0785', false);
INSERT INTO auth.client_scope_client VALUES ('1e839edf-2187-4e52-b927-f5c883ad0177', 'dce4aead-463e-404c-b6d3-8b8af9848858', false);
INSERT INTO auth.client_scope_client VALUES ('1e839edf-2187-4e52-b927-f5c883ad0177', 'd8127c92-669b-4103-9589-18a43f0b96d0', false);
INSERT INTO auth.client_scope_client VALUES ('2834224e-a18a-4c06-bb03-62e6232ee4dc', '01e619ba-b33a-43f1-9fce-275a97bd1ed9', true);
INSERT INTO auth.client_scope_client VALUES ('2834224e-a18a-4c06-bb03-62e6232ee4dc', '04f08721-3b7f-474f-a832-fd10dbb25f93', true);
INSERT INTO auth.client_scope_client VALUES ('2834224e-a18a-4c06-bb03-62e6232ee4dc', 'e673129b-836e-4d83-b2e3-29222d509cbd', true);
INSERT INTO auth.client_scope_client VALUES ('2834224e-a18a-4c06-bb03-62e6232ee4dc', '60a35141-6a11-4cf7-b6e6-d96551bf769f', true);
INSERT INTO auth.client_scope_client VALUES ('2834224e-a18a-4c06-bb03-62e6232ee4dc', '6fb2a610-f30d-422e-8989-65de1384b2a7', false);
INSERT INTO auth.client_scope_client VALUES ('2834224e-a18a-4c06-bb03-62e6232ee4dc', '6af199c9-55c2-4bfd-8d84-4754ac4e0785', false);
INSERT INTO auth.client_scope_client VALUES ('2834224e-a18a-4c06-bb03-62e6232ee4dc', 'dce4aead-463e-404c-b6d3-8b8af9848858', false);
INSERT INTO auth.client_scope_client VALUES ('2834224e-a18a-4c06-bb03-62e6232ee4dc', 'd8127c92-669b-4103-9589-18a43f0b96d0', false);
INSERT INTO auth.client_scope_client VALUES ('a965bf32-66d4-41a3-8b65-c9747ad2ac41', '137bbf78-f128-4430-a903-3f75d2dc21c1', true);
INSERT INTO auth.client_scope_client VALUES ('a965bf32-66d4-41a3-8b65-c9747ad2ac41', '01e619ba-b33a-43f1-9fce-275a97bd1ed9', true);
INSERT INTO auth.client_scope_client VALUES ('a965bf32-66d4-41a3-8b65-c9747ad2ac41', '04f08721-3b7f-474f-a832-fd10dbb25f93', true);
INSERT INTO auth.client_scope_client VALUES ('a965bf32-66d4-41a3-8b65-c9747ad2ac41', 'e673129b-836e-4d83-b2e3-29222d509cbd', true);
INSERT INTO auth.client_scope_client VALUES ('a965bf32-66d4-41a3-8b65-c9747ad2ac41', '60a35141-6a11-4cf7-b6e6-d96551bf769f', true);
INSERT INTO auth.client_scope_client VALUES ('a965bf32-66d4-41a3-8b65-c9747ad2ac41', '6fb2a610-f30d-422e-8989-65de1384b2a7', false);
INSERT INTO auth.client_scope_client VALUES ('a965bf32-66d4-41a3-8b65-c9747ad2ac41', '6af199c9-55c2-4bfd-8d84-4754ac4e0785', false);
INSERT INTO auth.client_scope_client VALUES ('a965bf32-66d4-41a3-8b65-c9747ad2ac41', 'dce4aead-463e-404c-b6d3-8b8af9848858', false);
INSERT INTO auth.client_scope_client VALUES ('a965bf32-66d4-41a3-8b65-c9747ad2ac41', 'd8127c92-669b-4103-9589-18a43f0b96d0', false);
INSERT INTO auth.client_scope_client VALUES ('de4d38cb-f94e-48ee-8d2d-58f7e84a56e6', 'd7aa712a-765a-4781-bc44-71cd45e9478d', true);
INSERT INTO auth.client_scope_client VALUES ('9a368257-2d7d-4fcc-84b8-e94eb60452f3', 'd7aa712a-765a-4781-bc44-71cd45e9478d', true);
INSERT INTO auth.client_scope_client VALUES ('e58c9a0e-a896-485c-9bde-8af66fbb5144', 'd7aa712a-765a-4781-bc44-71cd45e9478d', true);
INSERT INTO auth.client_scope_client VALUES ('79d753fd-3540-4b38-862a-f1b1852628fe', 'd7aa712a-765a-4781-bc44-71cd45e9478d', true);
INSERT INTO auth.client_scope_client VALUES ('22d35cbb-94aa-4004-bb96-82b3687c3441', 'd7aa712a-765a-4781-bc44-71cd45e9478d', true);
INSERT INTO auth.client_scope_client VALUES ('de4d38cb-f94e-48ee-8d2d-58f7e84a56e6', 'be2e69a2-3bab-4dc6-ad0b-08da74ad6639', true);
INSERT INTO auth.client_scope_client VALUES ('de4d38cb-f94e-48ee-8d2d-58f7e84a56e6', '893f5998-d9e1-4abe-97d6-c76d36eeca5b', true);
INSERT INTO auth.client_scope_client VALUES ('de4d38cb-f94e-48ee-8d2d-58f7e84a56e6', 'c3087358-01ce-4008-b98c-ef8a63314098', true);
INSERT INTO auth.client_scope_client VALUES ('de4d38cb-f94e-48ee-8d2d-58f7e84a56e6', '94394280-1348-44e2-b162-3ae3423ea30c', true);
INSERT INTO auth.client_scope_client VALUES ('de4d38cb-f94e-48ee-8d2d-58f7e84a56e6', '09b00872-fac3-4e22-b8e4-58b8e5b8974e', false);
INSERT INTO auth.client_scope_client VALUES ('de4d38cb-f94e-48ee-8d2d-58f7e84a56e6', '9e9d9d47-955f-4caa-b1d7-6270c3ec9851', false);
INSERT INTO auth.client_scope_client VALUES ('de4d38cb-f94e-48ee-8d2d-58f7e84a56e6', 'f983b788-77ec-4d94-827a-381cd4a09fdb', false);
INSERT INTO auth.client_scope_client VALUES ('de4d38cb-f94e-48ee-8d2d-58f7e84a56e6', '166fc10c-ad21-428e-b44c-957572891d70', false);
INSERT INTO auth.client_scope_client VALUES ('9a368257-2d7d-4fcc-84b8-e94eb60452f3', 'be2e69a2-3bab-4dc6-ad0b-08da74ad6639', true);
INSERT INTO auth.client_scope_client VALUES ('9a368257-2d7d-4fcc-84b8-e94eb60452f3', '893f5998-d9e1-4abe-97d6-c76d36eeca5b', true);
INSERT INTO auth.client_scope_client VALUES ('9a368257-2d7d-4fcc-84b8-e94eb60452f3', 'c3087358-01ce-4008-b98c-ef8a63314098', true);
INSERT INTO auth.client_scope_client VALUES ('9a368257-2d7d-4fcc-84b8-e94eb60452f3', '94394280-1348-44e2-b162-3ae3423ea30c', true);
INSERT INTO auth.client_scope_client VALUES ('9a368257-2d7d-4fcc-84b8-e94eb60452f3', '09b00872-fac3-4e22-b8e4-58b8e5b8974e', false);
INSERT INTO auth.client_scope_client VALUES ('9a368257-2d7d-4fcc-84b8-e94eb60452f3', '9e9d9d47-955f-4caa-b1d7-6270c3ec9851', false);
INSERT INTO auth.client_scope_client VALUES ('9a368257-2d7d-4fcc-84b8-e94eb60452f3', 'f983b788-77ec-4d94-827a-381cd4a09fdb', false);
INSERT INTO auth.client_scope_client VALUES ('9a368257-2d7d-4fcc-84b8-e94eb60452f3', '166fc10c-ad21-428e-b44c-957572891d70', false);
INSERT INTO auth.client_scope_client VALUES ('e58c9a0e-a896-485c-9bde-8af66fbb5144', 'be2e69a2-3bab-4dc6-ad0b-08da74ad6639', true);
INSERT INTO auth.client_scope_client VALUES ('e58c9a0e-a896-485c-9bde-8af66fbb5144', '893f5998-d9e1-4abe-97d6-c76d36eeca5b', true);
INSERT INTO auth.client_scope_client VALUES ('e58c9a0e-a896-485c-9bde-8af66fbb5144', 'c3087358-01ce-4008-b98c-ef8a63314098', true);
INSERT INTO auth.client_scope_client VALUES ('e58c9a0e-a896-485c-9bde-8af66fbb5144', '94394280-1348-44e2-b162-3ae3423ea30c', true);
INSERT INTO auth.client_scope_client VALUES ('e58c9a0e-a896-485c-9bde-8af66fbb5144', '09b00872-fac3-4e22-b8e4-58b8e5b8974e', false);
INSERT INTO auth.client_scope_client VALUES ('e58c9a0e-a896-485c-9bde-8af66fbb5144', '9e9d9d47-955f-4caa-b1d7-6270c3ec9851', false);
INSERT INTO auth.client_scope_client VALUES ('e58c9a0e-a896-485c-9bde-8af66fbb5144', 'f983b788-77ec-4d94-827a-381cd4a09fdb', false);
INSERT INTO auth.client_scope_client VALUES ('e58c9a0e-a896-485c-9bde-8af66fbb5144', '166fc10c-ad21-428e-b44c-957572891d70', false);
INSERT INTO auth.client_scope_client VALUES ('79d753fd-3540-4b38-862a-f1b1852628fe', 'be2e69a2-3bab-4dc6-ad0b-08da74ad6639', true);
INSERT INTO auth.client_scope_client VALUES ('79d753fd-3540-4b38-862a-f1b1852628fe', '893f5998-d9e1-4abe-97d6-c76d36eeca5b', true);
INSERT INTO auth.client_scope_client VALUES ('79d753fd-3540-4b38-862a-f1b1852628fe', 'c3087358-01ce-4008-b98c-ef8a63314098', true);
INSERT INTO auth.client_scope_client VALUES ('79d753fd-3540-4b38-862a-f1b1852628fe', '94394280-1348-44e2-b162-3ae3423ea30c', true);
INSERT INTO auth.client_scope_client VALUES ('79d753fd-3540-4b38-862a-f1b1852628fe', '09b00872-fac3-4e22-b8e4-58b8e5b8974e', false);
INSERT INTO auth.client_scope_client VALUES ('79d753fd-3540-4b38-862a-f1b1852628fe', '9e9d9d47-955f-4caa-b1d7-6270c3ec9851', false);
INSERT INTO auth.client_scope_client VALUES ('79d753fd-3540-4b38-862a-f1b1852628fe', 'f983b788-77ec-4d94-827a-381cd4a09fdb', false);
INSERT INTO auth.client_scope_client VALUES ('79d753fd-3540-4b38-862a-f1b1852628fe', '166fc10c-ad21-428e-b44c-957572891d70', false);
INSERT INTO auth.client_scope_client VALUES ('22d35cbb-94aa-4004-bb96-82b3687c3441', 'be2e69a2-3bab-4dc6-ad0b-08da74ad6639', true);
INSERT INTO auth.client_scope_client VALUES ('22d35cbb-94aa-4004-bb96-82b3687c3441', '893f5998-d9e1-4abe-97d6-c76d36eeca5b', true);
INSERT INTO auth.client_scope_client VALUES ('22d35cbb-94aa-4004-bb96-82b3687c3441', 'c3087358-01ce-4008-b98c-ef8a63314098', true);
INSERT INTO auth.client_scope_client VALUES ('22d35cbb-94aa-4004-bb96-82b3687c3441', '94394280-1348-44e2-b162-3ae3423ea30c', true);
INSERT INTO auth.client_scope_client VALUES ('22d35cbb-94aa-4004-bb96-82b3687c3441', '09b00872-fac3-4e22-b8e4-58b8e5b8974e', false);
INSERT INTO auth.client_scope_client VALUES ('22d35cbb-94aa-4004-bb96-82b3687c3441', '9e9d9d47-955f-4caa-b1d7-6270c3ec9851', false);
INSERT INTO auth.client_scope_client VALUES ('22d35cbb-94aa-4004-bb96-82b3687c3441', 'f983b788-77ec-4d94-827a-381cd4a09fdb', false);
INSERT INTO auth.client_scope_client VALUES ('22d35cbb-94aa-4004-bb96-82b3687c3441', '166fc10c-ad21-428e-b44c-957572891d70', false);
INSERT INTO auth.client_scope_client VALUES ('4ae3a8d3-031e-4332-bbeb-c05edb02f358', 'd7aa712a-765a-4781-bc44-71cd45e9478d', true);
INSERT INTO auth.client_scope_client VALUES ('4ae3a8d3-031e-4332-bbeb-c05edb02f358', 'be2e69a2-3bab-4dc6-ad0b-08da74ad6639', true);
INSERT INTO auth.client_scope_client VALUES ('4ae3a8d3-031e-4332-bbeb-c05edb02f358', '893f5998-d9e1-4abe-97d6-c76d36eeca5b', true);
INSERT INTO auth.client_scope_client VALUES ('4ae3a8d3-031e-4332-bbeb-c05edb02f358', 'c3087358-01ce-4008-b98c-ef8a63314098', true);
INSERT INTO auth.client_scope_client VALUES ('4ae3a8d3-031e-4332-bbeb-c05edb02f358', '94394280-1348-44e2-b162-3ae3423ea30c', true);
INSERT INTO auth.client_scope_client VALUES ('4ae3a8d3-031e-4332-bbeb-c05edb02f358', '09b00872-fac3-4e22-b8e4-58b8e5b8974e', false);
INSERT INTO auth.client_scope_client VALUES ('4ae3a8d3-031e-4332-bbeb-c05edb02f358', '9e9d9d47-955f-4caa-b1d7-6270c3ec9851', false);
INSERT INTO auth.client_scope_client VALUES ('4ae3a8d3-031e-4332-bbeb-c05edb02f358', 'f983b788-77ec-4d94-827a-381cd4a09fdb', false);
INSERT INTO auth.client_scope_client VALUES ('4ae3a8d3-031e-4332-bbeb-c05edb02f358', '166fc10c-ad21-428e-b44c-957572891d70', false);
INSERT INTO auth.client_scope_client VALUES ('4ae3a8d3-031e-4332-bbeb-c05edb02f358', '22f7b8c8-2916-45c5-ab2d-04f9604531ed', true);


--
-- Data for Name: client_scope_role_mapping; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--

INSERT INTO auth.client_scope_role_mapping VALUES ('6fb2a610-f30d-422e-8989-65de1384b2a7', 'a2f1c7f8-e931-4ae0-b554-32a351bbbb14');
INSERT INTO auth.client_scope_role_mapping VALUES ('09b00872-fac3-4e22-b8e4-58b8e5b8974e', 'f1bee91c-a966-489f-8613-17f3b2c4f97a');


--
-- Data for Name: user_session; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: client_session; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: client_session_auth_status; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: client_session_note; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: client_session_prot_mapper; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: client_session_role; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: client_user_session_note; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: component; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--

INSERT INTO auth.component VALUES ('2cdfd256-fd13-49e8-88c3-d683705a7637', 'Trusted Hosts', 'master', 'trusted-hosts', 'org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy', 'master', 'anonymous');
INSERT INTO auth.component VALUES ('aa691b67-0402-4fca-8982-f0f9f065a7e4', 'Consent Required', 'master', 'consent-required', 'org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy', 'master', 'anonymous');
INSERT INTO auth.component VALUES ('18758c46-f294-4279-8812-de95bb749692', 'Full Scope Disabled', 'master', 'scope', 'org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy', 'master', 'anonymous');
INSERT INTO auth.component VALUES ('d44efc15-3441-4017-bdb8-2139d310f4e2', 'Max Clients Limit', 'master', 'max-clients', 'org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy', 'master', 'anonymous');
INSERT INTO auth.component VALUES ('3bdce7ac-4b5d-42d3-8e99-facaa2b5c4bd', 'Allowed Protocol Mapper Types', 'master', 'allowed-protocol-mappers', 'org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy', 'master', 'anonymous');
INSERT INTO auth.component VALUES ('1f4d78bf-7639-436f-8b19-76ea68a39091', 'Allowed Client Scopes', 'master', 'allowed-client-templates', 'org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy', 'master', 'anonymous');
INSERT INTO auth.component VALUES ('1725fdaf-b18b-4cc0-8b65-156addaa9c5a', 'Allowed Protocol Mapper Types', 'master', 'allowed-protocol-mappers', 'org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy', 'master', 'authenticated');
INSERT INTO auth.component VALUES ('01f50d7c-aabd-45b8-b56b-439ab5fce4a8', 'Allowed Client Scopes', 'master', 'allowed-client-templates', 'org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy', 'master', 'authenticated');
INSERT INTO auth.component VALUES ('be1d85c9-8e5c-4f4a-bab4-782eb9f711fa', 'rsa-generated', 'master', 'rsa-generated', 'org.keycloak.keys.KeyProvider', 'master', NULL);
INSERT INTO auth.component VALUES ('9184d5d1-60ec-4c4a-853b-472968487163', 'hmac-generated', 'master', 'hmac-generated', 'org.keycloak.keys.KeyProvider', 'master', NULL);
INSERT INTO auth.component VALUES ('c83587a2-18ff-4052-9005-2ebd6502c341', 'aes-generated', 'master', 'aes-generated', 'org.keycloak.keys.KeyProvider', 'master', NULL);
INSERT INTO auth.component VALUES ('d4bab77b-981e-4028-9936-536a1f6e85e4', 'rsa-generated', 'covid-19-response', 'rsa-generated', 'org.keycloak.keys.KeyProvider', 'covid-19-response', NULL);
INSERT INTO auth.component VALUES ('9544d8bc-12e5-4441-ab5a-288a55a9fb27', 'hmac-generated', 'covid-19-response', 'hmac-generated', 'org.keycloak.keys.KeyProvider', 'covid-19-response', NULL);
INSERT INTO auth.component VALUES ('d31563e9-8834-4524-9591-04d42889a21e', 'aes-generated', 'covid-19-response', 'aes-generated', 'org.keycloak.keys.KeyProvider', 'covid-19-response', NULL);
INSERT INTO auth.component VALUES ('718ff553-e405-49a1-a03b-0ef64165aa34', 'Trusted Hosts', 'covid-19-response', 'trusted-hosts', 'org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy', 'covid-19-response', 'anonymous');
INSERT INTO auth.component VALUES ('36d35a45-700d-45fb-87fd-b79b10a2b174', 'Consent Required', 'covid-19-response', 'consent-required', 'org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy', 'covid-19-response', 'anonymous');
INSERT INTO auth.component VALUES ('dc761642-34f5-4144-9a02-f7603a14e8e1', 'Full Scope Disabled', 'covid-19-response', 'scope', 'org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy', 'covid-19-response', 'anonymous');
INSERT INTO auth.component VALUES ('067537ac-cde3-4905-8ea7-7ed4e98852cf', 'Max Clients Limit', 'covid-19-response', 'max-clients', 'org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy', 'covid-19-response', 'anonymous');
INSERT INTO auth.component VALUES ('ef548e3d-ea61-4472-bd0a-5c821e0f0506', 'Allowed Protocol Mapper Types', 'covid-19-response', 'allowed-protocol-mappers', 'org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy', 'covid-19-response', 'anonymous');
INSERT INTO auth.component VALUES ('7bc697d2-5691-4d3d-b840-5c5fd41a2524', 'Allowed Client Scopes', 'covid-19-response', 'allowed-client-templates', 'org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy', 'covid-19-response', 'anonymous');
INSERT INTO auth.component VALUES ('7e51b2f5-d8a1-434b-9943-59640f14f99f', 'Allowed Protocol Mapper Types', 'covid-19-response', 'allowed-protocol-mappers', 'org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy', 'covid-19-response', 'authenticated');
INSERT INTO auth.component VALUES ('bb8f3ede-67d1-48ab-892a-036ca2d881ef', 'Allowed Client Scopes', 'covid-19-response', 'allowed-client-templates', 'org.keycloak.services.clientregistration.policy.ClientRegistrationPolicy', 'covid-19-response', 'authenticated');


--
-- Data for Name: component_config; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--

INSERT INTO auth.component_config VALUES ('212d8dca-9699-4816-8111-c8841172396a', '3bdce7ac-4b5d-42d3-8e99-facaa2b5c4bd', 'allowed-protocol-mapper-types', 'saml-user-property-mapper');
INSERT INTO auth.component_config VALUES ('187b0226-a962-4145-a5e0-c8853a9037fb', '3bdce7ac-4b5d-42d3-8e99-facaa2b5c4bd', 'allowed-protocol-mapper-types', 'oidc-usermodel-property-mapper');
INSERT INTO auth.component_config VALUES ('f0b5d92e-88f5-4026-ae2f-13bea7c74345', '3bdce7ac-4b5d-42d3-8e99-facaa2b5c4bd', 'allowed-protocol-mapper-types', 'saml-user-attribute-mapper');
INSERT INTO auth.component_config VALUES ('f0ec82f2-9f6c-4ca9-b057-bda85d396ac7', '3bdce7ac-4b5d-42d3-8e99-facaa2b5c4bd', 'allowed-protocol-mapper-types', 'oidc-address-mapper');
INSERT INTO auth.component_config VALUES ('44ac360d-5c58-4215-b9db-addc8608bc7b', '3bdce7ac-4b5d-42d3-8e99-facaa2b5c4bd', 'allowed-protocol-mapper-types', 'oidc-sha256-pairwise-sub-mapper');
INSERT INTO auth.component_config VALUES ('e2b90119-d23d-40dd-88a9-a83b47d2e5b0', '3bdce7ac-4b5d-42d3-8e99-facaa2b5c4bd', 'allowed-protocol-mapper-types', 'saml-role-list-mapper');
INSERT INTO auth.component_config VALUES ('59c087a5-4d21-4931-9894-201ba4293de8', '3bdce7ac-4b5d-42d3-8e99-facaa2b5c4bd', 'allowed-protocol-mapper-types', 'oidc-usermodel-attribute-mapper');
INSERT INTO auth.component_config VALUES ('6e14f2f7-5ec1-45e0-998a-19de4b2ad12f', '3bdce7ac-4b5d-42d3-8e99-facaa2b5c4bd', 'allowed-protocol-mapper-types', 'oidc-full-name-mapper');
INSERT INTO auth.component_config VALUES ('8b067197-018a-44be-a19c-c4f81db4fa5b', '01f50d7c-aabd-45b8-b56b-439ab5fce4a8', 'allow-default-scopes', 'true');
INSERT INTO auth.component_config VALUES ('a02a4450-6456-4e1b-a847-ea9a5dd39c28', '1725fdaf-b18b-4cc0-8b65-156addaa9c5a', 'allowed-protocol-mapper-types', 'oidc-full-name-mapper');
INSERT INTO auth.component_config VALUES ('4536e7ff-dfc3-41ff-84ae-52214ed69c1b', '1725fdaf-b18b-4cc0-8b65-156addaa9c5a', 'allowed-protocol-mapper-types', 'oidc-sha256-pairwise-sub-mapper');
INSERT INTO auth.component_config VALUES ('4c50d77b-82ce-434d-b760-876697a07059', '1725fdaf-b18b-4cc0-8b65-156addaa9c5a', 'allowed-protocol-mapper-types', 'oidc-address-mapper');
INSERT INTO auth.component_config VALUES ('d826be16-21f6-4bd1-aaf7-ad928566f032', '1725fdaf-b18b-4cc0-8b65-156addaa9c5a', 'allowed-protocol-mapper-types', 'saml-user-property-mapper');
INSERT INTO auth.component_config VALUES ('1549ee13-d2eb-40c3-8ac3-a3515cee5809', '1725fdaf-b18b-4cc0-8b65-156addaa9c5a', 'allowed-protocol-mapper-types', 'saml-user-attribute-mapper');
INSERT INTO auth.component_config VALUES ('9c689199-ad5a-4818-8288-5cdfcb931e5f', '1725fdaf-b18b-4cc0-8b65-156addaa9c5a', 'allowed-protocol-mapper-types', 'saml-role-list-mapper');
INSERT INTO auth.component_config VALUES ('6125d683-6a19-41e6-9da8-2633ed1361ea', '1725fdaf-b18b-4cc0-8b65-156addaa9c5a', 'allowed-protocol-mapper-types', 'oidc-usermodel-attribute-mapper');
INSERT INTO auth.component_config VALUES ('5cba1acb-dff7-48b3-b487-453fe301ce69', '1725fdaf-b18b-4cc0-8b65-156addaa9c5a', 'allowed-protocol-mapper-types', 'oidc-usermodel-property-mapper');
INSERT INTO auth.component_config VALUES ('408d02b0-938e-4b85-9629-00b584f043c9', '2cdfd256-fd13-49e8-88c3-d683705a7637', 'client-uris-must-match', 'true');
INSERT INTO auth.component_config VALUES ('34bab319-e665-49fd-94e8-5273de369871', '2cdfd256-fd13-49e8-88c3-d683705a7637', 'host-sending-registration-request-must-match', 'true');
INSERT INTO auth.component_config VALUES ('964d5e24-6eac-4c7e-9f6d-4d956bb22e31', 'd44efc15-3441-4017-bdb8-2139d310f4e2', 'max-clients', '200');
INSERT INTO auth.component_config VALUES ('36430b69-ceb0-4552-95ae-d2ff37d59d7c', '1f4d78bf-7639-436f-8b19-76ea68a39091', 'allow-default-scopes', 'true');
INSERT INTO auth.component_config VALUES ('27c6128d-e218-4d3e-a6cc-90ffdedb73ba', 'c83587a2-18ff-4052-9005-2ebd6502c341', 'kid', 'efd53ed0-0924-4246-8127-b40291a38eb6');
INSERT INTO auth.component_config VALUES ('75ebdb06-c858-4c8e-9ecd-61801f691db3', 'c83587a2-18ff-4052-9005-2ebd6502c341', 'priority', '100');
INSERT INTO auth.component_config VALUES ('fbb91b85-457f-4f48-ab2e-9d4834010f17', 'c83587a2-18ff-4052-9005-2ebd6502c341', 'secret', 'rEA2rUFzp8mnpTZwVx6jVg');
INSERT INTO auth.component_config VALUES ('e1db5080-47e2-4fbf-9f11-5a0f83366b23', 'be1d85c9-8e5c-4f4a-bab4-782eb9f711fa', 'privateKey', 'MIIEowIBAAKCAQEAn5zrZvdn/44HqIvHT2TtvRe8BKTKVPrSXJlQ14nWpI0ipI6q+4nIHKehli1GlK77FqrRoXNexHGCVqhbBXmFHbF/mE7E2HDaruDXvQIlAvqwx7waIO/gjLAYfzUHHXGEaVm2Lgp42LYqdXWz7g5QB7oKH1T7hV9kDn5+IhIezaShX+UXW28DTXrd8hUEyxi0cSZDEBNwmo4fh+3zBmJ5RCXSgj58bLIVxkQvjqLWrDerccV0BgI9DX6fCH5Lh/d5mNzGfn4IU6vmzWjSzb5kjs6eIoTU0DWLhRh/L9FjcTnbYNzMKTRtwFeApkCD9RJpBcEjJxMHnOoU2nWpKIR80QIDAQABAoIBABpJNHdjnUv37wjx5CQOIhz3EK7PGMc/+27mjOrCN4gn+iGRoVUFjuAxNypNUIdh/dXw0c89+chuTSHKXhsbtTF7ruC9cCuZAlq0irME7GV+NajfMIpmFtv0kEQ6zm4p6L2U2azhap99+KYZeUVmNAXhvJMGYtnHKOo+KLWhuFLzXB5PEqyt1iVJdPwDveoIUnEWwou6l+Xa7e4MF6gBH+LpdV+rpa8mFUui5WgWFow/NFJuBj76YOGHYGsZAJFTB6pg+MpqHma6c5mzSI8SZT1WLN5LVZPW1Aynu1krpFZ3PB5mrBjjVtefEvBXmYKQjraEVVpeUY04DdNCnW6F5UECgYEA2h29PdvrO9S8NBOl91aQTzVVvgDnykg2jAX2LF5K0iDzBIEQF0+z6spXIIOht2fiF4WoMYLw945VD6YC87+zh+K0d6j74vLtwdhm2HaRtx1OatMeOtZZcuUuzwUogiJ3k+uVr5kRAOjBii/VbxO53F8IoYJJPfiHPtkJWD3DLXkCgYEAu1Xou8izIGpPLW1rPEgyPIuyErjCNBqJAwkMKGB9XOVayc2joUUJQgWfYLVLrgw+mrDhKdIDclP2uzriezwMOnLBpdOQyPjVPSuu1hzx/TeLlKp0GcUxmym50q4if938HbifBsB9wPmBjRGfhyely74DAtk9KL4tlei+rQ/2bBkCgYEApEHFI/KcAeDBIs6xK8TqxFu8CGySIgmj2msa6mrQGdJqcLfpMET7jxN17axzerUm1OP4t+COKz/q2vAARSPWxbx6f6qolk6vsk2PlfAk6CuPxG7c0x5dGmNxjsGpsebW+nRts2GoJnEiXziKmS3XyBdhXo/JLHwfDbtQiD1q4LECgYBh0dkl/QWz3m8brG/7WUOKI/PHHNyjcluqzxWfG8s6L7T5fTx9IFxWMjNnvyMWbeReyA+606PjFf7XA0dxzAgimPuPCGso1e4a8+1mdlTRwShMZ7kaH8LoX6BdZ3oyRIq6RkYbs3W2x1IjhFlFrKlBOyW9Xgw2cx/AkHjIS3aEOQKBgF9hbQsHDZpkzICoGy1G+qGGwChvMiojDLRLB0sUeL00vL7ZsaLGRr+KQQCg7sv/o48sbChUGLIzluuwXnE/K7uGmFyEGpIKXjtNtJ2M7xcqCM4QP2S2FtP+IuZi0waCXlT9k8togXvtnk7XGyXGKlx3TXPDLELVo20hW3Crp6vp');
INSERT INTO auth.component_config VALUES ('544028c9-054e-473b-a0cf-cff7fd2af3f6', 'be1d85c9-8e5c-4f4a-bab4-782eb9f711fa', 'certificate', 'MIICmzCCAYMCBgFxX1jlkTANBgkqhkiG9w0BAQsFADARMQ8wDQYDVQQDDAZtYXN0ZXIwHhcNMjAwNDA5MTQyODEzWhcNMzAwNDA5MTQyOTUzWjARMQ8wDQYDVQQDDAZtYXN0ZXIwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQCfnOtm92f/jgeoi8dPZO29F7wEpMpU+tJcmVDXidakjSKkjqr7icgcp6GWLUaUrvsWqtGhc17EcYJWqFsFeYUdsX+YTsTYcNqu4Ne9AiUC+rDHvBog7+CMsBh/NQcdcYRpWbYuCnjYtip1dbPuDlAHugofVPuFX2QOfn4iEh7NpKFf5RdbbwNNet3yFQTLGLRxJkMQE3Cajh+H7fMGYnlEJdKCPnxsshXGRC+OotasN6txxXQGAj0Nfp8IfkuH93mY3MZ+fghTq+bNaNLNvmSOzp4ihNTQNYuFGH8v0WNxOdtg3MwpNG3AV4CmQIP1EmkFwSMnEwec6hTadakohHzRAgMBAAEwDQYJKoZIhvcNAQELBQADggEBAFaoa/vvIKDYG/LwQebVXqS3sNUkENBk9tpuY3Tckn1orqHVnWd4miheZcoXCnwdJqgbEXjadgbNfFrarjgOvaxqRt+z9Y4qiUKtOqZSVJOCB+5NIK+qSNxA6KlPaabHDhSh8LsEsoICPr3cpP1Z0QNBNfFwNzuzuUU+maFcGe2POkwTZ7d1XZgYbVBDnWnnEG+peUtYiOwUOTktz7Hg7jRive97euIIBA8OWkwMHJzAt9AoqFNGtc/BkTzrIXA/7AE+4DjsRUs6zKIkoJ5NcJqssJ8TEBHhyBwocGxM6ty9X+NMjSoKdvXHERQVrlJEIR+F6ZUd7mhrODbLTCn77l8=');
INSERT INTO auth.component_config VALUES ('2ac399a7-99c0-4ea6-936f-cc165c5baa54', 'be1d85c9-8e5c-4f4a-bab4-782eb9f711fa', 'priority', '100');
INSERT INTO auth.component_config VALUES ('03a24198-db47-4043-a82d-27f2d6d5170c', '9184d5d1-60ec-4c4a-853b-472968487163', 'kid', '69c0a33b-d55e-4c80-abe1-a75131bac069');
INSERT INTO auth.component_config VALUES ('c9191e09-f2ff-4e3a-8826-f5daa737727a', '9184d5d1-60ec-4c4a-853b-472968487163', 'secret', 'iUzHQAohps_5ljSw4wBQkyd-eZtrnC2LUZbvytKtCoSVzPL2JFvFazAvdYJFiWYaSfqzvt_TXNbmLBcctioRiQ');
INSERT INTO auth.component_config VALUES ('4d10d004-9835-4139-91a7-a30838dddb9b', '9184d5d1-60ec-4c4a-853b-472968487163', 'priority', '100');
INSERT INTO auth.component_config VALUES ('f08da854-78be-4dd0-8132-0e1bc446bf5f', '9184d5d1-60ec-4c4a-853b-472968487163', 'algorithm', 'HS256');
INSERT INTO auth.component_config VALUES ('18059f52-2758-4bcc-9a1d-2ae6d97611b8', 'd31563e9-8834-4524-9591-04d42889a21e', 'kid', '25409fa7-928b-4311-a4ae-a6db53933ec9');
INSERT INTO auth.component_config VALUES ('d005bb80-063e-464e-9e2d-a195333789be', 'd31563e9-8834-4524-9591-04d42889a21e', 'secret', 'hE3SPlQf-iyW_qVVRrXLIQ');
INSERT INTO auth.component_config VALUES ('5fd285cc-3b37-4fc1-95cb-40946034b16d', 'd31563e9-8834-4524-9591-04d42889a21e', 'priority', '100');
INSERT INTO auth.component_config VALUES ('f5d4d143-1fbe-47e3-9e88-4cc57b155da8', 'd4bab77b-981e-4028-9936-536a1f6e85e4', 'priority', '100');
INSERT INTO auth.component_config VALUES ('e8c1ac37-6b9b-4ea8-b29f-99197ebd95d7', 'd4bab77b-981e-4028-9936-536a1f6e85e4', 'certificate', 'MIICsTCCAZkCBgFxX3tOZTANBgkqhkiG9w0BAQsFADAcMRowGAYDVQQDDBFjb3ZpZC0xOS1yZXNwb25zZTAeFw0yMDA0MDkxNTA1NDhaFw0zMDA0MDkxNTA3MjhaMBwxGjAYBgNVBAMMEWNvdmlkLTE5LXJlc3BvbnNlMIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAumpHGdrYW9H/soonPW29Mm793IgB0PvIkRLWmyPVdfzAxqleQ0jXEzCDyVGPRkfmhekom5dGj1oF1HvR5Ia8sQOS/YtevnoMJcW2zg3pgFPYXSqn7q0zvjNqHY+FjYrru13UTCYRynvhlI+hQH8je0QhRG5ysvp08ALi7oX/V02UGF+9W72MdX3Iep/OgJSWkyBvQCLUeJ2GBbkBhYJjd6E/oCxiU4HpaltvW4OmMM65bNzk7vHieXsVXI15EtzSoiv/dv7al0Wdw9GwCdWMl13C4qsn3QGOYytUKpY/gnFMg3PwPgfuQzzf6TsmaPO4vdtRVjvQvzVDBgr3J4sVIQIDAQABMA0GCSqGSIb3DQEBCwUAA4IBAQCWWiC2LOSdGT6RR+TgfWr1nJbZzFhtupVf4zjfAY3sxEgER3FlzrUyqM1QKjgypzI4IoHxUNzTVOduQ+mwLSdz+qbMlMFwaZGXZoLi94F2lSSKcEKOKMnZU6pNk2Z872Mk6XWe5yl1ttZP9QcclBp94yqz8UTFl/lbF7DZWnd5CWK4P+CF6XZdTaWLKJ3j4bbO7dXgDjnS+BcyVsr9B/xMR3JuCTALX+mp/2KNvwuAlfIR3FwxWHN7VXUfPxE09UAmgpEaoBGIxir6beKzQRx99nM4xzsNmwWg7JX/2yeiSyKbBavqCoO1Qdno0M9yYX9H5sU/BDNf8lxmd+pvP8A3');
INSERT INTO auth.component_config VALUES ('0b9734d6-9cf1-46bc-8cc9-8c185459f307', 'd4bab77b-981e-4028-9936-536a1f6e85e4', 'privateKey', 'MIIEowIBAAKCAQEAumpHGdrYW9H/soonPW29Mm793IgB0PvIkRLWmyPVdfzAxqleQ0jXEzCDyVGPRkfmhekom5dGj1oF1HvR5Ia8sQOS/YtevnoMJcW2zg3pgFPYXSqn7q0zvjNqHY+FjYrru13UTCYRynvhlI+hQH8je0QhRG5ysvp08ALi7oX/V02UGF+9W72MdX3Iep/OgJSWkyBvQCLUeJ2GBbkBhYJjd6E/oCxiU4HpaltvW4OmMM65bNzk7vHieXsVXI15EtzSoiv/dv7al0Wdw9GwCdWMl13C4qsn3QGOYytUKpY/gnFMg3PwPgfuQzzf6TsmaPO4vdtRVjvQvzVDBgr3J4sVIQIDAQABAoIBAHqI95m4L/YTUPfENkz7wJzRNo/q2v7LMqih8G1kHaGD9hfgJvUlgTK89jO0J/CzvIUToTy9MIV0UFa+2VZVdt0Ik2uUXD/qq+d8UlCZ9n4DHzsGmgKa2WNSGpO5zrkesxujbrRrZdBqo/pqez2RFPqm6xM3Yq/hUFKgIxlU/WYdmZj/2T4z6AV86CutrIOdLzA2VFpPhOrTlzWiITPqVfjWV4+y1yXqBj0cVoIvMyA6IkSygaJNGF2rW20ferW9pPS7TOvtcXuHCpPwDvpiLkCFwTWrUXOKyV+D3xfP9lZV+Bjds18lDyy7YEr0n7Q0SxIudTLXTDb4+ceq0mGQ+50CgYEA61SUF/BqTIKtj3+Z7taMn1hiACZ/LscLyU7YsiGVldBOQTnBBk6dtR7kzmvRxZ6917thbbLx4ZdkJ+zonvThUQ3iCd4cIS/H7lm3vGi2ViT2YUlL+eiVVSxI8O735gAteiZh48ssTJxKrXmVa/l3mSn8QM4iV4SLuJ1m6TUr5scCgYEAysnWIJ7MgbHbBbLZ8e+Vsn1JJGFieUVXFXKO84wx0i5KPOvvRgbwyivqs4J6YjeTrjtMO3QIvC0zCQfSZZmOXFvTt1XmGOC12QrTiGAPzMEMOtC8T44N+xO7iZUJPN6tD+TXdBaIFsH2s4rdZnlTZbP84E5PnbDvpYno4xGdnNcCgYAzWTUnNBI9t5XfZdxUvQPaERLxLXU647uypT2rQKC/G8C3gr6XfbfzuVX7pJMVJ9oMpJpRsWYN71avT4ZCiPRRF1JnDMvegeqCG5l3xvFiQpZwC+pEEXfkKdGQoNP+n7NMn+tkJQT5gnDZAVqVQM3T4dFk4pWjuwwxsEawRCXcKQKBgGh2ULe9Fdw5onntCBh5MsQfhe4XvIJ8FcAWo00SixeagOOhJkEMkj2TIfQW4rR1rL20DjESNO262u9r+BYeZBdvtkBbH4zxNLv5t74ovfrb91DU9iyFIO6V26WkAH46tyvY1qQWd0RB5rPWdPa0ySE69qkVri2tV8YOpMlT6b1BAoGBANfxrUjElBC3IxVBcCmXQUZJPsYOipTmUrsQl7Z/pQaA4cp21oWrwxEpYyIBAmoudH6gt+yLHCDl3gb2QGrSXBCFf6pE2PAnlzvhFvfVfPgi6sFjW4vb4NTMHMcVqAuVzu/Is29nXGPD/TSz4izct/XCAijgiDyOewLrIqrdvO8y');
INSERT INTO auth.component_config VALUES ('13386b04-265c-4b04-b72b-72ef0faf024b', '9544d8bc-12e5-4441-ab5a-288a55a9fb27', 'kid', '535e83d8-e0c1-44a7-8c8a-acb2ecf75f61');
INSERT INTO auth.component_config VALUES ('a96885ed-44b7-4e8c-a4e4-50301c80803f', '9544d8bc-12e5-4441-ab5a-288a55a9fb27', 'priority', '100');
INSERT INTO auth.component_config VALUES ('f099a7d7-75d1-4fa7-a177-87e6d9c92c29', '9544d8bc-12e5-4441-ab5a-288a55a9fb27', 'secret', 'eVcv9uBJISRafy_7fhX2qIsXlBEhTgreWgFB8B1-bLu3ovxsm-PTAoz4EHgNNmb4iRAMIMAmfOWxzFtxaR46ew');
INSERT INTO auth.component_config VALUES ('6ec30e26-e09a-4111-b898-e0e127ed9817', '9544d8bc-12e5-4441-ab5a-288a55a9fb27', 'algorithm', 'HS256');
INSERT INTO auth.component_config VALUES ('21b53846-39b5-4f4c-a9dc-de9b2afb783a', '7e51b2f5-d8a1-434b-9943-59640f14f99f', 'allowed-protocol-mapper-types', 'saml-user-property-mapper');
INSERT INTO auth.component_config VALUES ('d6dd38e5-de37-4c16-8e36-1f8548201d33', '7e51b2f5-d8a1-434b-9943-59640f14f99f', 'allowed-protocol-mapper-types', 'oidc-sha256-pairwise-sub-mapper');
INSERT INTO auth.component_config VALUES ('a332f402-fd45-4d70-8331-95781248eac8', '7e51b2f5-d8a1-434b-9943-59640f14f99f', 'allowed-protocol-mapper-types', 'saml-role-list-mapper');
INSERT INTO auth.component_config VALUES ('59604145-603b-4888-8586-32983d25a801', '7e51b2f5-d8a1-434b-9943-59640f14f99f', 'allowed-protocol-mapper-types', 'oidc-address-mapper');
INSERT INTO auth.component_config VALUES ('0ef341d8-ef8c-4562-a805-dec99be094b8', '7e51b2f5-d8a1-434b-9943-59640f14f99f', 'allowed-protocol-mapper-types', 'saml-user-attribute-mapper');
INSERT INTO auth.component_config VALUES ('3ae7a3ab-2bef-4d2f-9ee1-2cca17bf7965', '7e51b2f5-d8a1-434b-9943-59640f14f99f', 'allowed-protocol-mapper-types', 'oidc-full-name-mapper');
INSERT INTO auth.component_config VALUES ('86cd94d4-ca56-4f0b-8ccc-67a39542a2a1', '7e51b2f5-d8a1-434b-9943-59640f14f99f', 'allowed-protocol-mapper-types', 'oidc-usermodel-attribute-mapper');
INSERT INTO auth.component_config VALUES ('44c88d59-2b7a-4274-86ec-b87744c144cf', '7e51b2f5-d8a1-434b-9943-59640f14f99f', 'allowed-protocol-mapper-types', 'oidc-usermodel-property-mapper');
INSERT INTO auth.component_config VALUES ('7714faa7-572e-4ee3-b152-38639f50289d', 'ef548e3d-ea61-4472-bd0a-5c821e0f0506', 'allowed-protocol-mapper-types', 'oidc-full-name-mapper');
INSERT INTO auth.component_config VALUES ('e5612573-1f68-401f-84aa-5cace221ef6f', 'ef548e3d-ea61-4472-bd0a-5c821e0f0506', 'allowed-protocol-mapper-types', 'oidc-address-mapper');
INSERT INTO auth.component_config VALUES ('4944c165-a0a3-47ca-b544-dad9fbd9b6cb', 'ef548e3d-ea61-4472-bd0a-5c821e0f0506', 'allowed-protocol-mapper-types', 'oidc-usermodel-attribute-mapper');
INSERT INTO auth.component_config VALUES ('8edcae4a-2ffa-4fdb-9d76-8ffebc38db5b', 'ef548e3d-ea61-4472-bd0a-5c821e0f0506', 'allowed-protocol-mapper-types', 'saml-user-attribute-mapper');
INSERT INTO auth.component_config VALUES ('05dd05d8-c014-4134-b9d7-1ab432949a6d', 'ef548e3d-ea61-4472-bd0a-5c821e0f0506', 'allowed-protocol-mapper-types', 'saml-user-property-mapper');
INSERT INTO auth.component_config VALUES ('c727c1b8-b4cb-48a6-933c-3ccc7c736180', 'ef548e3d-ea61-4472-bd0a-5c821e0f0506', 'allowed-protocol-mapper-types', 'saml-role-list-mapper');
INSERT INTO auth.component_config VALUES ('92ebf6e4-1d1d-4cdb-ac75-c52611f5c331', 'ef548e3d-ea61-4472-bd0a-5c821e0f0506', 'allowed-protocol-mapper-types', 'oidc-usermodel-property-mapper');
INSERT INTO auth.component_config VALUES ('4c417fa4-9f8a-42d0-a29d-3fa6b3e2e779', 'ef548e3d-ea61-4472-bd0a-5c821e0f0506', 'allowed-protocol-mapper-types', 'oidc-sha256-pairwise-sub-mapper');
INSERT INTO auth.component_config VALUES ('49828696-d051-4d30-b781-760e1c43ad10', 'bb8f3ede-67d1-48ab-892a-036ca2d881ef', 'allow-default-scopes', 'true');
INSERT INTO auth.component_config VALUES ('199add90-5bee-482c-857a-a488fe5401bb', '718ff553-e405-49a1-a03b-0ef64165aa34', 'client-uris-must-match', 'true');
INSERT INTO auth.component_config VALUES ('a603b6d1-f7b5-4fcb-b583-2d7e7a4a38c9', '718ff553-e405-49a1-a03b-0ef64165aa34', 'host-sending-registration-request-must-match', 'true');
INSERT INTO auth.component_config VALUES ('63febb1f-b76c-4dc0-8da1-0c66bebdddc9', '067537ac-cde3-4905-8ea7-7ed4e98852cf', 'max-clients', '200');
INSERT INTO auth.component_config VALUES ('44f62c83-ecb9-436c-ae0d-8135221273ab', '7bc697d2-5691-4d3d-b840-5c5fd41a2524', 'allow-default-scopes', 'true');


--
-- Data for Name: composite_role; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--

INSERT INTO auth.composite_role VALUES ('348a55d0-14a9-428d-8b9c-3ea6cc73495f', 'a09b4532-2c7b-42e0-a0b4-d63693e1fed1');
INSERT INTO auth.composite_role VALUES ('348a55d0-14a9-428d-8b9c-3ea6cc73495f', '8e1f5874-d11d-4195-88b0-2ce3ee3ed6ba');
INSERT INTO auth.composite_role VALUES ('348a55d0-14a9-428d-8b9c-3ea6cc73495f', '459d7386-d5c0-410c-bac9-e34e71008c59');
INSERT INTO auth.composite_role VALUES ('348a55d0-14a9-428d-8b9c-3ea6cc73495f', '4d881722-396f-4aac-9ced-d1b141e65486');
INSERT INTO auth.composite_role VALUES ('348a55d0-14a9-428d-8b9c-3ea6cc73495f', '40fdf4e8-9882-4310-9743-a2e2168ad70c');
INSERT INTO auth.composite_role VALUES ('348a55d0-14a9-428d-8b9c-3ea6cc73495f', '3931e94d-9398-4be8-b7ee-b1f4b78f544a');
INSERT INTO auth.composite_role VALUES ('348a55d0-14a9-428d-8b9c-3ea6cc73495f', '13b7f8c6-e121-4bd1-a02d-1cd72b741ad5');
INSERT INTO auth.composite_role VALUES ('348a55d0-14a9-428d-8b9c-3ea6cc73495f', '5f71ace1-babd-488e-ad39-887451d36263');
INSERT INTO auth.composite_role VALUES ('348a55d0-14a9-428d-8b9c-3ea6cc73495f', 'bca41d85-48b9-4206-a79b-752d372a65e6');
INSERT INTO auth.composite_role VALUES ('348a55d0-14a9-428d-8b9c-3ea6cc73495f', 'b1f4dc89-50f5-4bd3-b01f-a682b2dfcfa6');
INSERT INTO auth.composite_role VALUES ('348a55d0-14a9-428d-8b9c-3ea6cc73495f', '73c8af2d-1b65-4823-8f52-fe3939637010');
INSERT INTO auth.composite_role VALUES ('348a55d0-14a9-428d-8b9c-3ea6cc73495f', 'e58187c1-7759-4f0f-a947-b4a7db393efa');
INSERT INTO auth.composite_role VALUES ('348a55d0-14a9-428d-8b9c-3ea6cc73495f', 'efba2c27-2c1f-48c2-8c0b-413d00e43003');
INSERT INTO auth.composite_role VALUES ('348a55d0-14a9-428d-8b9c-3ea6cc73495f', '311e52e2-39b0-4580-8b59-18121fa04441');
INSERT INTO auth.composite_role VALUES ('348a55d0-14a9-428d-8b9c-3ea6cc73495f', '1b2aa2d3-9056-4855-984d-21487dd3ad22');
INSERT INTO auth.composite_role VALUES ('348a55d0-14a9-428d-8b9c-3ea6cc73495f', 'a6fa0a1f-6795-4359-ab3e-55f369682153');
INSERT INTO auth.composite_role VALUES ('348a55d0-14a9-428d-8b9c-3ea6cc73495f', '8a8c6b44-f202-444f-ae15-7672889d067b');
INSERT INTO auth.composite_role VALUES ('348a55d0-14a9-428d-8b9c-3ea6cc73495f', '96320915-ec41-486f-bb4f-a159b9b518cc');
INSERT INTO auth.composite_role VALUES ('4d881722-396f-4aac-9ced-d1b141e65486', '1b2aa2d3-9056-4855-984d-21487dd3ad22');
INSERT INTO auth.composite_role VALUES ('4d881722-396f-4aac-9ced-d1b141e65486', '96320915-ec41-486f-bb4f-a159b9b518cc');
INSERT INTO auth.composite_role VALUES ('40fdf4e8-9882-4310-9743-a2e2168ad70c', 'a6fa0a1f-6795-4359-ab3e-55f369682153');
INSERT INTO auth.composite_role VALUES ('b673e7a0-bd70-4543-b840-c1c6cb193bee', 'f5dc963c-735f-44b9-8fb4-2030993b693c');
INSERT INTO auth.composite_role VALUES ('348a55d0-14a9-428d-8b9c-3ea6cc73495f', '0189e5f4-8c50-48fb-b663-1303eeac0493');
INSERT INTO auth.composite_role VALUES ('348a55d0-14a9-428d-8b9c-3ea6cc73495f', '1e272c1f-ed09-494e-8a4e-18ae33466b5c');
INSERT INTO auth.composite_role VALUES ('348a55d0-14a9-428d-8b9c-3ea6cc73495f', '952e122d-a8d2-4979-9ddc-eb159fb4ab72');
INSERT INTO auth.composite_role VALUES ('348a55d0-14a9-428d-8b9c-3ea6cc73495f', '291d9982-ce08-44ff-8b86-330984e7c786');
INSERT INTO auth.composite_role VALUES ('348a55d0-14a9-428d-8b9c-3ea6cc73495f', '725f9e5b-3896-4a43-aa19-438862528549');
INSERT INTO auth.composite_role VALUES ('348a55d0-14a9-428d-8b9c-3ea6cc73495f', 'ec8ddff1-390e-4c51-a5b8-4d65043263c0');
INSERT INTO auth.composite_role VALUES ('348a55d0-14a9-428d-8b9c-3ea6cc73495f', '8379a962-0964-4eb8-a55c-afea13e45cb4');
INSERT INTO auth.composite_role VALUES ('348a55d0-14a9-428d-8b9c-3ea6cc73495f', '85f4ea7b-c18f-4451-9efb-350b73bbcc5b');
INSERT INTO auth.composite_role VALUES ('348a55d0-14a9-428d-8b9c-3ea6cc73495f', '5d1facc3-d305-49d0-82af-8286e9a1d03e');
INSERT INTO auth.composite_role VALUES ('348a55d0-14a9-428d-8b9c-3ea6cc73495f', 'ea38b558-928a-4e6f-9383-18bf06d113dc');
INSERT INTO auth.composite_role VALUES ('348a55d0-14a9-428d-8b9c-3ea6cc73495f', '8ded8850-7b73-48ec-8faf-89042a9a81c0');
INSERT INTO auth.composite_role VALUES ('348a55d0-14a9-428d-8b9c-3ea6cc73495f', '1a22dbe4-b81e-4cae-b038-c2d9638c4b7d');
INSERT INTO auth.composite_role VALUES ('348a55d0-14a9-428d-8b9c-3ea6cc73495f', '09f22d46-655e-4fe1-a9e5-ffde9015160a');
INSERT INTO auth.composite_role VALUES ('348a55d0-14a9-428d-8b9c-3ea6cc73495f', 'c62fff6d-a217-48ff-a132-5ea4237dcc6a');
INSERT INTO auth.composite_role VALUES ('348a55d0-14a9-428d-8b9c-3ea6cc73495f', '6c752745-e71a-465e-9729-6c4feb35d91e');
INSERT INTO auth.composite_role VALUES ('348a55d0-14a9-428d-8b9c-3ea6cc73495f', 'a68a5254-39c1-4b95-8df3-1b18de4bbd08');
INSERT INTO auth.composite_role VALUES ('348a55d0-14a9-428d-8b9c-3ea6cc73495f', '5765c0ef-cc9a-4993-ad16-59aa1a4870c7');
INSERT INTO auth.composite_role VALUES ('348a55d0-14a9-428d-8b9c-3ea6cc73495f', '57cd65f9-e1c9-402e-ad6a-572a782033c8');
INSERT INTO auth.composite_role VALUES ('291d9982-ce08-44ff-8b86-330984e7c786', '57cd65f9-e1c9-402e-ad6a-572a782033c8');
INSERT INTO auth.composite_role VALUES ('291d9982-ce08-44ff-8b86-330984e7c786', '6c752745-e71a-465e-9729-6c4feb35d91e');
INSERT INTO auth.composite_role VALUES ('725f9e5b-3896-4a43-aa19-438862528549', 'a68a5254-39c1-4b95-8df3-1b18de4bbd08');
INSERT INTO auth.composite_role VALUES ('40830bf7-5ad4-4a78-84c7-e2e8bc464286', 'e5ec89d0-a653-4147-b43c-490769626dde');
INSERT INTO auth.composite_role VALUES ('40830bf7-5ad4-4a78-84c7-e2e8bc464286', '90d99e25-83f6-4b4c-9d1e-25fa89701194');
INSERT INTO auth.composite_role VALUES ('40830bf7-5ad4-4a78-84c7-e2e8bc464286', '8e158abe-5758-4358-8c7d-4b20536bb6a7');
INSERT INTO auth.composite_role VALUES ('40830bf7-5ad4-4a78-84c7-e2e8bc464286', '2c094da4-1417-4cd6-ad9a-8ed51567da35');
INSERT INTO auth.composite_role VALUES ('40830bf7-5ad4-4a78-84c7-e2e8bc464286', 'bc3140d9-a709-44bc-9f92-a3cd4a7d3a9f');
INSERT INTO auth.composite_role VALUES ('40830bf7-5ad4-4a78-84c7-e2e8bc464286', 'c248d505-9717-44ce-a750-b27faedbf59a');
INSERT INTO auth.composite_role VALUES ('40830bf7-5ad4-4a78-84c7-e2e8bc464286', 'bc2acc7e-fe0b-4265-8c4d-5e64179ff7ad');
INSERT INTO auth.composite_role VALUES ('40830bf7-5ad4-4a78-84c7-e2e8bc464286', '698e008d-daa4-4af4-b5e9-715d25625bff');
INSERT INTO auth.composite_role VALUES ('40830bf7-5ad4-4a78-84c7-e2e8bc464286', 'e7ef0adb-7c2a-4602-95cf-e058adbdff94');
INSERT INTO auth.composite_role VALUES ('40830bf7-5ad4-4a78-84c7-e2e8bc464286', 'c341a789-0fc2-4a1a-9195-92a4a5da9bb6');
INSERT INTO auth.composite_role VALUES ('40830bf7-5ad4-4a78-84c7-e2e8bc464286', 'e858fccb-b989-4b31-bf51-24a48488ada3');
INSERT INTO auth.composite_role VALUES ('40830bf7-5ad4-4a78-84c7-e2e8bc464286', '4d6bb3c6-a97f-4f9f-a137-fe632feb9377');
INSERT INTO auth.composite_role VALUES ('40830bf7-5ad4-4a78-84c7-e2e8bc464286', '105e31f4-4dcb-4fc9-b0e5-3550da4cf53f');
INSERT INTO auth.composite_role VALUES ('40830bf7-5ad4-4a78-84c7-e2e8bc464286', 'fd7c904c-ddfb-4545-92f9-380f6964b861');
INSERT INTO auth.composite_role VALUES ('40830bf7-5ad4-4a78-84c7-e2e8bc464286', 'b76d64e1-be19-4ce9-b388-dc792a13df00');
INSERT INTO auth.composite_role VALUES ('40830bf7-5ad4-4a78-84c7-e2e8bc464286', '490b15ee-5a66-4a34-ab0f-6c04468e6c05');
INSERT INTO auth.composite_role VALUES ('40830bf7-5ad4-4a78-84c7-e2e8bc464286', 'c141a289-d0ab-4835-af72-372b84793d2d');
INSERT INTO auth.composite_role VALUES ('8e158abe-5758-4358-8c7d-4b20536bb6a7', 'fd7c904c-ddfb-4545-92f9-380f6964b861');
INSERT INTO auth.composite_role VALUES ('8e158abe-5758-4358-8c7d-4b20536bb6a7', 'c141a289-d0ab-4835-af72-372b84793d2d');
INSERT INTO auth.composite_role VALUES ('2c094da4-1417-4cd6-ad9a-8ed51567da35', 'b76d64e1-be19-4ce9-b388-dc792a13df00');
INSERT INTO auth.composite_role VALUES ('348a55d0-14a9-428d-8b9c-3ea6cc73495f', 'f828cd1e-a0bb-48d0-9e9f-220e4a7e7479');
INSERT INTO auth.composite_role VALUES ('3faf057f-3d35-4ddb-b32f-f3e8d066e824', '73d40e2e-22d0-443e-b33b-6fd2dda5fb00');
INSERT INTO auth.composite_role VALUES ('40830bf7-5ad4-4a78-84c7-e2e8bc464286', '3eb1fa86-9976-49c0-bff0-76b9f51e4849');


--
-- Data for Name: user_entity; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--

INSERT INTO auth.user_entity VALUES ('5ce49b12-86bc-4eb1-88c5-40b0aeea9106', NULL, 'ed689160-6961-49da-953d-d4bbe3243ee1', false, true, NULL, NULL, NULL, 'master', 'admin', 1586442594594, NULL, 0);
INSERT INTO auth.user_entity VALUES ('61ce387d-99e3-4336-88f3-3eb695379216', 'test.user@covid19response.online', 'test.user@covid19response.online', false, true, NULL, 'test', 'user', 'covid-19-response', 'test.user', 1586445272355, NULL, 0);
INSERT INTO auth.user_entity VALUES ('2b571d00-7cd2-432b-a22a-fe1f4f8c9904', NULL, '24dcfdf8-fe51-4e37-889c-6985f8730eb9', false, true, NULL, NULL, NULL, 'covid-19-response', 'application-temperature-tracker', 1589142844702, NULL, 0);


--
-- Data for Name: credential; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--

INSERT INTO auth.credential VALUES ('ec6a925c-79d6-46e8-a378-fafa641f4e50', NULL, 27500, '\xbf0320fc15629ac5d1fa6f522cd10e82', 'password', '7LQNpZiS23GLc+qQdw1zqq+a/rz1Elh5wA368GQwFdwzF5kUonsFDaaqClM/MJE0h4AtM/g0FX+QiGZhmgjTEg==', '5ce49b12-86bc-4eb1-88c5-40b0aeea9106', NULL, 0, 0, 0, 'pbkdf2-sha256');
INSERT INTO auth.credential VALUES ('2de88b59-8da4-47da-935a-d8c6e45ce0d4', NULL, 27500, '\x30b6a2a182816925e638c5fce9360d16', 'password', 'g89aTSpzCxd1z2jmiKXGrUzLrw1ioy26EMpoEXvP/PxuToIrMl+fjQQWKq7R97NhFVo17oVtiyArBMCA2FVaUw==', '61ce387d-99e3-4336-88f3-3eb695379216', 1586457646457, 0, 0, 0, 'pbkdf2-sha256');
INSERT INTO auth.credential VALUES ('649855c7-6da0-4fe8-b9a6-ed61b7ed00c2', NULL, 27500, '\xf28820db530c3266507ba2d8507f91a2', 'password', '83BW8hd3Dk8nB1fysu3NX8KQDEze882EC9YHuPRIUHCzusCU1FemKl86v97Vwc9cmk971egDIg9uE98OWRUvpQ==', '2b571d00-7cd2-432b-a22a-fe1f4f8c9904', 1589142925020, 0, 0, 0, 'pbkdf2-sha256');


--
-- Data for Name: credential_attribute; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: databasechangelog; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--

INSERT INTO auth.databasechangelog VALUES ('1.0.0.Final-KEYCLOAK-5461', 'sthorger@redhat.com', 'META-INF/jpa-changelog-1.0.0.Final.xml', '2020-04-09 14:29:35.816771', 1, 'EXECUTED', '7:4e70412f24a3f382c82183742ec79317', 'createTable tableName=APPLICATION_DEFAULT_ROLES; createTable tableName=CLIENT; createTable tableName=CLIENT_SESSION; createTable tableName=CLIENT_SESSION_ROLE; createTable tableName=COMPOSITE_ROLE; createTable tableName=CREDENTIAL; createTable tab...', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('1.0.0.Final-KEYCLOAK-5461', 'sthorger@redhat.com', 'META-INF/db2-jpa-changelog-1.0.0.Final.xml', '2020-04-09 14:29:35.863177', 2, 'MARK_RAN', '7:cb16724583e9675711801c6875114f28', 'createTable tableName=APPLICATION_DEFAULT_ROLES; createTable tableName=CLIENT; createTable tableName=CLIENT_SESSION; createTable tableName=CLIENT_SESSION_ROLE; createTable tableName=COMPOSITE_ROLE; createTable tableName=CREDENTIAL; createTable tab...', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('1.1.0.Beta1', 'sthorger@redhat.com', 'META-INF/jpa-changelog-1.1.0.Beta1.xml', '2020-04-09 14:29:36.120476', 3, 'EXECUTED', '7:0310eb8ba07cec616460794d42ade0fa', 'delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION; createTable tableName=CLIENT_ATTRIBUTES; createTable tableName=CLIENT_SESSION_NOTE; createTable tableName=APP_NODE_REGISTRATIONS; addColumn table...', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('1.1.0.Final', 'sthorger@redhat.com', 'META-INF/jpa-changelog-1.1.0.Final.xml', '2020-04-09 14:29:36.132569', 4, 'EXECUTED', '7:5d25857e708c3233ef4439df1f93f012', 'renameColumn newColumnName=EVENT_TIME, oldColumnName=TIME, tableName=EVENT_ENTITY', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('1.2.0.Beta1', 'psilva@redhat.com', 'META-INF/jpa-changelog-1.2.0.Beta1.xml', '2020-04-09 14:29:36.768732', 5, 'EXECUTED', '7:c7a54a1041d58eb3817a4a883b4d4e84', 'delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION; createTable tableName=PROTOCOL_MAPPER; createTable tableName=PROTOCOL_MAPPER_CONFIG; createTable tableName=...', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('1.2.0.Beta1', 'psilva@redhat.com', 'META-INF/db2-jpa-changelog-1.2.0.Beta1.xml', '2020-04-09 14:29:36.78405', 6, 'MARK_RAN', '7:2e01012df20974c1c2a605ef8afe25b7', 'delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION; createTable tableName=PROTOCOL_MAPPER; createTable tableName=PROTOCOL_MAPPER_CONFIG; createTable tableName=...', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('1.2.0.RC1', 'bburke@redhat.com', 'META-INF/jpa-changelog-1.2.0.CR1.xml', '2020-04-09 14:29:37.285673', 7, 'EXECUTED', '7:0f08df48468428e0f30ee59a8ec01a41', 'delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete tableName=USER_SESSION; createTable tableName=MIGRATION_MODEL; createTable tableName=IDENTITY_P...', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('1.2.0.RC1', 'bburke@redhat.com', 'META-INF/db2-jpa-changelog-1.2.0.CR1.xml', '2020-04-09 14:29:37.300402', 8, 'MARK_RAN', '7:a77ea2ad226b345e7d689d366f185c8c', 'delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete tableName=USER_SESSION; createTable tableName=MIGRATION_MODEL; createTable tableName=IDENTITY_P...', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('1.2.0.Final', 'keycloak', 'META-INF/jpa-changelog-1.2.0.Final.xml', '2020-04-09 14:29:37.328607', 9, 'EXECUTED', '7:a3377a2059aefbf3b90ebb4c4cc8e2ab', 'update tableName=CLIENT; update tableName=CLIENT; update tableName=CLIENT', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('1.3.0', 'bburke@redhat.com', 'META-INF/jpa-changelog-1.3.0.xml', '2020-04-09 14:29:38.115652', 10, 'EXECUTED', '7:04c1dbedc2aa3e9756d1a1668e003451', 'delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete tableName=USER_SESSION; createTable tableName=ADMI...', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('1.4.0', 'bburke@redhat.com', 'META-INF/jpa-changelog-1.4.0.xml', '2020-04-09 14:29:38.541904', 11, 'EXECUTED', '7:36ef39ed560ad07062d956db861042ba', 'delete tableName=CLIENT_SESSION_AUTH_STATUS; delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete table...', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('1.4.0', 'bburke@redhat.com', 'META-INF/db2-jpa-changelog-1.4.0.xml', '2020-04-09 14:29:38.548602', 12, 'MARK_RAN', '7:d909180b2530479a716d3f9c9eaea3d7', 'delete tableName=CLIENT_SESSION_AUTH_STATUS; delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete table...', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('1.5.0', 'bburke@redhat.com', 'META-INF/jpa-changelog-1.5.0.xml', '2020-04-09 14:29:39.028285', 13, 'EXECUTED', '7:cf12b04b79bea5152f165eb41f3955f6', 'delete tableName=CLIENT_SESSION_AUTH_STATUS; delete tableName=CLIENT_SESSION_ROLE; delete tableName=CLIENT_SESSION_PROT_MAPPER; delete tableName=CLIENT_SESSION_NOTE; delete tableName=CLIENT_SESSION; delete tableName=USER_SESSION_NOTE; delete table...', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('1.6.1_from15', 'mposolda@redhat.com', 'META-INF/jpa-changelog-1.6.1.xml', '2020-04-09 14:29:39.190313', 14, 'EXECUTED', '7:7e32c8f05c755e8675764e7d5f514509', 'addColumn tableName=REALM; addColumn tableName=KEYCLOAK_ROLE; addColumn tableName=CLIENT; createTable tableName=OFFLINE_USER_SESSION; createTable tableName=OFFLINE_CLIENT_SESSION; addPrimaryKey constraintName=CONSTRAINT_OFFL_US_SES_PK2, tableName=...', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('1.6.1_from16-pre', 'mposolda@redhat.com', 'META-INF/jpa-changelog-1.6.1.xml', '2020-04-09 14:29:39.193648', 15, 'MARK_RAN', '7:980ba23cc0ec39cab731ce903dd01291', 'delete tableName=OFFLINE_CLIENT_SESSION; delete tableName=OFFLINE_USER_SESSION', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('1.6.1_from16', 'mposolda@redhat.com', 'META-INF/jpa-changelog-1.6.1.xml', '2020-04-09 14:29:39.196756', 16, 'MARK_RAN', '7:2fa220758991285312eb84f3b4ff5336', 'dropPrimaryKey constraintName=CONSTRAINT_OFFLINE_US_SES_PK, tableName=OFFLINE_USER_SESSION; dropPrimaryKey constraintName=CONSTRAINT_OFFLINE_CL_SES_PK, tableName=OFFLINE_CLIENT_SESSION; addColumn tableName=OFFLINE_USER_SESSION; update tableName=OF...', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('1.6.1', 'mposolda@redhat.com', 'META-INF/jpa-changelog-1.6.1.xml', '2020-04-09 14:29:39.199049', 17, 'EXECUTED', '7:d41d8cd98f00b204e9800998ecf8427e', 'empty', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('1.7.0', 'bburke@redhat.com', 'META-INF/jpa-changelog-1.7.0.xml', '2020-04-09 14:29:39.549945', 18, 'EXECUTED', '7:91ace540896df890cc00a0490ee52bbc', 'createTable tableName=KEYCLOAK_GROUP; createTable tableName=GROUP_ROLE_MAPPING; createTable tableName=GROUP_ATTRIBUTE; createTable tableName=USER_GROUP_MEMBERSHIP; createTable tableName=REALM_DEFAULT_GROUPS; addColumn tableName=IDENTITY_PROVIDER; ...', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('1.8.0', 'mposolda@redhat.com', 'META-INF/jpa-changelog-1.8.0.xml', '2020-04-09 14:29:39.839283', 19, 'EXECUTED', '7:c31d1646dfa2618a9335c00e07f89f24', 'addColumn tableName=IDENTITY_PROVIDER; createTable tableName=CLIENT_TEMPLATE; createTable tableName=CLIENT_TEMPLATE_ATTRIBUTES; createTable tableName=TEMPLATE_SCOPE_MAPPING; dropNotNullConstraint columnName=CLIENT_ID, tableName=PROTOCOL_MAPPER; ad...', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('1.8.0-2', 'keycloak', 'META-INF/jpa-changelog-1.8.0.xml', '2020-04-09 14:29:39.851123', 20, 'EXECUTED', '7:df8bc21027a4f7cbbb01f6344e89ce07', 'dropDefaultValue columnName=ALGORITHM, tableName=CREDENTIAL; update tableName=CREDENTIAL', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('authz-3.4.0.CR1-resource-server-pk-change-part1', 'glavoie@gmail.com', 'META-INF/jpa-changelog-authz-3.4.0.CR1.xml', '2020-04-09 14:29:41.394952', 45, 'EXECUTED', '7:6a48ce645a3525488a90fbf76adf3bb3', 'addColumn tableName=RESOURCE_SERVER_POLICY; addColumn tableName=RESOURCE_SERVER_RESOURCE; addColumn tableName=RESOURCE_SERVER_SCOPE', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('1.8.0', 'mposolda@redhat.com', 'META-INF/db2-jpa-changelog-1.8.0.xml', '2020-04-09 14:29:39.854016', 21, 'MARK_RAN', '7:f987971fe6b37d963bc95fee2b27f8df', 'addColumn tableName=IDENTITY_PROVIDER; createTable tableName=CLIENT_TEMPLATE; createTable tableName=CLIENT_TEMPLATE_ATTRIBUTES; createTable tableName=TEMPLATE_SCOPE_MAPPING; dropNotNullConstraint columnName=CLIENT_ID, tableName=PROTOCOL_MAPPER; ad...', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('1.8.0-2', 'keycloak', 'META-INF/db2-jpa-changelog-1.8.0.xml', '2020-04-09 14:29:39.859496', 22, 'MARK_RAN', '7:df8bc21027a4f7cbbb01f6344e89ce07', 'dropDefaultValue columnName=ALGORITHM, tableName=CREDENTIAL; update tableName=CREDENTIAL', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('1.9.0', 'mposolda@redhat.com', 'META-INF/jpa-changelog-1.9.0.xml', '2020-04-09 14:29:39.924112', 23, 'EXECUTED', '7:ed2dc7f799d19ac452cbcda56c929e47', 'update tableName=REALM; update tableName=REALM; update tableName=REALM; update tableName=REALM; update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=CREDENTIAL; update tableName=REALM; update tableName=REALM; customChange; dr...', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('1.9.1', 'keycloak', 'META-INF/jpa-changelog-1.9.1.xml', '2020-04-09 14:29:39.929247', 24, 'EXECUTED', '7:80b5db88a5dda36ece5f235be8757615', 'modifyDataType columnName=PRIVATE_KEY, tableName=REALM; modifyDataType columnName=PUBLIC_KEY, tableName=REALM; modifyDataType columnName=CERTIFICATE, tableName=REALM', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('1.9.1', 'keycloak', 'META-INF/db2-jpa-changelog-1.9.1.xml', '2020-04-09 14:29:39.931073', 25, 'MARK_RAN', '7:1437310ed1305a9b93f8848f301726ce', 'modifyDataType columnName=PRIVATE_KEY, tableName=REALM; modifyDataType columnName=CERTIFICATE, tableName=REALM', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('1.9.2', 'keycloak', 'META-INF/jpa-changelog-1.9.2.xml', '2020-04-09 14:29:40.024818', 26, 'EXECUTED', '7:b82ffb34850fa0836be16deefc6a87c4', 'createIndex indexName=IDX_USER_EMAIL, tableName=USER_ENTITY; createIndex indexName=IDX_USER_ROLE_MAPPING, tableName=USER_ROLE_MAPPING; createIndex indexName=IDX_USER_GROUP_MAPPING, tableName=USER_GROUP_MEMBERSHIP; createIndex indexName=IDX_USER_CO...', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('authz-2.0.0', 'psilva@redhat.com', 'META-INF/jpa-changelog-authz-2.0.0.xml', '2020-04-09 14:29:40.291492', 27, 'EXECUTED', '7:9cc98082921330d8d9266decdd4bd658', 'createTable tableName=RESOURCE_SERVER; addPrimaryKey constraintName=CONSTRAINT_FARS, tableName=RESOURCE_SERVER; addUniqueConstraint constraintName=UK_AU8TT6T700S9V50BU18WS5HA6, tableName=RESOURCE_SERVER; createTable tableName=RESOURCE_SERVER_RESOU...', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('authz-2.5.1', 'psilva@redhat.com', 'META-INF/jpa-changelog-authz-2.5.1.xml', '2020-04-09 14:29:40.307984', 28, 'EXECUTED', '7:03d64aeed9cb52b969bd30a7ac0db57e', 'update tableName=RESOURCE_SERVER_POLICY', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('2.1.0-KEYCLOAK-5461', 'bburke@redhat.com', 'META-INF/jpa-changelog-2.1.0.xml', '2020-04-09 14:29:40.49127', 29, 'EXECUTED', '7:f1f9fd8710399d725b780f463c6b21cd', 'createTable tableName=BROKER_LINK; createTable tableName=FED_USER_ATTRIBUTE; createTable tableName=FED_USER_CONSENT; createTable tableName=FED_USER_CONSENT_ROLE; createTable tableName=FED_USER_CONSENT_PROT_MAPPER; createTable tableName=FED_USER_CR...', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('2.2.0', 'bburke@redhat.com', 'META-INF/jpa-changelog-2.2.0.xml', '2020-04-09 14:29:40.534276', 30, 'EXECUTED', '7:53188c3eb1107546e6f765835705b6c1', 'addColumn tableName=ADMIN_EVENT_ENTITY; createTable tableName=CREDENTIAL_ATTRIBUTE; createTable tableName=FED_CREDENTIAL_ATTRIBUTE; modifyDataType columnName=VALUE, tableName=CREDENTIAL; addForeignKeyConstraint baseTableName=FED_CREDENTIAL_ATTRIBU...', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('2.3.0', 'bburke@redhat.com', 'META-INF/jpa-changelog-2.3.0.xml', '2020-04-09 14:29:40.575259', 31, 'EXECUTED', '7:d6e6f3bc57a0c5586737d1351725d4d4', 'createTable tableName=FEDERATED_USER; addPrimaryKey constraintName=CONSTR_FEDERATED_USER, tableName=FEDERATED_USER; dropDefaultValue columnName=TOTP, tableName=USER_ENTITY; dropColumn columnName=TOTP, tableName=USER_ENTITY; addColumn tableName=IDE...', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('2.4.0', 'bburke@redhat.com', 'META-INF/jpa-changelog-2.4.0.xml', '2020-04-09 14:29:40.589778', 32, 'EXECUTED', '7:454d604fbd755d9df3fd9c6329043aa5', 'customChange', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('2.5.0', 'bburke@redhat.com', 'META-INF/jpa-changelog-2.5.0.xml', '2020-04-09 14:29:40.605157', 33, 'EXECUTED', '7:57e98a3077e29caf562f7dbf80c72600', 'customChange; modifyDataType columnName=USER_ID, tableName=OFFLINE_USER_SESSION', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('2.5.0-unicode-oracle', 'hmlnarik@redhat.com', 'META-INF/jpa-changelog-2.5.0.xml', '2020-04-09 14:29:40.607274', 34, 'MARK_RAN', '7:e4c7e8f2256210aee71ddc42f538b57a', 'modifyDataType columnName=DESCRIPTION, tableName=AUTHENTICATION_FLOW; modifyDataType columnName=DESCRIPTION, tableName=CLIENT_TEMPLATE; modifyDataType columnName=DESCRIPTION, tableName=RESOURCE_SERVER_POLICY; modifyDataType columnName=DESCRIPTION,...', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('2.5.0-unicode-other-dbs', 'hmlnarik@redhat.com', 'META-INF/jpa-changelog-2.5.0.xml', '2020-04-09 14:29:40.67728', 35, 'EXECUTED', '7:09a43c97e49bc626460480aa1379b522', 'modifyDataType columnName=DESCRIPTION, tableName=AUTHENTICATION_FLOW; modifyDataType columnName=DESCRIPTION, tableName=CLIENT_TEMPLATE; modifyDataType columnName=DESCRIPTION, tableName=RESOURCE_SERVER_POLICY; modifyDataType columnName=DESCRIPTION,...', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('2.5.0-duplicate-email-support', 'slawomir@dabek.name', 'META-INF/jpa-changelog-2.5.0.xml', '2020-04-09 14:29:40.804786', 36, 'EXECUTED', '7:26bfc7c74fefa9126f2ce702fb775553', 'addColumn tableName=REALM', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('2.5.0-unique-group-names', 'hmlnarik@redhat.com', 'META-INF/jpa-changelog-2.5.0.xml', '2020-04-09 14:29:40.817237', 37, 'EXECUTED', '7:a161e2ae671a9020fff61e996a207377', 'addUniqueConstraint constraintName=SIBLING_NAMES, tableName=KEYCLOAK_GROUP', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('2.5.1', 'bburke@redhat.com', 'META-INF/jpa-changelog-2.5.1.xml', '2020-04-09 14:29:40.821211', 38, 'EXECUTED', '7:37fc1781855ac5388c494f1442b3f717', 'addColumn tableName=FED_USER_CONSENT', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('3.0.0', 'bburke@redhat.com', 'META-INF/jpa-changelog-3.0.0.xml', '2020-04-09 14:29:40.879492', 39, 'EXECUTED', '7:13a27db0dae6049541136adad7261d27', 'addColumn tableName=IDENTITY_PROVIDER', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('3.2.0-fix', 'keycloak', 'META-INF/jpa-changelog-3.2.0.xml', '2020-04-09 14:29:40.88233', 40, 'MARK_RAN', '7:550300617e3b59e8af3a6294df8248a3', 'addNotNullConstraint columnName=REALM_ID, tableName=CLIENT_INITIAL_ACCESS', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('3.2.0-fix-with-keycloak-5416', 'keycloak', 'META-INF/jpa-changelog-3.2.0.xml', '2020-04-09 14:29:40.885937', 41, 'MARK_RAN', '7:e3a9482b8931481dc2772a5c07c44f17', 'dropIndex indexName=IDX_CLIENT_INIT_ACC_REALM, tableName=CLIENT_INITIAL_ACCESS; addNotNullConstraint columnName=REALM_ID, tableName=CLIENT_INITIAL_ACCESS; createIndex indexName=IDX_CLIENT_INIT_ACC_REALM, tableName=CLIENT_INITIAL_ACCESS', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('3.2.0-fix-offline-sessions', 'hmlnarik', 'META-INF/jpa-changelog-3.2.0.xml', '2020-04-09 14:29:40.904056', 42, 'EXECUTED', '7:72b07d85a2677cb257edb02b408f332d', 'customChange', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('3.2.0-fixed', 'keycloak', 'META-INF/jpa-changelog-3.2.0.xml', '2020-04-09 14:29:41.31365', 43, 'EXECUTED', '7:a72a7858967bd414835d19e04d880312', 'addColumn tableName=REALM; dropPrimaryKey constraintName=CONSTRAINT_OFFL_CL_SES_PK2, tableName=OFFLINE_CLIENT_SESSION; dropColumn columnName=CLIENT_SESSION_ID, tableName=OFFLINE_CLIENT_SESSION; addPrimaryKey constraintName=CONSTRAINT_OFFL_CL_SES_P...', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('3.3.0', 'keycloak', 'META-INF/jpa-changelog-3.3.0.xml', '2020-04-09 14:29:41.390353', 44, 'EXECUTED', '7:94edff7cf9ce179e7e85f0cd78a3cf2c', 'addColumn tableName=USER_ENTITY', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('authz-3.4.0.CR1-resource-server-pk-change-part2-KEYCLOAK-6095', 'hmlnarik@redhat.com', 'META-INF/jpa-changelog-authz-3.4.0.CR1.xml', '2020-04-09 14:29:41.4116', 46, 'EXECUTED', '7:e64b5dcea7db06077c6e57d3b9e5ca14', 'customChange', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('authz-3.4.0.CR1-resource-server-pk-change-part3-fixed', 'glavoie@gmail.com', 'META-INF/jpa-changelog-authz-3.4.0.CR1.xml', '2020-04-09 14:29:41.413545', 47, 'MARK_RAN', '7:fd8cf02498f8b1e72496a20afc75178c', 'dropIndex indexName=IDX_RES_SERV_POL_RES_SERV, tableName=RESOURCE_SERVER_POLICY; dropIndex indexName=IDX_RES_SRV_RES_RES_SRV, tableName=RESOURCE_SERVER_RESOURCE; dropIndex indexName=IDX_RES_SRV_SCOPE_RES_SRV, tableName=RESOURCE_SERVER_SCOPE', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('authz-3.4.0.CR1-resource-server-pk-change-part3-fixed-nodropindex', 'glavoie@gmail.com', 'META-INF/jpa-changelog-authz-3.4.0.CR1.xml', '2020-04-09 14:29:41.548406', 48, 'EXECUTED', '7:542794f25aa2b1fbabb7e577d6646319', 'addNotNullConstraint columnName=RESOURCE_SERVER_CLIENT_ID, tableName=RESOURCE_SERVER_POLICY; addNotNullConstraint columnName=RESOURCE_SERVER_CLIENT_ID, tableName=RESOURCE_SERVER_RESOURCE; addNotNullConstraint columnName=RESOURCE_SERVER_CLIENT_ID, ...', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('authn-3.4.0.CR1-refresh-token-max-reuse', 'glavoie@gmail.com', 'META-INF/jpa-changelog-authz-3.4.0.CR1.xml', '2020-04-09 14:29:41.62521', 49, 'EXECUTED', '7:edad604c882df12f74941dac3cc6d650', 'addColumn tableName=REALM', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('3.4.0', 'keycloak', 'META-INF/jpa-changelog-3.4.0.xml', '2020-04-09 14:29:41.741888', 50, 'EXECUTED', '7:0f88b78b7b46480eb92690cbf5e44900', 'addPrimaryKey constraintName=CONSTRAINT_REALM_DEFAULT_ROLES, tableName=REALM_DEFAULT_ROLES; addPrimaryKey constraintName=CONSTRAINT_COMPOSITE_ROLE, tableName=COMPOSITE_ROLE; addPrimaryKey constraintName=CONSTR_REALM_DEFAULT_GROUPS, tableName=REALM...', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('3.4.0-KEYCLOAK-5230', 'hmlnarik@redhat.com', 'META-INF/jpa-changelog-3.4.0.xml', '2020-04-09 14:29:41.826822', 51, 'EXECUTED', '7:d560e43982611d936457c327f872dd59', 'createIndex indexName=IDX_FU_ATTRIBUTE, tableName=FED_USER_ATTRIBUTE; createIndex indexName=IDX_FU_CONSENT, tableName=FED_USER_CONSENT; createIndex indexName=IDX_FU_CONSENT_RU, tableName=FED_USER_CONSENT; createIndex indexName=IDX_FU_CREDENTIAL, t...', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('3.4.1', 'psilva@redhat.com', 'META-INF/jpa-changelog-3.4.1.xml', '2020-04-09 14:29:41.832092', 52, 'EXECUTED', '7:c155566c42b4d14ef07059ec3b3bbd8e', 'modifyDataType columnName=VALUE, tableName=CLIENT_ATTRIBUTES', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('3.4.2', 'keycloak', 'META-INF/jpa-changelog-3.4.2.xml', '2020-04-09 14:29:41.843025', 53, 'EXECUTED', '7:b40376581f12d70f3c89ba8ddf5b7dea', 'update tableName=REALM', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('3.4.2-KEYCLOAK-5172', 'mkanis@redhat.com', 'META-INF/jpa-changelog-3.4.2.xml', '2020-04-09 14:29:41.849359', 54, 'EXECUTED', '7:a1132cc395f7b95b3646146c2e38f168', 'update tableName=CLIENT', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('4.0.0-KEYCLOAK-6335', 'bburke@redhat.com', 'META-INF/jpa-changelog-4.0.0.xml', '2020-04-09 14:29:41.8609', 55, 'EXECUTED', '7:d8dc5d89c789105cfa7ca0e82cba60af', 'createTable tableName=CLIENT_AUTH_FLOW_BINDINGS; addPrimaryKey constraintName=C_CLI_FLOW_BIND, tableName=CLIENT_AUTH_FLOW_BINDINGS', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('4.0.0-CLEANUP-UNUSED-TABLE', 'bburke@redhat.com', 'META-INF/jpa-changelog-4.0.0.xml', '2020-04-09 14:29:41.878709', 56, 'EXECUTED', '7:7822e0165097182e8f653c35517656a3', 'dropTable tableName=CLIENT_IDENTITY_PROV_MAPPING', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('4.0.0-KEYCLOAK-6228', 'bburke@redhat.com', 'META-INF/jpa-changelog-4.0.0.xml', '2020-04-09 14:29:42.028512', 57, 'EXECUTED', '7:c6538c29b9c9a08f9e9ea2de5c2b6375', 'dropUniqueConstraint constraintName=UK_JKUWUVD56ONTGSUHOGM8UEWRT, tableName=USER_CONSENT; dropNotNullConstraint columnName=CLIENT_ID, tableName=USER_CONSENT; addColumn tableName=USER_CONSENT; addUniqueConstraint constraintName=UK_JKUWUVD56ONTGSUHO...', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('4.0.0-KEYCLOAK-5579-fixed', 'mposolda@redhat.com', 'META-INF/jpa-changelog-4.0.0.xml', '2020-04-09 14:29:42.329597', 58, 'EXECUTED', '7:6d4893e36de22369cf73bcb051ded875', 'dropForeignKeyConstraint baseTableName=CLIENT_TEMPLATE_ATTRIBUTES, constraintName=FK_CL_TEMPL_ATTR_TEMPL; renameTable newTableName=CLIENT_SCOPE_ATTRIBUTES, oldTableName=CLIENT_TEMPLATE_ATTRIBUTES; renameColumn newColumnName=SCOPE_ID, oldColumnName...', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('authz-4.0.0.CR1', 'psilva@redhat.com', 'META-INF/jpa-changelog-authz-4.0.0.CR1.xml', '2020-04-09 14:29:42.557206', 59, 'EXECUTED', '7:57960fc0b0f0dd0563ea6f8b2e4a1707', 'createTable tableName=RESOURCE_SERVER_PERM_TICKET; addPrimaryKey constraintName=CONSTRAINT_FAPMT, tableName=RESOURCE_SERVER_PERM_TICKET; addForeignKeyConstraint baseTableName=RESOURCE_SERVER_PERM_TICKET, constraintName=FK_FRSRHO213XCX4WNKOG82SSPMT...', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('authz-4.0.0.Beta3', 'psilva@redhat.com', 'META-INF/jpa-changelog-authz-4.0.0.Beta3.xml', '2020-04-09 14:29:42.581235', 60, 'EXECUTED', '7:2b4b8bff39944c7097977cc18dbceb3b', 'addColumn tableName=RESOURCE_SERVER_POLICY; addColumn tableName=RESOURCE_SERVER_PERM_TICKET; addForeignKeyConstraint baseTableName=RESOURCE_SERVER_PERM_TICKET, constraintName=FK_FRSRPO2128CX4WNKOG82SSRFY, referencedTableName=RESOURCE_SERVER_POLICY', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('authz-4.2.0.Final', 'mhajas@redhat.com', 'META-INF/jpa-changelog-authz-4.2.0.Final.xml', '2020-04-09 14:29:42.627976', 61, 'EXECUTED', '7:2aa42a964c59cd5b8ca9822340ba33a8', 'createTable tableName=RESOURCE_URIS; addForeignKeyConstraint baseTableName=RESOURCE_URIS, constraintName=FK_RESOURCE_SERVER_URIS, referencedTableName=RESOURCE_SERVER_RESOURCE; customChange; dropColumn columnName=URI, tableName=RESOURCE_SERVER_RESO...', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('4.2.0-KEYCLOAK-6313', 'wadahiro@gmail.com', 'META-INF/jpa-changelog-4.2.0.xml', '2020-04-09 14:29:42.633436', 62, 'EXECUTED', '7:14d407c35bc4fe1976867756bcea0c36', 'addColumn tableName=REQUIRED_ACTION_PROVIDER', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('4.3.0-KEYCLOAK-7984', 'wadahiro@gmail.com', 'META-INF/jpa-changelog-4.3.0.xml', '2020-04-09 14:29:42.644738', 63, 'EXECUTED', '7:241a8030c748c8548e346adee548fa93', 'update tableName=REQUIRED_ACTION_PROVIDER', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('4.6.0-KEYCLOAK-7950', 'psilva@redhat.com', 'META-INF/jpa-changelog-4.6.0.xml', '2020-04-09 14:29:42.657096', 64, 'EXECUTED', '7:7d3182f65a34fcc61e8d23def037dc3f', 'update tableName=RESOURCE_SERVER_RESOURCE', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('4.6.0-KEYCLOAK-8377', 'keycloak', 'META-INF/jpa-changelog-4.6.0.xml', '2020-04-09 14:29:42.709461', 65, 'EXECUTED', '7:b30039e00a0b9715d430d1b0636728fa', 'createTable tableName=ROLE_ATTRIBUTE; addPrimaryKey constraintName=CONSTRAINT_ROLE_ATTRIBUTE_PK, tableName=ROLE_ATTRIBUTE; addForeignKeyConstraint baseTableName=ROLE_ATTRIBUTE, constraintName=FK_ROLE_ATTRIBUTE_ID, referencedTableName=KEYCLOAK_ROLE...', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('4.6.0-KEYCLOAK-8555', 'gideonray@gmail.com', 'META-INF/jpa-changelog-4.6.0.xml', '2020-04-09 14:29:42.727633', 66, 'EXECUTED', '7:3797315ca61d531780f8e6f82f258159', 'createIndex indexName=IDX_COMPONENT_PROVIDER_TYPE, tableName=COMPONENT', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('4.7.0-KEYCLOAK-1267', 'sguilhen@redhat.com', 'META-INF/jpa-changelog-4.7.0.xml', '2020-04-09 14:29:42.953364', 67, 'EXECUTED', '7:c7aa4c8d9573500c2d347c1941ff0301', 'addColumn tableName=REALM', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('4.7.0-KEYCLOAK-7275', 'keycloak', 'META-INF/jpa-changelog-4.7.0.xml', '2020-04-09 14:29:43.100869', 68, 'EXECUTED', '7:b207faee394fc074a442ecd42185a5dd', 'renameColumn newColumnName=CREATED_ON, oldColumnName=LAST_SESSION_REFRESH, tableName=OFFLINE_USER_SESSION; addNotNullConstraint columnName=CREATED_ON, tableName=OFFLINE_USER_SESSION; addColumn tableName=OFFLINE_USER_SESSION; customChange; createIn...', '', NULL, '3.5.4', NULL, NULL, '6442573717');
INSERT INTO auth.databasechangelog VALUES ('4.8.0-KEYCLOAK-8835', 'sguilhen@redhat.com', 'META-INF/jpa-changelog-4.8.0.xml', '2020-04-09 14:29:43.129001', 69, 'EXECUTED', '7:ab9a9762faaba4ddfa35514b212c4922', 'addNotNullConstraint columnName=SSO_MAX_LIFESPAN_REMEMBER_ME, tableName=REALM; addNotNullConstraint columnName=SSO_IDLE_TIMEOUT_REMEMBER_ME, tableName=REALM', '', NULL, '3.5.4', NULL, NULL, '6442573717');


--
-- Data for Name: databasechangeloglock; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--

INSERT INTO auth.databasechangeloglock VALUES (1, false, NULL, NULL);


--
-- Data for Name: default_client_scope; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--

INSERT INTO auth.default_client_scope VALUES ('master', '6fb2a610-f30d-422e-8989-65de1384b2a7', false);
INSERT INTO auth.default_client_scope VALUES ('master', '137bbf78-f128-4430-a903-3f75d2dc21c1', true);
INSERT INTO auth.default_client_scope VALUES ('master', '01e619ba-b33a-43f1-9fce-275a97bd1ed9', true);
INSERT INTO auth.default_client_scope VALUES ('master', '04f08721-3b7f-474f-a832-fd10dbb25f93', true);
INSERT INTO auth.default_client_scope VALUES ('master', '6af199c9-55c2-4bfd-8d84-4754ac4e0785', false);
INSERT INTO auth.default_client_scope VALUES ('master', 'dce4aead-463e-404c-b6d3-8b8af9848858', false);
INSERT INTO auth.default_client_scope VALUES ('master', 'e673129b-836e-4d83-b2e3-29222d509cbd', true);
INSERT INTO auth.default_client_scope VALUES ('master', '60a35141-6a11-4cf7-b6e6-d96551bf769f', true);
INSERT INTO auth.default_client_scope VALUES ('master', 'd8127c92-669b-4103-9589-18a43f0b96d0', false);
INSERT INTO auth.default_client_scope VALUES ('covid-19-response', '09b00872-fac3-4e22-b8e4-58b8e5b8974e', false);
INSERT INTO auth.default_client_scope VALUES ('covid-19-response', 'd7aa712a-765a-4781-bc44-71cd45e9478d', true);
INSERT INTO auth.default_client_scope VALUES ('covid-19-response', 'be2e69a2-3bab-4dc6-ad0b-08da74ad6639', true);
INSERT INTO auth.default_client_scope VALUES ('covid-19-response', '893f5998-d9e1-4abe-97d6-c76d36eeca5b', true);
INSERT INTO auth.default_client_scope VALUES ('covid-19-response', '9e9d9d47-955f-4caa-b1d7-6270c3ec9851', false);
INSERT INTO auth.default_client_scope VALUES ('covid-19-response', 'f983b788-77ec-4d94-827a-381cd4a09fdb', false);
INSERT INTO auth.default_client_scope VALUES ('covid-19-response', 'c3087358-01ce-4008-b98c-ef8a63314098', true);
INSERT INTO auth.default_client_scope VALUES ('covid-19-response', '94394280-1348-44e2-b162-3ae3423ea30c', true);
INSERT INTO auth.default_client_scope VALUES ('covid-19-response', '166fc10c-ad21-428e-b44c-957572891d70', false);
INSERT INTO auth.default_client_scope VALUES ('covid-19-response', '22f7b8c8-2916-45c5-ab2d-04f9604531ed', false);


--
-- Data for Name: event_entity; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: fed_user_credential; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: fed_credential_attribute; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: fed_user_attribute; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: fed_user_consent; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: fed_user_consent_cl_scope; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: fed_user_group_membership; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: fed_user_required_action; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: fed_user_role_mapping; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: federated_identity; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: federated_user; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: keycloak_group; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: group_attribute; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: group_role_mapping; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: identity_provider; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: identity_provider_config; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: identity_provider_mapper; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: idp_mapper_config; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: migration_model; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--

INSERT INTO auth.migration_model VALUES ('SINGLETON', '4.6.0');


--
-- Data for Name: offline_client_session; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: offline_user_session; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: policy_config; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: protocol_mapper; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--

INSERT INTO auth.protocol_mapper VALUES ('0eb780c6-a2d8-4ded-9c13-dbe04f16ff24', 'locale', 'openid-connect', 'oidc-usermodel-attribute-mapper', '2834224e-a18a-4c06-bb03-62e6232ee4dc', NULL);
INSERT INTO auth.protocol_mapper VALUES ('633c8683-5aef-4638-a250-50fa22b39733', 'role list', 'saml', 'saml-role-list-mapper', NULL, '137bbf78-f128-4430-a903-3f75d2dc21c1');
INSERT INTO auth.protocol_mapper VALUES ('2cf018e2-4eca-4e31-8fd3-64b7baee112d', 'full name', 'openid-connect', 'oidc-full-name-mapper', NULL, '01e619ba-b33a-43f1-9fce-275a97bd1ed9');
INSERT INTO auth.protocol_mapper VALUES ('22c5cc1b-81af-478b-bdb4-f89b9564510c', 'family name', 'openid-connect', 'oidc-usermodel-property-mapper', NULL, '01e619ba-b33a-43f1-9fce-275a97bd1ed9');
INSERT INTO auth.protocol_mapper VALUES ('c68dd392-bf5c-42b8-b036-c881657dc0da', 'given name', 'openid-connect', 'oidc-usermodel-property-mapper', NULL, '01e619ba-b33a-43f1-9fce-275a97bd1ed9');
INSERT INTO auth.protocol_mapper VALUES ('57a21e0d-fbbf-4a1f-a61e-8f10a53debda', 'middle name', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, '01e619ba-b33a-43f1-9fce-275a97bd1ed9');
INSERT INTO auth.protocol_mapper VALUES ('1638b421-c3e3-44a2-a713-56f7ba21689a', 'nickname', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, '01e619ba-b33a-43f1-9fce-275a97bd1ed9');
INSERT INTO auth.protocol_mapper VALUES ('73ba3f6e-7418-48d9-ae6b-65ba43e48833', 'username', 'openid-connect', 'oidc-usermodel-property-mapper', NULL, '01e619ba-b33a-43f1-9fce-275a97bd1ed9');
INSERT INTO auth.protocol_mapper VALUES ('ed282abd-c078-4dca-8fc3-b16562cfc4a5', 'profile', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, '01e619ba-b33a-43f1-9fce-275a97bd1ed9');
INSERT INTO auth.protocol_mapper VALUES ('46588848-241b-4561-976a-be289bb8be58', 'picture', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, '01e619ba-b33a-43f1-9fce-275a97bd1ed9');
INSERT INTO auth.protocol_mapper VALUES ('c95f6f55-dc85-437c-936e-336ee1eaa1db', 'website', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, '01e619ba-b33a-43f1-9fce-275a97bd1ed9');
INSERT INTO auth.protocol_mapper VALUES ('de1cb800-79c7-4946-9920-4c0be40df407', 'gender', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, '01e619ba-b33a-43f1-9fce-275a97bd1ed9');
INSERT INTO auth.protocol_mapper VALUES ('84a0fc6e-4665-4a6d-98f0-388a7878e417', 'birthdate', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, '01e619ba-b33a-43f1-9fce-275a97bd1ed9');
INSERT INTO auth.protocol_mapper VALUES ('3952a685-6d32-4004-bd2a-57f991bb2017', 'zoneinfo', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, '01e619ba-b33a-43f1-9fce-275a97bd1ed9');
INSERT INTO auth.protocol_mapper VALUES ('e3d09aa7-45e9-4d2d-9c69-7d6ba24ba430', 'locale', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, '01e619ba-b33a-43f1-9fce-275a97bd1ed9');
INSERT INTO auth.protocol_mapper VALUES ('f6853d53-8243-43bc-9a1a-b608f725197f', 'updated at', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, '01e619ba-b33a-43f1-9fce-275a97bd1ed9');
INSERT INTO auth.protocol_mapper VALUES ('db334b12-9b30-4241-bf11-2b2245cfbd93', 'email', 'openid-connect', 'oidc-usermodel-property-mapper', NULL, '04f08721-3b7f-474f-a832-fd10dbb25f93');
INSERT INTO auth.protocol_mapper VALUES ('c275cce6-2315-4a7b-b1cd-271a404f42e9', 'email verified', 'openid-connect', 'oidc-usermodel-property-mapper', NULL, '04f08721-3b7f-474f-a832-fd10dbb25f93');
INSERT INTO auth.protocol_mapper VALUES ('92bd85b4-f97d-45da-9fdb-b9091b358a00', 'address', 'openid-connect', 'oidc-address-mapper', NULL, '6af199c9-55c2-4bfd-8d84-4754ac4e0785');
INSERT INTO auth.protocol_mapper VALUES ('1ee51198-790f-4930-a65d-5fe0cab5e86e', 'phone number', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, 'dce4aead-463e-404c-b6d3-8b8af9848858');
INSERT INTO auth.protocol_mapper VALUES ('bfa61524-dc67-4ae0-92d6-f484968b9a3d', 'phone number verified', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, 'dce4aead-463e-404c-b6d3-8b8af9848858');
INSERT INTO auth.protocol_mapper VALUES ('d097b40a-4cd2-4ae3-8436-88a1eec67a26', 'realm roles', 'openid-connect', 'oidc-usermodel-realm-role-mapper', NULL, 'e673129b-836e-4d83-b2e3-29222d509cbd');
INSERT INTO auth.protocol_mapper VALUES ('23ab5efd-ba94-4727-b087-eb2d98802f2f', 'client roles', 'openid-connect', 'oidc-usermodel-client-role-mapper', NULL, 'e673129b-836e-4d83-b2e3-29222d509cbd');
INSERT INTO auth.protocol_mapper VALUES ('c345d0ab-9798-4a6b-8f64-a06df2c0c5be', 'audience resolve', 'openid-connect', 'oidc-audience-resolve-mapper', NULL, 'e673129b-836e-4d83-b2e3-29222d509cbd');
INSERT INTO auth.protocol_mapper VALUES ('414595e8-1105-4a6c-8502-f211ea933bd5', 'allowed web origins', 'openid-connect', 'oidc-allowed-origins-mapper', NULL, '60a35141-6a11-4cf7-b6e6-d96551bf769f');
INSERT INTO auth.protocol_mapper VALUES ('4fbc4243-3cae-4b83-82bf-8233c366629d', 'upn', 'openid-connect', 'oidc-usermodel-property-mapper', NULL, 'd8127c92-669b-4103-9589-18a43f0b96d0');
INSERT INTO auth.protocol_mapper VALUES ('af7ef612-bccd-4203-9021-7e4c9a083e70', 'groups', 'openid-connect', 'oidc-usermodel-realm-role-mapper', NULL, 'd8127c92-669b-4103-9589-18a43f0b96d0');
INSERT INTO auth.protocol_mapper VALUES ('86f3fd5d-fc9d-4456-98ab-6a89c1ca0e4c', 'role list', 'saml', 'saml-role-list-mapper', NULL, 'd7aa712a-765a-4781-bc44-71cd45e9478d');
INSERT INTO auth.protocol_mapper VALUES ('373bdc67-14ea-4495-93e5-fa86ca3d853a', 'full name', 'openid-connect', 'oidc-full-name-mapper', NULL, 'be2e69a2-3bab-4dc6-ad0b-08da74ad6639');
INSERT INTO auth.protocol_mapper VALUES ('feca9bcf-8b83-462a-9803-12399024ff2e', 'family name', 'openid-connect', 'oidc-usermodel-property-mapper', NULL, 'be2e69a2-3bab-4dc6-ad0b-08da74ad6639');
INSERT INTO auth.protocol_mapper VALUES ('1e921eed-97ac-4ead-afe9-18d3e45efbf5', 'given name', 'openid-connect', 'oidc-usermodel-property-mapper', NULL, 'be2e69a2-3bab-4dc6-ad0b-08da74ad6639');
INSERT INTO auth.protocol_mapper VALUES ('817b32ae-1208-4f38-86ad-b5648e6f9a35', 'middle name', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, 'be2e69a2-3bab-4dc6-ad0b-08da74ad6639');
INSERT INTO auth.protocol_mapper VALUES ('01d5364c-56c9-41f1-925f-6bbe7de0ffe8', 'nickname', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, 'be2e69a2-3bab-4dc6-ad0b-08da74ad6639');
INSERT INTO auth.protocol_mapper VALUES ('52707ded-895f-4b90-891c-0f34aa8133ba', 'username', 'openid-connect', 'oidc-usermodel-property-mapper', NULL, 'be2e69a2-3bab-4dc6-ad0b-08da74ad6639');
INSERT INTO auth.protocol_mapper VALUES ('ed271d5b-a784-477f-8669-b90dae8d8aa4', 'profile', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, 'be2e69a2-3bab-4dc6-ad0b-08da74ad6639');
INSERT INTO auth.protocol_mapper VALUES ('5bb76608-4ba6-46f9-9862-773d48799d4b', 'picture', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, 'be2e69a2-3bab-4dc6-ad0b-08da74ad6639');
INSERT INTO auth.protocol_mapper VALUES ('708c414f-fa78-400e-8a3e-5b948a5e6371', 'website', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, 'be2e69a2-3bab-4dc6-ad0b-08da74ad6639');
INSERT INTO auth.protocol_mapper VALUES ('d754d99d-f078-4050-a344-cd92ef08305e', 'gender', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, 'be2e69a2-3bab-4dc6-ad0b-08da74ad6639');
INSERT INTO auth.protocol_mapper VALUES ('bc8c4936-070c-40e2-a7d0-a3514b4584cc', 'birthdate', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, 'be2e69a2-3bab-4dc6-ad0b-08da74ad6639');
INSERT INTO auth.protocol_mapper VALUES ('d3f40651-51d7-4062-aac5-6256fcad2f1e', 'zoneinfo', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, 'be2e69a2-3bab-4dc6-ad0b-08da74ad6639');
INSERT INTO auth.protocol_mapper VALUES ('35194f3b-28a1-4240-97a6-6a34d7a0b142', 'locale', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, 'be2e69a2-3bab-4dc6-ad0b-08da74ad6639');
INSERT INTO auth.protocol_mapper VALUES ('9301812d-bc7e-43de-8bb0-8ce22696ba8e', 'updated at', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, 'be2e69a2-3bab-4dc6-ad0b-08da74ad6639');
INSERT INTO auth.protocol_mapper VALUES ('c8c26692-6e21-4b70-a098-496a1799d491', 'email', 'openid-connect', 'oidc-usermodel-property-mapper', NULL, '893f5998-d9e1-4abe-97d6-c76d36eeca5b');
INSERT INTO auth.protocol_mapper VALUES ('bc822e11-eedf-4ac9-9959-0816a9f66e19', 'email verified', 'openid-connect', 'oidc-usermodel-property-mapper', NULL, '893f5998-d9e1-4abe-97d6-c76d36eeca5b');
INSERT INTO auth.protocol_mapper VALUES ('c041b782-1625-423e-b7c5-b7b3026fbcad', 'address', 'openid-connect', 'oidc-address-mapper', NULL, '9e9d9d47-955f-4caa-b1d7-6270c3ec9851');
INSERT INTO auth.protocol_mapper VALUES ('95133923-26be-427b-b7f5-d527b7577a2d', 'phone number', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, 'f983b788-77ec-4d94-827a-381cd4a09fdb');
INSERT INTO auth.protocol_mapper VALUES ('9988b28f-c18b-4714-b347-fc0a10dfb05a', 'phone number verified', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, 'f983b788-77ec-4d94-827a-381cd4a09fdb');
INSERT INTO auth.protocol_mapper VALUES ('ba52f3a3-d4ee-4ed0-ab28-3ae976e81a0a', 'realm roles', 'openid-connect', 'oidc-usermodel-realm-role-mapper', NULL, 'c3087358-01ce-4008-b98c-ef8a63314098');
INSERT INTO auth.protocol_mapper VALUES ('70dc2c0c-ea32-4a37-b222-a2161ddc452e', 'client roles', 'openid-connect', 'oidc-usermodel-client-role-mapper', NULL, 'c3087358-01ce-4008-b98c-ef8a63314098');
INSERT INTO auth.protocol_mapper VALUES ('186eb5aa-5122-47f7-b030-31f816c682f2', 'audience resolve', 'openid-connect', 'oidc-audience-resolve-mapper', NULL, 'c3087358-01ce-4008-b98c-ef8a63314098');
INSERT INTO auth.protocol_mapper VALUES ('3ebde3e5-54d0-4a20-83fc-db14d91f4ddd', 'allowed web origins', 'openid-connect', 'oidc-allowed-origins-mapper', NULL, '94394280-1348-44e2-b162-3ae3423ea30c');
INSERT INTO auth.protocol_mapper VALUES ('d6e4b98f-2639-4018-9d42-be2a9b197e9e', 'upn', 'openid-connect', 'oidc-usermodel-property-mapper', NULL, '166fc10c-ad21-428e-b44c-957572891d70');
INSERT INTO auth.protocol_mapper VALUES ('d6130b68-476a-447e-a770-cd24d48dcf28', 'groups', 'openid-connect', 'oidc-usermodel-realm-role-mapper', NULL, '166fc10c-ad21-428e-b44c-957572891d70');
INSERT INTO auth.protocol_mapper VALUES ('0d8df143-222f-4945-ac1b-705dbcefab5b', 'locale', 'openid-connect', 'oidc-usermodel-attribute-mapper', '22d35cbb-94aa-4004-bb96-82b3687c3441', NULL);
INSERT INTO auth.protocol_mapper VALUES ('f7a64425-8980-4df4-8b13-4ca6bdf57597', 'profileId', 'openid-connect', 'oidc-usermodel-attribute-mapper', NULL, '22f7b8c8-2916-45c5-ab2d-04f9604531ed');


--
-- Data for Name: protocol_mapper_config; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--

INSERT INTO auth.protocol_mapper_config VALUES ('0eb780c6-a2d8-4ded-9c13-dbe04f16ff24', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('0eb780c6-a2d8-4ded-9c13-dbe04f16ff24', 'locale', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('0eb780c6-a2d8-4ded-9c13-dbe04f16ff24', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('0eb780c6-a2d8-4ded-9c13-dbe04f16ff24', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('0eb780c6-a2d8-4ded-9c13-dbe04f16ff24', 'locale', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('0eb780c6-a2d8-4ded-9c13-dbe04f16ff24', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('633c8683-5aef-4638-a250-50fa22b39733', 'false', 'single');
INSERT INTO auth.protocol_mapper_config VALUES ('633c8683-5aef-4638-a250-50fa22b39733', 'Basic', 'attribute.nameformat');
INSERT INTO auth.protocol_mapper_config VALUES ('633c8683-5aef-4638-a250-50fa22b39733', 'Role', 'attribute.name');
INSERT INTO auth.protocol_mapper_config VALUES ('2cf018e2-4eca-4e31-8fd3-64b7baee112d', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('2cf018e2-4eca-4e31-8fd3-64b7baee112d', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('2cf018e2-4eca-4e31-8fd3-64b7baee112d', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('22c5cc1b-81af-478b-bdb4-f89b9564510c', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('22c5cc1b-81af-478b-bdb4-f89b9564510c', 'lastName', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('22c5cc1b-81af-478b-bdb4-f89b9564510c', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('22c5cc1b-81af-478b-bdb4-f89b9564510c', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('22c5cc1b-81af-478b-bdb4-f89b9564510c', 'family_name', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('22c5cc1b-81af-478b-bdb4-f89b9564510c', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('c68dd392-bf5c-42b8-b036-c881657dc0da', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('c68dd392-bf5c-42b8-b036-c881657dc0da', 'firstName', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('c68dd392-bf5c-42b8-b036-c881657dc0da', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('c68dd392-bf5c-42b8-b036-c881657dc0da', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('c68dd392-bf5c-42b8-b036-c881657dc0da', 'given_name', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('c68dd392-bf5c-42b8-b036-c881657dc0da', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('57a21e0d-fbbf-4a1f-a61e-8f10a53debda', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('57a21e0d-fbbf-4a1f-a61e-8f10a53debda', 'middleName', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('57a21e0d-fbbf-4a1f-a61e-8f10a53debda', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('57a21e0d-fbbf-4a1f-a61e-8f10a53debda', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('57a21e0d-fbbf-4a1f-a61e-8f10a53debda', 'middle_name', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('57a21e0d-fbbf-4a1f-a61e-8f10a53debda', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('1638b421-c3e3-44a2-a713-56f7ba21689a', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('1638b421-c3e3-44a2-a713-56f7ba21689a', 'nickname', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('1638b421-c3e3-44a2-a713-56f7ba21689a', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('1638b421-c3e3-44a2-a713-56f7ba21689a', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('1638b421-c3e3-44a2-a713-56f7ba21689a', 'nickname', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('1638b421-c3e3-44a2-a713-56f7ba21689a', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('73ba3f6e-7418-48d9-ae6b-65ba43e48833', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('73ba3f6e-7418-48d9-ae6b-65ba43e48833', 'username', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('73ba3f6e-7418-48d9-ae6b-65ba43e48833', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('73ba3f6e-7418-48d9-ae6b-65ba43e48833', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('73ba3f6e-7418-48d9-ae6b-65ba43e48833', 'preferred_username', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('73ba3f6e-7418-48d9-ae6b-65ba43e48833', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('ed282abd-c078-4dca-8fc3-b16562cfc4a5', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('ed282abd-c078-4dca-8fc3-b16562cfc4a5', 'profile', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('ed282abd-c078-4dca-8fc3-b16562cfc4a5', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('ed282abd-c078-4dca-8fc3-b16562cfc4a5', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('ed282abd-c078-4dca-8fc3-b16562cfc4a5', 'profile', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('ed282abd-c078-4dca-8fc3-b16562cfc4a5', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('46588848-241b-4561-976a-be289bb8be58', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('46588848-241b-4561-976a-be289bb8be58', 'picture', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('46588848-241b-4561-976a-be289bb8be58', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('46588848-241b-4561-976a-be289bb8be58', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('46588848-241b-4561-976a-be289bb8be58', 'picture', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('46588848-241b-4561-976a-be289bb8be58', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('c95f6f55-dc85-437c-936e-336ee1eaa1db', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('c95f6f55-dc85-437c-936e-336ee1eaa1db', 'website', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('c95f6f55-dc85-437c-936e-336ee1eaa1db', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('c95f6f55-dc85-437c-936e-336ee1eaa1db', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('c95f6f55-dc85-437c-936e-336ee1eaa1db', 'website', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('c95f6f55-dc85-437c-936e-336ee1eaa1db', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('de1cb800-79c7-4946-9920-4c0be40df407', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('de1cb800-79c7-4946-9920-4c0be40df407', 'gender', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('de1cb800-79c7-4946-9920-4c0be40df407', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('de1cb800-79c7-4946-9920-4c0be40df407', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('de1cb800-79c7-4946-9920-4c0be40df407', 'gender', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('de1cb800-79c7-4946-9920-4c0be40df407', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('84a0fc6e-4665-4a6d-98f0-388a7878e417', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('84a0fc6e-4665-4a6d-98f0-388a7878e417', 'birthdate', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('84a0fc6e-4665-4a6d-98f0-388a7878e417', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('84a0fc6e-4665-4a6d-98f0-388a7878e417', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('84a0fc6e-4665-4a6d-98f0-388a7878e417', 'birthdate', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('84a0fc6e-4665-4a6d-98f0-388a7878e417', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('3952a685-6d32-4004-bd2a-57f991bb2017', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('3952a685-6d32-4004-bd2a-57f991bb2017', 'zoneinfo', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('3952a685-6d32-4004-bd2a-57f991bb2017', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('3952a685-6d32-4004-bd2a-57f991bb2017', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('3952a685-6d32-4004-bd2a-57f991bb2017', 'zoneinfo', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('3952a685-6d32-4004-bd2a-57f991bb2017', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('e3d09aa7-45e9-4d2d-9c69-7d6ba24ba430', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('e3d09aa7-45e9-4d2d-9c69-7d6ba24ba430', 'locale', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('e3d09aa7-45e9-4d2d-9c69-7d6ba24ba430', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('e3d09aa7-45e9-4d2d-9c69-7d6ba24ba430', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('e3d09aa7-45e9-4d2d-9c69-7d6ba24ba430', 'locale', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('e3d09aa7-45e9-4d2d-9c69-7d6ba24ba430', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('f6853d53-8243-43bc-9a1a-b608f725197f', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('f6853d53-8243-43bc-9a1a-b608f725197f', 'updatedAt', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('f6853d53-8243-43bc-9a1a-b608f725197f', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('f6853d53-8243-43bc-9a1a-b608f725197f', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('f6853d53-8243-43bc-9a1a-b608f725197f', 'updated_at', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('f6853d53-8243-43bc-9a1a-b608f725197f', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('db334b12-9b30-4241-bf11-2b2245cfbd93', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('db334b12-9b30-4241-bf11-2b2245cfbd93', 'email', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('db334b12-9b30-4241-bf11-2b2245cfbd93', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('db334b12-9b30-4241-bf11-2b2245cfbd93', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('db334b12-9b30-4241-bf11-2b2245cfbd93', 'email', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('db334b12-9b30-4241-bf11-2b2245cfbd93', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('c275cce6-2315-4a7b-b1cd-271a404f42e9', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('c275cce6-2315-4a7b-b1cd-271a404f42e9', 'emailVerified', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('c275cce6-2315-4a7b-b1cd-271a404f42e9', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('c275cce6-2315-4a7b-b1cd-271a404f42e9', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('c275cce6-2315-4a7b-b1cd-271a404f42e9', 'email_verified', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('c275cce6-2315-4a7b-b1cd-271a404f42e9', 'boolean', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('92bd85b4-f97d-45da-9fdb-b9091b358a00', 'formatted', 'user.attribute.formatted');
INSERT INTO auth.protocol_mapper_config VALUES ('92bd85b4-f97d-45da-9fdb-b9091b358a00', 'country', 'user.attribute.country');
INSERT INTO auth.protocol_mapper_config VALUES ('92bd85b4-f97d-45da-9fdb-b9091b358a00', 'postal_code', 'user.attribute.postal_code');
INSERT INTO auth.protocol_mapper_config VALUES ('92bd85b4-f97d-45da-9fdb-b9091b358a00', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('92bd85b4-f97d-45da-9fdb-b9091b358a00', 'street', 'user.attribute.street');
INSERT INTO auth.protocol_mapper_config VALUES ('92bd85b4-f97d-45da-9fdb-b9091b358a00', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('92bd85b4-f97d-45da-9fdb-b9091b358a00', 'region', 'user.attribute.region');
INSERT INTO auth.protocol_mapper_config VALUES ('92bd85b4-f97d-45da-9fdb-b9091b358a00', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('92bd85b4-f97d-45da-9fdb-b9091b358a00', 'locality', 'user.attribute.locality');
INSERT INTO auth.protocol_mapper_config VALUES ('1ee51198-790f-4930-a65d-5fe0cab5e86e', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('1ee51198-790f-4930-a65d-5fe0cab5e86e', 'phoneNumber', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('1ee51198-790f-4930-a65d-5fe0cab5e86e', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('1ee51198-790f-4930-a65d-5fe0cab5e86e', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('1ee51198-790f-4930-a65d-5fe0cab5e86e', 'phone_number', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('1ee51198-790f-4930-a65d-5fe0cab5e86e', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('bfa61524-dc67-4ae0-92d6-f484968b9a3d', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('bfa61524-dc67-4ae0-92d6-f484968b9a3d', 'phoneNumberVerified', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('bfa61524-dc67-4ae0-92d6-f484968b9a3d', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('bfa61524-dc67-4ae0-92d6-f484968b9a3d', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('bfa61524-dc67-4ae0-92d6-f484968b9a3d', 'phone_number_verified', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('bfa61524-dc67-4ae0-92d6-f484968b9a3d', 'boolean', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('d097b40a-4cd2-4ae3-8436-88a1eec67a26', 'true', 'multivalued');
INSERT INTO auth.protocol_mapper_config VALUES ('d097b40a-4cd2-4ae3-8436-88a1eec67a26', 'foo', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('d097b40a-4cd2-4ae3-8436-88a1eec67a26', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('d097b40a-4cd2-4ae3-8436-88a1eec67a26', 'realm_access.roles', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('d097b40a-4cd2-4ae3-8436-88a1eec67a26', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('23ab5efd-ba94-4727-b087-eb2d98802f2f', 'true', 'multivalued');
INSERT INTO auth.protocol_mapper_config VALUES ('23ab5efd-ba94-4727-b087-eb2d98802f2f', 'foo', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('23ab5efd-ba94-4727-b087-eb2d98802f2f', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('23ab5efd-ba94-4727-b087-eb2d98802f2f', 'resource_access.${client_id}.roles', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('23ab5efd-ba94-4727-b087-eb2d98802f2f', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('4fbc4243-3cae-4b83-82bf-8233c366629d', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('4fbc4243-3cae-4b83-82bf-8233c366629d', 'username', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('4fbc4243-3cae-4b83-82bf-8233c366629d', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('4fbc4243-3cae-4b83-82bf-8233c366629d', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('4fbc4243-3cae-4b83-82bf-8233c366629d', 'upn', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('4fbc4243-3cae-4b83-82bf-8233c366629d', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('af7ef612-bccd-4203-9021-7e4c9a083e70', 'true', 'multivalued');
INSERT INTO auth.protocol_mapper_config VALUES ('af7ef612-bccd-4203-9021-7e4c9a083e70', 'foo', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('af7ef612-bccd-4203-9021-7e4c9a083e70', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('af7ef612-bccd-4203-9021-7e4c9a083e70', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('af7ef612-bccd-4203-9021-7e4c9a083e70', 'groups', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('af7ef612-bccd-4203-9021-7e4c9a083e70', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('86f3fd5d-fc9d-4456-98ab-6a89c1ca0e4c', 'false', 'single');
INSERT INTO auth.protocol_mapper_config VALUES ('86f3fd5d-fc9d-4456-98ab-6a89c1ca0e4c', 'Basic', 'attribute.nameformat');
INSERT INTO auth.protocol_mapper_config VALUES ('86f3fd5d-fc9d-4456-98ab-6a89c1ca0e4c', 'Role', 'attribute.name');
INSERT INTO auth.protocol_mapper_config VALUES ('373bdc67-14ea-4495-93e5-fa86ca3d853a', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('373bdc67-14ea-4495-93e5-fa86ca3d853a', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('373bdc67-14ea-4495-93e5-fa86ca3d853a', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('feca9bcf-8b83-462a-9803-12399024ff2e', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('feca9bcf-8b83-462a-9803-12399024ff2e', 'lastName', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('feca9bcf-8b83-462a-9803-12399024ff2e', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('feca9bcf-8b83-462a-9803-12399024ff2e', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('feca9bcf-8b83-462a-9803-12399024ff2e', 'family_name', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('feca9bcf-8b83-462a-9803-12399024ff2e', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('1e921eed-97ac-4ead-afe9-18d3e45efbf5', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('1e921eed-97ac-4ead-afe9-18d3e45efbf5', 'firstName', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('1e921eed-97ac-4ead-afe9-18d3e45efbf5', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('1e921eed-97ac-4ead-afe9-18d3e45efbf5', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('1e921eed-97ac-4ead-afe9-18d3e45efbf5', 'given_name', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('1e921eed-97ac-4ead-afe9-18d3e45efbf5', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('817b32ae-1208-4f38-86ad-b5648e6f9a35', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('817b32ae-1208-4f38-86ad-b5648e6f9a35', 'middleName', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('817b32ae-1208-4f38-86ad-b5648e6f9a35', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('817b32ae-1208-4f38-86ad-b5648e6f9a35', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('817b32ae-1208-4f38-86ad-b5648e6f9a35', 'middle_name', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('817b32ae-1208-4f38-86ad-b5648e6f9a35', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('01d5364c-56c9-41f1-925f-6bbe7de0ffe8', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('01d5364c-56c9-41f1-925f-6bbe7de0ffe8', 'nickname', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('01d5364c-56c9-41f1-925f-6bbe7de0ffe8', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('01d5364c-56c9-41f1-925f-6bbe7de0ffe8', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('01d5364c-56c9-41f1-925f-6bbe7de0ffe8', 'nickname', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('01d5364c-56c9-41f1-925f-6bbe7de0ffe8', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('52707ded-895f-4b90-891c-0f34aa8133ba', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('52707ded-895f-4b90-891c-0f34aa8133ba', 'username', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('52707ded-895f-4b90-891c-0f34aa8133ba', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('52707ded-895f-4b90-891c-0f34aa8133ba', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('52707ded-895f-4b90-891c-0f34aa8133ba', 'preferred_username', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('52707ded-895f-4b90-891c-0f34aa8133ba', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('ed271d5b-a784-477f-8669-b90dae8d8aa4', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('ed271d5b-a784-477f-8669-b90dae8d8aa4', 'profile', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('ed271d5b-a784-477f-8669-b90dae8d8aa4', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('ed271d5b-a784-477f-8669-b90dae8d8aa4', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('ed271d5b-a784-477f-8669-b90dae8d8aa4', 'profile', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('ed271d5b-a784-477f-8669-b90dae8d8aa4', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('5bb76608-4ba6-46f9-9862-773d48799d4b', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('5bb76608-4ba6-46f9-9862-773d48799d4b', 'picture', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('5bb76608-4ba6-46f9-9862-773d48799d4b', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('5bb76608-4ba6-46f9-9862-773d48799d4b', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('5bb76608-4ba6-46f9-9862-773d48799d4b', 'picture', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('5bb76608-4ba6-46f9-9862-773d48799d4b', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('708c414f-fa78-400e-8a3e-5b948a5e6371', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('708c414f-fa78-400e-8a3e-5b948a5e6371', 'website', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('708c414f-fa78-400e-8a3e-5b948a5e6371', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('708c414f-fa78-400e-8a3e-5b948a5e6371', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('708c414f-fa78-400e-8a3e-5b948a5e6371', 'website', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('708c414f-fa78-400e-8a3e-5b948a5e6371', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('d754d99d-f078-4050-a344-cd92ef08305e', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('d754d99d-f078-4050-a344-cd92ef08305e', 'gender', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('d754d99d-f078-4050-a344-cd92ef08305e', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('d754d99d-f078-4050-a344-cd92ef08305e', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('d754d99d-f078-4050-a344-cd92ef08305e', 'gender', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('d754d99d-f078-4050-a344-cd92ef08305e', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('bc8c4936-070c-40e2-a7d0-a3514b4584cc', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('bc8c4936-070c-40e2-a7d0-a3514b4584cc', 'birthdate', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('bc8c4936-070c-40e2-a7d0-a3514b4584cc', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('bc8c4936-070c-40e2-a7d0-a3514b4584cc', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('bc8c4936-070c-40e2-a7d0-a3514b4584cc', 'birthdate', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('bc8c4936-070c-40e2-a7d0-a3514b4584cc', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('d3f40651-51d7-4062-aac5-6256fcad2f1e', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('d3f40651-51d7-4062-aac5-6256fcad2f1e', 'zoneinfo', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('d3f40651-51d7-4062-aac5-6256fcad2f1e', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('d3f40651-51d7-4062-aac5-6256fcad2f1e', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('d3f40651-51d7-4062-aac5-6256fcad2f1e', 'zoneinfo', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('d3f40651-51d7-4062-aac5-6256fcad2f1e', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('35194f3b-28a1-4240-97a6-6a34d7a0b142', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('35194f3b-28a1-4240-97a6-6a34d7a0b142', 'locale', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('35194f3b-28a1-4240-97a6-6a34d7a0b142', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('35194f3b-28a1-4240-97a6-6a34d7a0b142', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('35194f3b-28a1-4240-97a6-6a34d7a0b142', 'locale', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('35194f3b-28a1-4240-97a6-6a34d7a0b142', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('9301812d-bc7e-43de-8bb0-8ce22696ba8e', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('9301812d-bc7e-43de-8bb0-8ce22696ba8e', 'updatedAt', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('9301812d-bc7e-43de-8bb0-8ce22696ba8e', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('9301812d-bc7e-43de-8bb0-8ce22696ba8e', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('9301812d-bc7e-43de-8bb0-8ce22696ba8e', 'updated_at', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('9301812d-bc7e-43de-8bb0-8ce22696ba8e', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('c8c26692-6e21-4b70-a098-496a1799d491', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('c8c26692-6e21-4b70-a098-496a1799d491', 'email', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('c8c26692-6e21-4b70-a098-496a1799d491', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('c8c26692-6e21-4b70-a098-496a1799d491', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('c8c26692-6e21-4b70-a098-496a1799d491', 'email', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('c8c26692-6e21-4b70-a098-496a1799d491', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('bc822e11-eedf-4ac9-9959-0816a9f66e19', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('bc822e11-eedf-4ac9-9959-0816a9f66e19', 'emailVerified', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('bc822e11-eedf-4ac9-9959-0816a9f66e19', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('bc822e11-eedf-4ac9-9959-0816a9f66e19', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('bc822e11-eedf-4ac9-9959-0816a9f66e19', 'email_verified', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('bc822e11-eedf-4ac9-9959-0816a9f66e19', 'boolean', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('c041b782-1625-423e-b7c5-b7b3026fbcad', 'formatted', 'user.attribute.formatted');
INSERT INTO auth.protocol_mapper_config VALUES ('c041b782-1625-423e-b7c5-b7b3026fbcad', 'country', 'user.attribute.country');
INSERT INTO auth.protocol_mapper_config VALUES ('c041b782-1625-423e-b7c5-b7b3026fbcad', 'postal_code', 'user.attribute.postal_code');
INSERT INTO auth.protocol_mapper_config VALUES ('c041b782-1625-423e-b7c5-b7b3026fbcad', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('c041b782-1625-423e-b7c5-b7b3026fbcad', 'street', 'user.attribute.street');
INSERT INTO auth.protocol_mapper_config VALUES ('c041b782-1625-423e-b7c5-b7b3026fbcad', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('c041b782-1625-423e-b7c5-b7b3026fbcad', 'region', 'user.attribute.region');
INSERT INTO auth.protocol_mapper_config VALUES ('c041b782-1625-423e-b7c5-b7b3026fbcad', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('c041b782-1625-423e-b7c5-b7b3026fbcad', 'locality', 'user.attribute.locality');
INSERT INTO auth.protocol_mapper_config VALUES ('95133923-26be-427b-b7f5-d527b7577a2d', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('95133923-26be-427b-b7f5-d527b7577a2d', 'phoneNumber', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('95133923-26be-427b-b7f5-d527b7577a2d', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('95133923-26be-427b-b7f5-d527b7577a2d', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('95133923-26be-427b-b7f5-d527b7577a2d', 'phone_number', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('95133923-26be-427b-b7f5-d527b7577a2d', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('9988b28f-c18b-4714-b347-fc0a10dfb05a', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('9988b28f-c18b-4714-b347-fc0a10dfb05a', 'phoneNumberVerified', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('9988b28f-c18b-4714-b347-fc0a10dfb05a', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('9988b28f-c18b-4714-b347-fc0a10dfb05a', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('9988b28f-c18b-4714-b347-fc0a10dfb05a', 'phone_number_verified', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('9988b28f-c18b-4714-b347-fc0a10dfb05a', 'boolean', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('ba52f3a3-d4ee-4ed0-ab28-3ae976e81a0a', 'true', 'multivalued');
INSERT INTO auth.protocol_mapper_config VALUES ('ba52f3a3-d4ee-4ed0-ab28-3ae976e81a0a', 'foo', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('ba52f3a3-d4ee-4ed0-ab28-3ae976e81a0a', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('ba52f3a3-d4ee-4ed0-ab28-3ae976e81a0a', 'realm_access.roles', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('ba52f3a3-d4ee-4ed0-ab28-3ae976e81a0a', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('70dc2c0c-ea32-4a37-b222-a2161ddc452e', 'true', 'multivalued');
INSERT INTO auth.protocol_mapper_config VALUES ('70dc2c0c-ea32-4a37-b222-a2161ddc452e', 'foo', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('70dc2c0c-ea32-4a37-b222-a2161ddc452e', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('70dc2c0c-ea32-4a37-b222-a2161ddc452e', 'resource_access.${client_id}.roles', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('70dc2c0c-ea32-4a37-b222-a2161ddc452e', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('d6e4b98f-2639-4018-9d42-be2a9b197e9e', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('d6e4b98f-2639-4018-9d42-be2a9b197e9e', 'username', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('d6e4b98f-2639-4018-9d42-be2a9b197e9e', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('d6e4b98f-2639-4018-9d42-be2a9b197e9e', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('d6e4b98f-2639-4018-9d42-be2a9b197e9e', 'upn', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('d6e4b98f-2639-4018-9d42-be2a9b197e9e', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('d6130b68-476a-447e-a770-cd24d48dcf28', 'true', 'multivalued');
INSERT INTO auth.protocol_mapper_config VALUES ('d6130b68-476a-447e-a770-cd24d48dcf28', 'foo', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('d6130b68-476a-447e-a770-cd24d48dcf28', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('d6130b68-476a-447e-a770-cd24d48dcf28', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('d6130b68-476a-447e-a770-cd24d48dcf28', 'groups', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('d6130b68-476a-447e-a770-cd24d48dcf28', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('0d8df143-222f-4945-ac1b-705dbcefab5b', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('0d8df143-222f-4945-ac1b-705dbcefab5b', 'locale', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('0d8df143-222f-4945-ac1b-705dbcefab5b', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('0d8df143-222f-4945-ac1b-705dbcefab5b', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('0d8df143-222f-4945-ac1b-705dbcefab5b', 'locale', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('0d8df143-222f-4945-ac1b-705dbcefab5b', 'String', 'jsonType.label');
INSERT INTO auth.protocol_mapper_config VALUES ('f7a64425-8980-4df4-8b13-4ca6bdf57597', 'true', 'userinfo.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('f7a64425-8980-4df4-8b13-4ca6bdf57597', 'profileId', 'user.attribute');
INSERT INTO auth.protocol_mapper_config VALUES ('f7a64425-8980-4df4-8b13-4ca6bdf57597', 'true', 'id.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('f7a64425-8980-4df4-8b13-4ca6bdf57597', 'true', 'access.token.claim');
INSERT INTO auth.protocol_mapper_config VALUES ('f7a64425-8980-4df4-8b13-4ca6bdf57597', 'profile_id', 'claim.name');
INSERT INTO auth.protocol_mapper_config VALUES ('f7a64425-8980-4df4-8b13-4ca6bdf57597', 'String', 'jsonType.label');


--
-- Data for Name: realm_attribute; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--

INSERT INTO auth.realm_attribute VALUES ('_browser_header.contentSecurityPolicyReportOnly', '', 'master');
INSERT INTO auth.realm_attribute VALUES ('_browser_header.xContentTypeOptions', 'nosniff', 'master');
INSERT INTO auth.realm_attribute VALUES ('_browser_header.xRobotsTag', 'none', 'master');
INSERT INTO auth.realm_attribute VALUES ('_browser_header.xFrameOptions', 'SAMEORIGIN', 'master');
INSERT INTO auth.realm_attribute VALUES ('_browser_header.contentSecurityPolicy', 'frame-src ''self''; frame-ancestors ''self''; object-src ''none'';', 'master');
INSERT INTO auth.realm_attribute VALUES ('_browser_header.xXSSProtection', '1; mode=block', 'master');
INSERT INTO auth.realm_attribute VALUES ('_browser_header.strictTransportSecurity', 'max-age=31536000; includeSubDomains', 'master');
INSERT INTO auth.realm_attribute VALUES ('bruteForceProtected', 'false', 'master');
INSERT INTO auth.realm_attribute VALUES ('permanentLockout', 'false', 'master');
INSERT INTO auth.realm_attribute VALUES ('maxFailureWaitSeconds', '900', 'master');
INSERT INTO auth.realm_attribute VALUES ('minimumQuickLoginWaitSeconds', '60', 'master');
INSERT INTO auth.realm_attribute VALUES ('waitIncrementSeconds', '60', 'master');
INSERT INTO auth.realm_attribute VALUES ('quickLoginCheckMilliSeconds', '1000', 'master');
INSERT INTO auth.realm_attribute VALUES ('maxDeltaTimeSeconds', '43200', 'master');
INSERT INTO auth.realm_attribute VALUES ('failureFactor', '30', 'master');
INSERT INTO auth.realm_attribute VALUES ('displayName', 'Keycloak', 'master');
INSERT INTO auth.realm_attribute VALUES ('displayNameHtml', '<div class="kc-logo-text"><span>Keycloak</span></div>', 'master');
INSERT INTO auth.realm_attribute VALUES ('offlineSessionMaxLifespanEnabled', 'false', 'master');
INSERT INTO auth.realm_attribute VALUES ('offlineSessionMaxLifespan', '5184000', 'master');
INSERT INTO auth.realm_attribute VALUES ('_browser_header.contentSecurityPolicyReportOnly', '', 'covid-19-response');
INSERT INTO auth.realm_attribute VALUES ('_browser_header.xContentTypeOptions', 'nosniff', 'covid-19-response');
INSERT INTO auth.realm_attribute VALUES ('_browser_header.xRobotsTag', 'none', 'covid-19-response');
INSERT INTO auth.realm_attribute VALUES ('_browser_header.xFrameOptions', 'SAMEORIGIN', 'covid-19-response');
INSERT INTO auth.realm_attribute VALUES ('_browser_header.contentSecurityPolicy', 'frame-src ''self''; frame-ancestors ''self''; object-src ''none'';', 'covid-19-response');
INSERT INTO auth.realm_attribute VALUES ('_browser_header.xXSSProtection', '1; mode=block', 'covid-19-response');
INSERT INTO auth.realm_attribute VALUES ('_browser_header.strictTransportSecurity', 'max-age=31536000; includeSubDomains', 'covid-19-response');
INSERT INTO auth.realm_attribute VALUES ('bruteForceProtected', 'false', 'covid-19-response');
INSERT INTO auth.realm_attribute VALUES ('permanentLockout', 'false', 'covid-19-response');
INSERT INTO auth.realm_attribute VALUES ('maxFailureWaitSeconds', '900', 'covid-19-response');
INSERT INTO auth.realm_attribute VALUES ('minimumQuickLoginWaitSeconds', '60', 'covid-19-response');
INSERT INTO auth.realm_attribute VALUES ('waitIncrementSeconds', '60', 'covid-19-response');
INSERT INTO auth.realm_attribute VALUES ('quickLoginCheckMilliSeconds', '1000', 'covid-19-response');
INSERT INTO auth.realm_attribute VALUES ('maxDeltaTimeSeconds', '43200', 'covid-19-response');
INSERT INTO auth.realm_attribute VALUES ('failureFactor', '30', 'covid-19-response');
INSERT INTO auth.realm_attribute VALUES ('offlineSessionMaxLifespanEnabled', 'false', 'covid-19-response');
INSERT INTO auth.realm_attribute VALUES ('offlineSessionMaxLifespan', '5184000', 'covid-19-response');
INSERT INTO auth.realm_attribute VALUES ('actionTokenGeneratedByAdminLifespan', '43200', 'covid-19-response');
INSERT INTO auth.realm_attribute VALUES ('actionTokenGeneratedByUserLifespan', '300', 'covid-19-response');
INSERT INTO auth.realm_attribute VALUES ('displayName', '', 'covid-19-response');


--
-- Data for Name: realm_default_groups; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: realm_default_roles; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--

INSERT INTO auth.realm_default_roles VALUES ('master', 'a2f1c7f8-e931-4ae0-b554-32a351bbbb14');
INSERT INTO auth.realm_default_roles VALUES ('master', 'f4ff4b3b-949d-49b3-a3e8-b7ab6a879e73');
INSERT INTO auth.realm_default_roles VALUES ('covid-19-response', 'f1bee91c-a966-489f-8613-17f3b2c4f97a');
INSERT INTO auth.realm_default_roles VALUES ('covid-19-response', '45778388-9cb7-41d9-a60c-fb85a4626823');


--
-- Data for Name: realm_enabled_event_types; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: realm_events_listeners; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--

INSERT INTO auth.realm_events_listeners VALUES ('master', 'jboss-logging');
INSERT INTO auth.realm_events_listeners VALUES ('covid-19-response', 'jboss-logging');


--
-- Data for Name: realm_required_credential; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--

INSERT INTO auth.realm_required_credential VALUES ('password', 'password', true, true, 'master');
INSERT INTO auth.realm_required_credential VALUES ('password', 'password', true, true, 'covid-19-response');


--
-- Data for Name: realm_smtp_config; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: realm_supported_locales; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: redirect_uris; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--

INSERT INTO auth.redirect_uris VALUES ('e5cc98c8-b77c-45d4-94c3-7880a3212ce9', '/auth/realms/master/account/*');
INSERT INTO auth.redirect_uris VALUES ('2834224e-a18a-4c06-bb03-62e6232ee4dc', '/auth/admin/master/console/*');
INSERT INTO auth.redirect_uris VALUES ('de4d38cb-f94e-48ee-8d2d-58f7e84a56e6', '/auth/realms/covid-19-response/account/*');
INSERT INTO auth.redirect_uris VALUES ('22d35cbb-94aa-4004-bb96-82b3687c3441', '/auth/admin/covid-19-response/console/*');
INSERT INTO auth.redirect_uris VALUES ('4ae3a8d3-031e-4332-bbeb-c05edb02f358', 'https://stage.wefightcovid19.online/*');
INSERT INTO auth.redirect_uris VALUES ('4ae3a8d3-031e-4332-bbeb-c05edb02f358', 'https://test.wefightcovid19.online/*');
INSERT INTO auth.redirect_uris VALUES ('4ae3a8d3-031e-4332-bbeb-c05edb02f358', 'http://localhost:4200/*');
INSERT INTO auth.redirect_uris VALUES ('4ae3a8d3-031e-4332-bbeb-c05edb02f358', 'https://www.wefightcovid19.online/*');


--
-- Data for Name: required_action_config; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: required_action_provider; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--

INSERT INTO auth.required_action_provider VALUES ('c690a43c-b028-4521-8263-7c093dbbd19d', 'VERIFY_EMAIL', 'Verify Email', 'master', true, false, 'VERIFY_EMAIL', 50);
INSERT INTO auth.required_action_provider VALUES ('596d6510-4b63-4b44-a0e9-319c47779438', 'UPDATE_PROFILE', 'Update Profile', 'master', true, false, 'UPDATE_PROFILE', 40);
INSERT INTO auth.required_action_provider VALUES ('a7215f32-d57a-46c5-bc50-9aef27476e4c', 'CONFIGURE_TOTP', 'Configure OTP', 'master', true, false, 'CONFIGURE_TOTP', 10);
INSERT INTO auth.required_action_provider VALUES ('f2e9fc74-a2ce-4ef1-b396-7c666f0991ea', 'UPDATE_PASSWORD', 'Update Password', 'master', true, false, 'UPDATE_PASSWORD', 30);
INSERT INTO auth.required_action_provider VALUES ('74728e19-75ed-473a-b17e-9f897475d9cc', 'terms_and_conditions', 'Terms and Conditions', 'master', false, false, 'terms_and_conditions', 20);
INSERT INTO auth.required_action_provider VALUES ('7ee25d70-e936-4e7b-8b74-d00e17d76823', 'VERIFY_EMAIL', 'Verify Email', 'covid-19-response', true, false, 'VERIFY_EMAIL', 50);
INSERT INTO auth.required_action_provider VALUES ('6e4426d1-82da-4954-8046-746aaec2292d', 'UPDATE_PROFILE', 'Update Profile', 'covid-19-response', true, false, 'UPDATE_PROFILE', 40);
INSERT INTO auth.required_action_provider VALUES ('ad11fba5-dad4-4d27-80bd-e44b18542773', 'CONFIGURE_TOTP', 'Configure OTP', 'covid-19-response', true, false, 'CONFIGURE_TOTP', 10);
INSERT INTO auth.required_action_provider VALUES ('df20cb87-f924-4176-81f0-ce40dbe50961', 'UPDATE_PASSWORD', 'Update Password', 'covid-19-response', true, false, 'UPDATE_PASSWORD', 30);
INSERT INTO auth.required_action_provider VALUES ('ca6f0299-007f-4832-a397-06861b29842b', 'terms_and_conditions', 'Terms and Conditions', 'covid-19-response', false, false, 'terms_and_conditions', 20);


--
-- Data for Name: resource_server_resource; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: resource_attribute; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: resource_policy; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: resource_server_scope; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: resource_scope; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: resource_server_perm_ticket; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: resource_uris; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: role_attribute; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: scope_mapping; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: scope_policy; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: user_attribute; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--

INSERT INTO auth.user_attribute VALUES ('profileId', '323622bc-9866-4807-a567-1192971dc584', '61ce387d-99e3-4336-88f3-3eb695379216', '31d761eb-03fe-418d-b40d-93e4b896ae8c');


--
-- Data for Name: user_consent; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: user_consent_client_scope; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: user_federation_provider; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: user_federation_config; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: user_federation_mapper; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: user_federation_mapper_config; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: user_group_membership; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: user_required_action; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: user_role_mapping; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--

INSERT INTO auth.user_role_mapping VALUES ('02326645-6d43-4026-9291-e6961f7ab352', '5ce49b12-86bc-4eb1-88c5-40b0aeea9106');
INSERT INTO auth.user_role_mapping VALUES ('a2f1c7f8-e931-4ae0-b554-32a351bbbb14', '5ce49b12-86bc-4eb1-88c5-40b0aeea9106');
INSERT INTO auth.user_role_mapping VALUES ('f4ff4b3b-949d-49b3-a3e8-b7ab6a879e73', '5ce49b12-86bc-4eb1-88c5-40b0aeea9106');
INSERT INTO auth.user_role_mapping VALUES ('b673e7a0-bd70-4543-b840-c1c6cb193bee', '5ce49b12-86bc-4eb1-88c5-40b0aeea9106');
INSERT INTO auth.user_role_mapping VALUES ('348a55d0-14a9-428d-8b9c-3ea6cc73495f', '5ce49b12-86bc-4eb1-88c5-40b0aeea9106');
INSERT INTO auth.user_role_mapping VALUES ('f1bee91c-a966-489f-8613-17f3b2c4f97a', '61ce387d-99e3-4336-88f3-3eb695379216');
INSERT INTO auth.user_role_mapping VALUES ('5be77ce0-89ec-4e1b-8e62-ae3e04ad2303', '61ce387d-99e3-4336-88f3-3eb695379216');
INSERT INTO auth.user_role_mapping VALUES ('45778388-9cb7-41d9-a60c-fb85a4626823', '61ce387d-99e3-4336-88f3-3eb695379216');
INSERT INTO auth.user_role_mapping VALUES ('3faf057f-3d35-4ddb-b32f-f3e8d066e824', '61ce387d-99e3-4336-88f3-3eb695379216');
INSERT INTO auth.user_role_mapping VALUES ('72cd3b20-4531-467f-8f07-5905c105fad7', '61ce387d-99e3-4336-88f3-3eb695379216');
INSERT INTO auth.user_role_mapping VALUES ('f1bee91c-a966-489f-8613-17f3b2c4f97a', '2b571d00-7cd2-432b-a22a-fe1f4f8c9904');
INSERT INTO auth.user_role_mapping VALUES ('5be77ce0-89ec-4e1b-8e62-ae3e04ad2303', '2b571d00-7cd2-432b-a22a-fe1f4f8c9904');
INSERT INTO auth.user_role_mapping VALUES ('45778388-9cb7-41d9-a60c-fb85a4626823', '2b571d00-7cd2-432b-a22a-fe1f4f8c9904');
INSERT INTO auth.user_role_mapping VALUES ('3faf057f-3d35-4ddb-b32f-f3e8d066e824', '2b571d00-7cd2-432b-a22a-fe1f4f8c9904');
INSERT INTO auth.user_role_mapping VALUES ('e7ef0adb-7c2a-4602-95cf-e058adbdff94', '2b571d00-7cd2-432b-a22a-fe1f4f8c9904');
INSERT INTO auth.user_role_mapping VALUES ('698e008d-daa4-4af4-b5e9-715d25625bff', '2b571d00-7cd2-432b-a22a-fe1f4f8c9904');


--
-- Data for Name: user_session_note; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: username_login_failure; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--



--
-- Data for Name: web_origins; Type: TABLE DATA; Schema: auth; Owner: auth-db-user
--

INSERT INTO auth.web_origins VALUES ('4ae3a8d3-031e-4332-bbeb-c05edb02f358', '+');


--
-- PostgreSQL database dump complete
--


SET session_replication_role = DEFAULT;
