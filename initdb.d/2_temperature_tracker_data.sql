--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.17
-- Dumped by pg_dump version 10.8

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Data for Name: place; Type: TABLE DATA; Schema: temperature_tracker; Owner: temperature-tracker-db-user
--

INSERT INTO temperature_tracker.place VALUES ('1ca30e39-fc15-45a2-a78d-b0170c594dca', 'COUNTRY', -66.8854170000000039, 'United States', 'ChIJCzYy5IS16lQRQrfeQ5K5Oxw', 37.0902400000000014, -95.7128909999999991, 71.5388001000000031, 18.7762999999999991, 170.595699999999994, NULL);


--
-- Data for Name: profile; Type: TABLE DATA; Schema: temperature_tracker; Owner: temperature-tracker-db-user
--



--
-- Data for Name: symptom; Type: TABLE DATA; Schema: temperature_tracker; Owner: temperature-tracker-db-user
--

INSERT INTO temperature_tracker.symptom VALUES ('ebc5b106-fd6c-44c9-9180-6e3165b6241f', 0, 'a temperature');
INSERT INTO temperature_tracker.symptom VALUES ('10a033b3-2744-491a-95d3-16f0497af8b1', 1, 'a cough');
INSERT INTO temperature_tracker.symptom VALUES ('ffbfd06a-d55c-496b-b323-7d2d6c7b02f4', 2, 'sore throat');
INSERT INTO temperature_tracker.symptom VALUES ('33629a8b-1b7d-4ba4-bb5a-683698290cbb', 3, 'fast breathing');
INSERT INTO temperature_tracker.symptom VALUES ('3499310b-58d4-45aa-8d11-d4b2ba373377', 4, 'chills');
INSERT INTO temperature_tracker.symptom VALUES ('7b963ef9-2692-486b-bcf9-5293cb426aa5', 5, 'shaking with chills');
INSERT INTO temperature_tracker.symptom VALUES ('d822f43d-ce25-4c63-9349-bbe5869b2975', 6, 'muscle pain');
INSERT INTO temperature_tracker.symptom VALUES ('82114ad5-a861-4aca-bd22-28d5a2b497d8', 7, 'headache');
INSERT INTO temperature_tracker.symptom VALUES ('fb3cb11f-9237-42d2-88bc-c8d4bb614b4f', 8, 'a loss of taste or smell');
INSERT INTO temperature_tracker.symptom VALUES ('83ac6b76-33bb-4702-be4e-a501319802fc', 9, 'none of the above');


--
-- Data for Name: temperature; Type: TABLE DATA; Schema: temperature_tracker; Owner: temperature-tracker-db-user
--



--
-- Data for Name: temperature_symptom; Type: TABLE DATA; Schema: temperature_tracker; Owner: temperature-tracker-db-user
--



--
-- PostgreSQL database dump complete
--

