--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.17
-- Dumped by pg_dump version 10.8

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: temperature_tracker; Type: SCHEMA; Schema: -; Owner: temperature-tracker-db-user
--

CREATE SCHEMA temperature_tracker;


ALTER SCHEMA temperature_tracker OWNER TO "temperature-tracker-db-user";

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: place; Type: TABLE; Schema: temperature_tracker; Owner: temperature-tracker-db-user
--

CREATE TABLE temperature_tracker.place (
    place_id uuid NOT NULL,
    address_type character varying(255),
    east_bound double precision,
    formatted_address character varying(255),
    google_maps_place_id character varying(255),
    latitude double precision,
    longitude double precision,
    north_bound double precision,
    south_bound double precision,
    west_bound double precision,
    parent_id uuid
);


ALTER TABLE temperature_tracker.place OWNER TO "temperature-tracker-db-user";

--
-- Name: profile; Type: TABLE; Schema: temperature_tracker; Owner: temperature-tracker-db-user
--

CREATE TABLE temperature_tracker.profile (
    profile_id uuid NOT NULL,
    age integer,
    email character varying(255),
    gender character varying(255),
    temperature_unit character varying(255),
    place_id uuid
);


ALTER TABLE temperature_tracker.profile OWNER TO "temperature-tracker-db-user";

--
-- Name: symptom; Type: TABLE; Schema: temperature_tracker; Owner: temperature-tracker-db-user
--

CREATE TABLE temperature_tracker.symptom (
    symptom_id uuid NOT NULL,
    index integer,
    name character varying(255)
);


ALTER TABLE temperature_tracker.symptom OWNER TO "temperature-tracker-db-user";

--
-- Name: temperature; Type: TABLE; Schema: temperature_tracker; Owner: temperature-tracker-db-user
--

CREATE TABLE temperature_tracker.temperature (
    temperature_id uuid NOT NULL,
    creation_timestamp timestamp without time zone,
    latitude double precision,
    longitude double precision,
    temperature double precision,
    type character varying(255),
    place_id uuid,
    profile_id uuid
);


ALTER TABLE temperature_tracker.temperature OWNER TO "temperature-tracker-db-user";

--
-- Name: temperature_symptom; Type: TABLE; Schema: temperature_tracker; Owner: temperature-tracker-db-user
--

CREATE TABLE temperature_tracker.temperature_symptom (
    symptom_id uuid NOT NULL,
    temperature_id uuid NOT NULL
);


ALTER TABLE temperature_tracker.temperature_symptom OWNER TO "temperature-tracker-db-user";

--
-- Name: place place_pkey; Type: CONSTRAINT; Schema: temperature_tracker; Owner: temperature-tracker-db-user
--

ALTER TABLE ONLY temperature_tracker.place
    ADD CONSTRAINT place_pkey PRIMARY KEY (place_id);


--
-- Name: profile profile_pkey; Type: CONSTRAINT; Schema: temperature_tracker; Owner: temperature-tracker-db-user
--

ALTER TABLE ONLY temperature_tracker.profile
    ADD CONSTRAINT profile_pkey PRIMARY KEY (profile_id);


--
-- Name: symptom symptom_pkey; Type: CONSTRAINT; Schema: temperature_tracker; Owner: temperature-tracker-db-user
--

ALTER TABLE ONLY temperature_tracker.symptom
    ADD CONSTRAINT symptom_pkey PRIMARY KEY (symptom_id);


--
-- Name: temperature temperature_pkey; Type: CONSTRAINT; Schema: temperature_tracker; Owner: temperature-tracker-db-user
--

ALTER TABLE ONLY temperature_tracker.temperature
    ADD CONSTRAINT temperature_pkey PRIMARY KEY (temperature_id);


--
-- Name: temperature_symptom temperature_symptom_pkey; Type: CONSTRAINT; Schema: temperature_tracker; Owner: temperature-tracker-db-user
--

ALTER TABLE ONLY temperature_tracker.temperature_symptom
    ADD CONSTRAINT temperature_symptom_pkey PRIMARY KEY (symptom_id, temperature_id);


--
-- Name: temperature fk69p1227niek0h2ar11f6awrqc; Type: FK CONSTRAINT; Schema: temperature_tracker; Owner: temperature-tracker-db-user
--

ALTER TABLE ONLY temperature_tracker.temperature
    ADD CONSTRAINT fk69p1227niek0h2ar11f6awrqc FOREIGN KEY (place_id) REFERENCES temperature_tracker.place(place_id);


--
-- Name: profile fk92sse0cq327c149653gupq32j; Type: FK CONSTRAINT; Schema: temperature_tracker; Owner: temperature-tracker-db-user
--

ALTER TABLE ONLY temperature_tracker.profile
    ADD CONSTRAINT fk92sse0cq327c149653gupq32j FOREIGN KEY (place_id) REFERENCES temperature_tracker.place(place_id);


--
-- Name: temperature fkchvhepahhx0jkt41b1hnqars6; Type: FK CONSTRAINT; Schema: temperature_tracker; Owner: temperature-tracker-db-user
--

ALTER TABLE ONLY temperature_tracker.temperature
    ADD CONSTRAINT fkchvhepahhx0jkt41b1hnqars6 FOREIGN KEY (profile_id) REFERENCES temperature_tracker.profile(profile_id);


--
-- Name: temperature_symptom fkm95g0fl5as2q35sgwlh2oyb5d; Type: FK CONSTRAINT; Schema: temperature_tracker; Owner: temperature-tracker-db-user
--

ALTER TABLE ONLY temperature_tracker.temperature_symptom
    ADD CONSTRAINT fkm95g0fl5as2q35sgwlh2oyb5d FOREIGN KEY (temperature_id) REFERENCES temperature_tracker.temperature(temperature_id);


--
-- Name: place fknflrd9rn7yxovdmpqmxluif4t; Type: FK CONSTRAINT; Schema: temperature_tracker; Owner: temperature-tracker-db-user
--

ALTER TABLE ONLY temperature_tracker.place
    ADD CONSTRAINT fknflrd9rn7yxovdmpqmxluif4t FOREIGN KEY (parent_id) REFERENCES temperature_tracker.place(place_id);


--
-- Name: temperature_symptom fkq8u16y64ncc71bbwvk4rtqvbv; Type: FK CONSTRAINT; Schema: temperature_tracker; Owner: temperature-tracker-db-user
--

ALTER TABLE ONLY temperature_tracker.temperature_symptom
    ADD CONSTRAINT fkq8u16y64ncc71bbwvk4rtqvbv FOREIGN KEY (symptom_id) REFERENCES temperature_tracker.symptom(symptom_id);


--
-- PostgreSQL database dump complete
--

