FROM postgres:9.6

COPY initdb.d/*.sql /docker-entrypoint-initdb.d/
